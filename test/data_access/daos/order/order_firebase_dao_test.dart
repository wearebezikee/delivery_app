import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/daos/order/order_firebase_dao.dart';
import 'package:delivery_app/misc/random_generator.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/extra_item.dart';
import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/model/order_status.dart';
import 'package:delivery_app/model/payment/payment_method/cash_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/zelle_payment_method.dart';
import 'package:delivery_app/model/product.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  OrderFirebaseDao dao;
  Order toCreate;
  Order toCreate2;
  Order toUpdate;

  group('Order Firebase Dao', () {
    group('CRUD', () {
      setUp(() {
        instance = MockFirestoreInstance();
        dao = OrderFirebaseDao(instance);

        toCreate = Order(
            id: "orderId",
            address: Address(
                town: "Caracas",
                street: "Santa Fe Sur",
                city: "Distrito Capital",
                address1: "Residencia Los tuqueues th 9-h"),
            extras: [
              ExtraItem(id: "extraItemId1", title: "Extra Por hora", price: 8),
              ExtraItem(
                  id: "extraItemId2", title: "Extra Por distancia", price: 15)
            ],
            observation: "La hamburguesa sin mostaza",
            pickup: false,
            tip: 2,
            userId: "Uid1",
            userName: "User name 1",
            userPhone: "321321",
            paymentMethod: ZellePaymentMethod(
                id: "idMethod",
                confirmationImageUrl: "www.zelle.com/imageConfirmation.png"),
            totalPrice: 50,
            products: [
              CartItem(
                  product: Product(
                      id: "root" +
                          CategoryFirebaseDao.ID_SEPARATOR +
                          "RootProduct",
                      title: "RootProduct",
                      imageUrl: "RootProductImageUrl",
                      price: 5,
                      description: "root product description"),
                  quantity: 5),
              CartItem(
                  product: Product(
                      id: "root" +
                          CategoryFirebaseDao.ID_SEPARATOR +
                          "RootProduct2",
                      title: "RootProduct2",
                      imageUrl: "RootProductImageUrl2",
                      price: 7,
                      description: "root product description2"),
                  quantity: 2)
            ],
            statusHistory: [
              OrderStatus(
                  id: "status1",
                  title: "Order created",
                  date: DateTime.now(),
                  priority: 1,
                  status: OrderStatus.STATUS_SUCCESS),
              OrderStatus(
                  id: "status2",
                  title: "Order on kitchen",
                  date: DateTime.now(),
                  priority: 2,
                  status: OrderStatus.STATUS_PENDING)
            ]);

        toUpdate = Order(
            id: "orderId",
            address: Address(
                town: "Caracas Updated",
                street: "Santa Fe Sur Updated",
                city: "Distrito Capital Updated",
                address1: "Residencia Los tuqueues th 9-h Updated"),
            extras: [
              ExtraItem(
                  id: "extraItemId1",
                  title: "Extra Por hora Updated",
                  price: 8),
              ExtraItem(
                  id: "extraItemId2",
                  title: "Extra Por distancia Updated",
                  price: 15)
            ],
            observation: "La hamburguesa sin mostaza Updated",
            pickup: false,
            tip: 5,
            userId: "Uid1",
            userName: "User name 1 Updated",
            userPhone: "321321",
            paymentMethod: ZellePaymentMethod(
                id: "idMethod",
                confirmationImageUrl: "www.zelle.com/imageConfirmationNew.png"),
            totalPrice: 56,
            products: [
              CartItem(
                  product: Product(
                      id: "root" +
                          CategoryFirebaseDao.ID_SEPARATOR +
                          "RootProduct",
                      title: "RootProduct",
                      imageUrl: "RootProductImageUrlUpdated",
                      price: 5,
                      description: "root product descriptionUpdated"),
                  quantity: 5),
              CartItem(
                  product: Product(
                      id: "root" +
                          CategoryFirebaseDao.ID_SEPARATOR +
                          "RootProduct2",
                      title: "RootProduct2",
                      imageUrl: "RootProductImageUrl2Updated",
                      price: 7,
                      description: "root product description2Updated"),
                  quantity: 2)
            ],
            statusHistory: [
              OrderStatus(
                  id: "status1",
                  title: "Order created New",
                  date: DateTime.now(),
                  priority: 1,
                  status: OrderStatus.STATUS_SUCCESS),
              OrderStatus(
                  id: "status2",
                  title: "Order on kitchen new",
                  date: DateTime.now(),
                  priority: 2,
                  status: OrderStatus.STATUS_SUCCESS)
            ]);

        toCreate2 = Order(
            id: "orderId2",
            address: Address(
                town: "Merida",
                street: "Costa linda",
                city: "Distrito Capital",
                address1: "Residencia Los juanitos 155 - b"),
            extras: [
              ExtraItem(id: "extraItemId1", title: "Extra Por hora", price: 4),
              ExtraItem(
                  id: "extraItemId2", title: "Extra Por distancia", price: 20)
            ],
            observation: "",
            pickup: true,
            tip: 35,
            userId: "Uid2",
            userName: "User name 2",
            userPhone: "321321",
            paymentMethod: CashPaymentMethod(id: "idMethod", cashAmount: 40),
            totalPrice: 50,
            products: [
              CartItem(
                  product: Product(
                      id: "root" +
                          CategoryFirebaseDao.ID_SEPARATOR +
                          "RootProduct",
                      title: "RootProduct",
                      imageUrl: "RootProductImageUrl",
                      price: 5,
                      description: "root product description"),
                  quantity: 5),
              CartItem(
                  product: Product(
                      id: "root" +
                          CategoryFirebaseDao.ID_SEPARATOR +
                          "RootProduct2",
                      title: "RootProduct2",
                      imageUrl: "RootProductImageUrl2",
                      price: 7,
                      description: "root product description2"),
                  quantity: 2)
            ],
            statusHistory: [
              OrderStatus(
                  id: "status1",
                  title: "Order created",
                  date: DateTime.now(),
                  priority: 1,
                  status: OrderStatus.STATUS_PENDING),
              OrderStatus(
                  id: "status2",
                  title: "Order on kitchen",
                  date: DateTime.now(),
                  priority: 2,
                  status: OrderStatus.STATUS_PENDING)
            ]);
      });
      test('Create', () async {
        toCreate = await dao.create(toCreate);
        print(instance.dump());
        Order fetched = await dao.getById(toCreate.id);
        expect(toCreate, fetched);
      });

      test('Update', () async {
        toCreate = await dao.create(toCreate);
        toUpdate.id = toCreate.id;
        await dao.update(toUpdate);
        print(instance.dump());
        Order fetched = await dao.getById(toUpdate.id);
        expect(toUpdate, fetched);
      });

      test('Delete', () async {
        toCreate = await dao.create(toCreate);
        print(instance.dump());
        await dao.delete(toCreate.id);
        var exception;
        try {
          await dao.getById(toCreate.id);
        } catch (e) {
          exception = e;
        }
        expect(exception, isInstanceOf<NoSuchMethodError>());
      });
      test('Get All', () async {
        toCreate = await dao.create(toCreate);
        toCreate2 = await dao.create(toCreate2);
        print(instance.dump());
        List<Order> list = await dao.getAll();
        expect(list, [toCreate, toCreate2]);
      });
      test('Get With Pagination', () async {
        List<Order> orders = List<Order>();

        for (int i = 0; i < 150; i++) {
          toCreate.totalPrice =
              (await RandomGenerator.randomInt(500)).toDouble();
          toCreate = await dao.create(toCreate);
          orders.add(toCreate);
        }

        orders = await dao.getNext(Order.ATTRIBUTE_TOTAL_PRICE, 150, null);
        List<Order> list =
            await dao.getNext(Order.ATTRIBUTE_TOTAL_PRICE, 10, null);
        print(instance.dump());
        list = await dao.getNext(Order.ATTRIBUTE_TOTAL_PRICE, 10, list.last);

        List<Order> toCompare = orders.getRange(5, 15).toList();
        expect(list, toCompare);
      });
    });
  });
}
