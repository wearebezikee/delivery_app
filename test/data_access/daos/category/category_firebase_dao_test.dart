import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/daos/product/product_firebase_dao.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  CategoryFirebaseDao dao;

  Category root = Category(
      id: Category.ROOT_ID,
      name: Category.ROOT_ID,
      imageUrl: "rootImageUrl",
      categories: List<Category>(),
      products: List<Product>());

  Category subCat = Category(
      id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory",
      name: "SubCategory",
      imageUrl: "SubCategoryImageUrl",
      categories: List<Category>(),
      products: List<Product>());

  Category subCat2 = Category(
      id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory2",
      name: "SubCategory2",
      imageUrl: "SubCategory2ImageUrl",
      categories: List<Category>(),
      products: List<Product>());

  Category toUpdateRootCase = Category(
      id: Category.ROOT_ID,
      name: Category.ROOT_ID,
      imageUrl: "rootImageUrlNew",
      categories: List<Category>(),
      products: List<Product>());

  Category toUpdateSubCategoryFirstLevelCase = Category(
      id: Category.ROOT_ID + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory",
      name: "SubCategory",
      imageUrl: "SubCategoryImageUrlNew",
      categories: List<Category>(),
      products: List<Product>());
  Category toUpdateSubCategorySecondLevelPlusCase = Category(
      id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory2",
      name: "SubCategory2",
      imageUrl: "SubCategory2ImageUrlNew",
      categories: List<Category>(),
      products: List<Product>());

  group('Category Firebase Dao', () {
    setUp(() {
      instance = MockFirestoreInstance();
      dao = CategoryFirebaseDao(instance);
      //print(instance.dump());

      root = Category(
          id: Category.ROOT_ID,
          name: Category.ROOT_ID,
          imageUrl: "rootImageUrl",
          categories: List<Category>(),
          products: List<Product>());

      subCat = Category(
          id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory",
          name: "SubCategory",
          imageUrl: "SubCategoryImageUrl",
          categories: List<Category>(),
          products: List<Product>());

      subCat2 = Category(
          id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory2",
          name: "SubCategory2",
          imageUrl: "SubCategory2ImageUrl",
          categories: List<Category>(),
          products: List<Product>());
    });

    group('CRUD', () {
      group('Root case', () {
        test('Root: Create', () async {
          await dao.create(root);
          print(instance.dump());
          Category fetched = await dao.getById(root.id);
          expect(fetched, root);
        });

        test('Root: Update', () async {
          await dao.update(toUpdateRootCase);
          Category fetched = await dao.getById(toUpdateRootCase.id);
          expect(fetched, toUpdateRootCase);
        });

        test('Root: Delete', () async {
          String toDelete = Category.ROOT_ID;
          await dao.delete(toDelete);

          var exception;
          try {
            await dao.getById(toDelete);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
      });

      group('Sub category first level case', () {
        test('First level: Create', () async {
          await dao.create(root);

          await dao.create(subCat);
          root.categories.add(subCat);

          //print(instance.dump());
          Category fetched = await dao.getById(subCat.id);
          Category rootFetched = await dao.getById(root.id);
          expect(fetched, subCat);
          expect(rootFetched, root);
          expect(rootFetched.categoriesPreviewToJson(),
              root.categoriesPreviewToJson());
        });

        test('First level: Update', () async {
          await dao.create(root);

          await dao.create(subCat);
          await dao.update(toUpdateSubCategoryFirstLevelCase);
          root.categories.add(toUpdateSubCategoryFirstLevelCase);

          Category fetched =
              await dao.getById(toUpdateSubCategoryFirstLevelCase.id);
          Category rootFetched = await dao.getById(root.id);

          //print(instance.dump());
          expect(fetched, toUpdateSubCategoryFirstLevelCase);
          expect(rootFetched, root);
          expect(rootFetched.categoriesPreviewToJson(),
              root.categoriesPreviewToJson());
        });
        test('First level: Delete', () async {
          await dao.create(root);
          await dao.create(subCat);

          //print(instance.dump());
          await dao.delete(subCat.id);
          //print(instance.dump());
          Category rootFetched = await dao.getById(root.id);
          var exception;
          try {
            await dao.getById(subCat.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
          expect(rootFetched, root);
          expect(rootFetched.categoriesPreviewToJson(),
              root.categoriesPreviewToJson());
        });
      });
      group('Sub category second+ level case', () {
        test('Second+ level: Create', () async {
          await dao.create(root);
          await dao.create(subCat);
          await dao.create(subCat2);

          subCat.categories.add(subCat2);

          //print(instance.dump());
          Category fetched = await dao.getById(subCat2.id);
          Category subCatFetched = await dao.getById(subCat.id);
          expect(fetched, subCat2);
          expect(subCatFetched, subCat);
          expect(subCatFetched.categoriesPreviewToJson(),
              subCat.categoriesPreviewToJson());
        });

        test('Second+ level: Update', () async {
          await dao.create(root);
          await dao.create(subCat);
          await dao.create(subCat2);
          await dao.update(toUpdateSubCategorySecondLevelPlusCase);
          subCat.categories.add(toUpdateSubCategorySecondLevelPlusCase);

          Category fetched =
              await dao.getById(toUpdateSubCategorySecondLevelPlusCase.id);
          Category subCatFetched = await dao.getById(subCat.id);

          //print(instance.dump());
          expect(fetched, toUpdateSubCategorySecondLevelPlusCase);
          expect(subCatFetched, subCat);
          expect(subCatFetched.categoriesPreviewToJson(),
              subCat.categoriesPreviewToJson());
        });
        test('Second+ level: Delete', () async {
          await dao.create(root);
          await dao.create(subCat);
          await dao.create(subCat2);
          //print(instance.dump());
          await dao.delete(subCat2.id);
          //print(instance.dump());
          Category subCatFetched = await dao.getById(subCat.id);
          var exception;
          try {
            await dao.getById(subCat2.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
          expect(subCatFetched, subCat);
          expect(subCatFetched.categoriesPreviewToJson(),
              subCat.categoriesPreviewToJson());
        });
      });
    });

    group('Split Id', () {
      test('Root case', () {
        String toSplit = Category.ROOT_ID;
        List<String> catPathList = dao.splitId(toSplit);
        expect(catPathList.length, 1);
      });

      test('Sub category first level case', () {
        String toSplit =
            Category.ROOT_ID + CategoryFirebaseDao.ID_SEPARATOR + "CategoryId";
        List<String> catPathList = dao.splitId(toSplit);

        expect(catPathList.length, 2);
        expect(catPathList[0], "root");
        expect(catPathList[1], "CategoryId");
      });

      test('Sub category second level+ case', () {
        String toSplit = Category.ROOT_ID +
            CategoryFirebaseDao.ID_SEPARATOR +
            "CategoryId" +
            CategoryFirebaseDao.ID_SEPARATOR +
            "CategoryId2";
        List<String> catPathList = dao.splitId(toSplit);

        expect(catPathList.length, 3);

        expect(catPathList[0], "root");
        expect(catPathList[1], "CategoryId");
        expect(catPathList[2], "CategoryId2");
      });
    });

    group('Get Parent Id', () {
      test('Root case', () {
        String toSplit = Category.ROOT_ID;
        String catParentString = dao.getParentId(toSplit);
        expect(catParentString, null);
      });

      test('Sub category first level case', () {
        String toSplit =
            Category.ROOT_ID + CategoryFirebaseDao.ID_SEPARATOR + "CategoryId";

        String catParentString = dao.getParentId(toSplit);
        expect(catParentString, "root");
      });

      test('Sub category second level+ case', () {
        String toSplit = Category.ROOT_ID +
            CategoryFirebaseDao.ID_SEPARATOR +
            "CategoryId" +
            CategoryFirebaseDao.ID_SEPARATOR +
            "CategoryId2";
        String catParentString = dao.getParentId(toSplit);
        expect(catParentString,
            "root" + CategoryFirebaseDao.ID_SEPARATOR + "CategoryId");
      });
    });

    group('Get Category Reference', () {
      test('Root case', () {
        String path = dao.getCategoryReference(root.id).path;
        expect(path, "Category/root");
      });

      test('Sub category first level case', () {
        String path = dao.getCategoryReference(subCat.id).path;
        expect(path, "Category/root/Category/SubCategory");
      });

      test('Sub category second level+ case', () {
        String path = dao.getCategoryReference(subCat2.id).path;
        expect(
            path, "Category/root/Category/SubCategory/Category/SubCategory2");
      });
    });

    test('Get Full Category Tree', () async {
      root = Category(
          id: Category.ROOT_ID,
          name: Category.ROOT_ID,
          imageUrl: "rootImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      await dao.create(root);

      subCat = Category(
          id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory",
          name: "SubCategory",
          imageUrl: "SubCategoryImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      root.categories.add(subCat);
      await dao.create(subCat);

      subCat2 = Category(
          id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory2",
          name: "SubCategory2",
          imageUrl: "SubCategory2ImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      subCat.categories.add(subCat2);
      await dao.create(subCat2);

      Category subCatA = Category(
          id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategoryA",
          name: "SubCategoryA",
          imageUrl: "SubCategoryAImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      root.categories.add(subCatA);
      await dao.create(subCatA);

      Category subCatA2 = Category(
          id: subCatA.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategoryA2",
          name: "SubCategoryA2",
          imageUrl: "SubCategoryA2ImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      subCatA.categories.add(subCatA2);
      await dao.create(subCatA2);

      Category subCatA3 = Category(
          id: subCatA2.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategoryA3",
          name: "SubCategoryA3",
          imageUrl: "SubCategoryA3ImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      subCatA2.categories.add(subCatA3);
      await dao.create(subCatA3);

      Category subCatB = Category(
          id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategoryB",
          name: "SubCategoryB",
          imageUrl: "SubCategoryBImageUrl",
          categories: List<Category>(),
          products: List<Product>());
      root.categories.add(subCatB);
      await dao.create(subCatB);

      ProductFirebaseDao prodDao = ProductFirebaseDao(instance);
      Product rootProduct = Product(
          id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "RootProduct",
          title: "RootProduct",
          imageUrl: "RootProductImageUrl",
          price: 5,
          description: "root product description");
      root.products.add(rootProduct);
      await prodDao.create(rootProduct);

      Product subCatBProduct = Product(
          id: subCatB.id + CategoryFirebaseDao.ID_SEPARATOR + "subCatBProduct",
          title: "subCatBProduct",
          imageUrl: "subCatBProductImageUrl",
          price: 9,
          description: "subCatBProduct description");
      subCatB.products.add(subCatBProduct);
      subCatB.products.add(subCatBProduct);
      await prodDao.create(subCatBProduct);
      await prodDao.create(subCatBProduct);

      Product subCatA3Product = Product(
          id: subCatA3.id +
              CategoryFirebaseDao.ID_SEPARATOR +
              "subCatA3Product",
          title: "subCatA3Product",
          imageUrl: "subCatA3ProductProductImageUrl",
          price: 90,
          description: "subCatA3Product description");
      subCatA3.products.add(subCatA3Product);
      await prodDao.create(subCatA3Product);

      Category fullTreeFetched = await dao.getFullTreeFrom(Category.ROOT_ID);
      print(instance.dump());

      expect(fullTreeFetched.toJson(), root.toJson());
    });
  });
}
