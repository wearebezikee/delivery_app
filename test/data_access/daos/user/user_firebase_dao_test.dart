import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/user/user_firebase_dao.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/user.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  UserFirebaseDao dao;

  Address addressToAdd = Address(
      address1: "residence 13",
      address2: "apartment 3 a",
      city: "Caracas",
      street: "1543 southwest",
      town: "the hatish");

  Address addressToUpdate = Address(
      address1: "residence 12",
      address2: "apartment 2 b",
      city: "Merida",
      street: "15 northeast",
      town: "the encenada");

  User toAdd = User(
      id: "aasdfjnsdfljnlsdf",
      email: "user@gmail.com",
      imageUrl: "imageUrl",
      addresses: [addressToAdd, addressToUpdate],
      name: "User name",
      phoneNumber: "+548844471423",
      role: User.ROLE_ADMIN);
  User toUpdate = User(
      id: "aasdfjnsdfljnlsdf",
      email: "userupdated@gmail.com",
      imageUrl: "imageUrlNEr",
      addresses: [addressToUpdate],
      name: "User New name",
      phoneNumber: "+1321321321",
      role: User.ROLE_ADMIN);

  group('User Firebase Dao', () {
    setUp(() async {
      instance = MockFirestoreInstance();
      dao = UserFirebaseDao(instance);
      //print(instance.dump());
    });

    group('CRUD', () {
      test('Create', () async {
        await dao.create(toAdd);

        print(instance.dump());
        User fetched = await dao.getById(toAdd.id);
        expect(fetched, toAdd);
      });
      test('Update', () async {
        await dao.create(toAdd);
        await dao.update(toUpdate);

        print(instance.dump());
        User fetched = await dao.getById(toUpdate.id);

        expect(fetched, toUpdate);
      });
      test('Delete', () async {
        await dao.create(toAdd);
        await dao.delete(toAdd.id);
        print(instance.dump());
        var exception;
        try {
          await dao.getById(toAdd.id);
        } catch (e) {
          exception = e;
        }
        expect(exception, isInstanceOf<NoSuchMethodError>());
      });

      test('Get Full User', () async {
        await dao.create(toAdd);
        User fetched = await dao.getById(toAdd.id);
        expect(fetched, toAdd);
      });
    });
  });
}
