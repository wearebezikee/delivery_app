import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/payment_method_setting/payment_method_firebase_dao.dart';
import 'package:delivery_app/model/payment/payment_method_setting/cash_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/pago_movil_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/transfer_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/zelle_payment_method_setting.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  PaymentMethodSettingFirebaseDao dao;

  group('PaymentMethodSetting Firebase Dao', () {
    group('CRUD', () {
      group('Cash case', () {
        CashPaymentMethodSetting paymentMethodSetting;
        CashPaymentMethodSetting paymentMethodSetting2;
        CashPaymentMethodSetting paymentMethodSettingToUpdate;

        setUp(() {
          instance = MockFirestoreInstance();
          dao = PaymentMethodSettingFirebaseDao(instance);

          paymentMethodSetting = CashPaymentMethodSetting(
              id: "id", title: "Cashing", active: true);

          paymentMethodSettingToUpdate = CashPaymentMethodSetting(
              id: "id", title: "CashingUpdated", active: false);

          paymentMethodSetting2 = CashPaymentMethodSetting(
              id: "id", title: "Cashing2", active: true);
        });

        test('Create', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSetting.id);
          expect(paymentMethodSetting, fetched);
        });

        test('Update', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSettingToUpdate.id = paymentMethodSetting.id;
          await dao.update(paymentMethodSettingToUpdate);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSettingToUpdate.id);
          expect(paymentMethodSettingToUpdate, fetched);
        });

        test('Delete', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          await dao.delete(paymentMethodSetting.id);
          var exception;
          try {
            await dao.getById(paymentMethodSetting.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Get All', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSetting2 = await dao.create(paymentMethodSetting2);
          print(instance.dump());
          List<PaymentMethodSetting> postList = await dao.getAll();
          expect(postList, [paymentMethodSetting, paymentMethodSetting2]);
        });
      });
      group('PagoMovil case', () {
        PagoMovilPaymentMethodSetting paymentMethodSetting;
        PagoMovilPaymentMethodSetting paymentMethodSetting2;
        PagoMovilPaymentMethodSetting paymentMethodSettingToUpdate;

        setUp(() {
          instance = MockFirestoreInstance();
          dao = PaymentMethodSettingFirebaseDao(instance);

          paymentMethodSetting = PagoMovilPaymentMethodSetting(
              id: "id",
              title: "PagoMovil Banesco",
              active: true,
              bankName: "Banesco",
              email: "email@gmail.com",
              idNumber: 123456,
              phoneNumber: "04123155151");

          paymentMethodSettingToUpdate = PagoMovilPaymentMethodSetting(
              id: "id",
              title: "PagoMovil Mercantil",
              active: false,
              bankName: "Banesco",
              email: "emailUpdated@gmail.com",
              idNumber: 123456,
              phoneNumber: "04143155151");

          paymentMethodSetting2 = PagoMovilPaymentMethodSetting(
              id: "id",
              title: "PagoMovil Banesco2",
              active: true,
              bankName: "Banesco2",
              email: "email2@gmail.com",
              idNumber: 1234356,
              phoneNumber: "04143155151");
        });

        test('Create', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSetting.id);
          expect(paymentMethodSetting, fetched);
        });

        test('Update', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSettingToUpdate.id = paymentMethodSetting.id;
          await dao.update(paymentMethodSettingToUpdate);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSettingToUpdate.id);
          expect(paymentMethodSettingToUpdate, fetched);
        });

        test('Delete', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          await dao.delete(paymentMethodSetting.id);
          var exception;
          try {
            await dao.getById(paymentMethodSetting.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Get All', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSetting2 = await dao.create(paymentMethodSetting2);
          print(instance.dump());
          List<PaymentMethodSetting> postList = await dao.getAll();
          expect(postList, [paymentMethodSetting, paymentMethodSetting2]);
        });
      });

      group('Transfer case', () {
        TransferPaymentMethodSetting paymentMethodSetting;
        TransferPaymentMethodSetting paymentMethodSetting2;
        TransferPaymentMethodSetting paymentMethodSettingToUpdate;

        setUp(() {
          instance = MockFirestoreInstance();
          dao = PaymentMethodSettingFirebaseDao(instance);

          paymentMethodSetting = TransferPaymentMethodSetting(
              id: "id",
              title: "Transfer Banesco",
              active: true,
              bankName: "Banesco",
              email: "email@gmail.com",
              accountNumber: 123456,
              idNumber: 23113123);

          paymentMethodSettingToUpdate = TransferPaymentMethodSetting(
              id: "id",
              title: "Transfer Mercantil",
              active: true,
              bankName: "Mercantil",
              email: "emailUpdated@gmail.com",
              accountNumber: 123456,
              idNumber: 23113123);

          paymentMethodSetting2 = TransferPaymentMethodSetting(
              id: "id",
              title: "Transfer Provincial",
              active: true,
              bankName: "Provincial",
              email: "email@gmail.com",
              accountNumber: 123456,
              idNumber: 23113123);
        });

        test('Create', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSetting.id);
          expect(paymentMethodSetting, fetched);
        });

        test('Update', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSettingToUpdate.id = paymentMethodSetting.id;
          await dao.update(paymentMethodSettingToUpdate);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSettingToUpdate.id);
          expect(paymentMethodSettingToUpdate, fetched);
        });

        test('Delete', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          await dao.delete(paymentMethodSetting.id);
          var exception;
          try {
            await dao.getById(paymentMethodSetting.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Get All', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSetting2 = await dao.create(paymentMethodSetting2);
          print(instance.dump());
          List<PaymentMethodSetting> postList = await dao.getAll();
          expect(postList, [paymentMethodSetting, paymentMethodSetting2]);
        });
      });
      group('Zelle case', () {
        ZellePaymentMethodSetting paymentMethodSetting;
        ZellePaymentMethodSetting paymentMethodSetting2;
        ZellePaymentMethodSetting paymentMethodSettingToUpdate;

        setUp(() {
          instance = MockFirestoreInstance();
          dao = PaymentMethodSettingFirebaseDao(instance);

          paymentMethodSetting = ZellePaymentMethodSetting(
              id: "id",
              title: "Zelle",
              active: true,
              email: "email@gmail.com",
              description: "Put this please");

          paymentMethodSettingToUpdate = ZellePaymentMethodSetting(
              id: "id",
              title: "Zelle Updated",
              active: false,
              email: "emailNew@gmail.com",
              description: "Put this please new");

          paymentMethodSetting2 = ZellePaymentMethodSetting(
              id: "id",
              title: "Zelle Bofa",
              active: false,
              email: "emailBOFA@gmail.com",
              description: "Put this bofa");
        });

        test('Create', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSetting.id);
          expect(paymentMethodSetting, fetched);
        });

        test('Update', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSettingToUpdate.id = paymentMethodSetting.id;
          await dao.update(paymentMethodSettingToUpdate);
          print(instance.dump());
          PaymentMethodSetting fetched =
              await dao.getById(paymentMethodSettingToUpdate.id);
          expect(paymentMethodSettingToUpdate, fetched);
        });

        test('Delete', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          print(instance.dump());
          await dao.delete(paymentMethodSetting.id);
          var exception;
          try {
            await dao.getById(paymentMethodSetting.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Get All', () async {
          paymentMethodSetting = await dao.create(paymentMethodSetting);
          paymentMethodSetting2 = await dao.create(paymentMethodSetting2);
          print(instance.dump());
          List<PaymentMethodSetting> postList = await dao.getAll();
          expect(postList, [paymentMethodSetting, paymentMethodSetting2]);
        });
      });
    });
  });
}
