import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/schedule/schedule_firebase_dao.dart';
import 'package:delivery_app/model/schedule.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  ScheduleFirebaseDao dao;
  Schedule schedule;
  Schedule schedule2;
  Schedule scheduleToUpdate;

  group('Schedule Firebase Dao', () {
    group('CRUD', () {
      setUp(() {
        instance = MockFirestoreInstance();
        dao = ScheduleFirebaseDao(instance);

        schedule = Schedule(
          dayOfWeek: Schedule.DAY_OF_WEEK_MONDAY,
          startHour: TimeOfDay.fromDateTime(
              DateTime(2020, 1, 1, 12, 30, 0, 0, 0)), //12:30pm
          endHour: TimeOfDay.fromDateTime(
              DateTime(2020, 1, 1, 20, 30, 0, 0, 0)), //10:13pm
        );

        scheduleToUpdate = Schedule(
          dayOfWeek: Schedule.DAY_OF_WEEK_MONDAY,
          startHour: TimeOfDay.fromDateTime(
              DateTime(2020, 1, 1, 6, 30, 0, 0, 0)), //12:30pm
          endHour: TimeOfDay.fromDateTime(DateTime(2020, 1, 1, 21, 0, 0, 0, 0)),
        );

        schedule2 = Schedule(
          dayOfWeek: Schedule.DAY_OF_WEEK_FRIDAY,
          startHour: TimeOfDay.fromDateTime(
              DateTime(2020, 1, 1, 10, 0, 0, 0, 0)), //12:30pm
          endHour: TimeOfDay.fromDateTime(DateTime(2020, 1, 1, 24, 0, 0, 0, 0)),
        );
      });
      test('Create', () async {
        await dao.create(schedule);
        print(instance.dump());
        Schedule fetched = await dao.getById(schedule.id);
        expect(schedule, fetched);
      });

      test('Update', () async {
        schedule = await dao.create(schedule);
        scheduleToUpdate.id = schedule.id;
        await dao.update(scheduleToUpdate);
        print(instance.dump());
        Schedule fetched = await dao.getById(scheduleToUpdate.id);
        expect(scheduleToUpdate, fetched);
      });

      test('Delete', () async {
        schedule = await dao.create(schedule);
        print(instance.dump());
        await dao.delete(schedule.id);
        var exception;
        try {
          await dao.getById(schedule.id);
        } catch (e) {
          exception = e;
        }
        expect(exception, isInstanceOf<NoSuchMethodError>());
      });
      test('Get All', () async {
        schedule = await dao.create(schedule);
        schedule2 = await dao.create(schedule2);
        print(instance.dump());
        List<Schedule> postList = await dao.getAll();
        expect(postList, [schedule, schedule2]);
      });
    });
  });
}
