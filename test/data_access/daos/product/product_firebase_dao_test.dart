import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/daos/product/product_firebase_dao.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  CategoryFirebaseDao catDao;
  ProductFirebaseDao prodDao;

  Category root = Category(
      id: Category.ROOT_ID,
      name: Category.ROOT_ID,
      imageUrl: "rootImageUrl",
      categories: List<Category>(),
      products: List<Product>());

  Category subCat = Category(
      id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory",
      name: "SubCategory",
      imageUrl: "SubCategoryImageUrl",
      categories: List<Category>(),
      products: List<Product>());

  Category subCat2 = Category(
      id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory2",
      name: "SubCategory2",
      imageUrl: "SubCategory2ImageUrl",
      categories: List<Category>(),
      products: List<Product>());

  Product rootProduct = Product(
      id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "RootProduct",
      title: "RootProduct",
      imageUrl: "RootProductImageUrl",
      price: 5,
      description: "root product description");

  Product toUpdateRootProduct = Product(
      id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "RootProduct",
      title: "RootProductNew",
      imageUrl: "RootProductImageUrl",
      price: 6,
      description: "root product new description");

  Product subCatProduct = Product(
      id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "subCatProduct",
      title: "subCatProduct title",
      imageUrl: "subCatProductImageUrl",
      price: 55,
      description: "subCatProduct description");

  Product toUpdateSubCatProduct = Product(
      id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "subCatProduct",
      title: "subCatProduct title new",
      imageUrl: "subCatProductImageUrlNew",
      price: 66,
      description: "subCatProduct new description");

  group('Product Firebase Dao', () {
    setUp(() async {
      instance = MockFirestoreInstance();
      catDao = CategoryFirebaseDao(instance);
      prodDao = ProductFirebaseDao(instance);
      //print(instance.dump());

      root = Category(
          id: Category.ROOT_ID,
          name: Category.ROOT_ID,
          imageUrl: "rootImageUrl",
          categories: List<Category>(),
          products: List<Product>());

      subCat = Category(
          id: root.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory",
          name: "SubCategory",
          imageUrl: "SubCategoryImageUrl",
          categories: List<Category>(),
          products: List<Product>());

      subCat2 = Category(
          id: subCat.id + CategoryFirebaseDao.ID_SEPARATOR + "SubCategory2",
          name: "SubCategory2",
          imageUrl: "SubCategory2ImageUrl",
          categories: List<Category>(),
          products: List<Product>());

      await catDao.create(root);
      await catDao.create(subCat);
      await catDao.create(subCat2);

      root.categories.add(subCat);
      subCat.categories.add(subCat2);
      //print(instance.dump());
    });

    group('CRUD', () {
      group('Root case', () {
        test('Root: Create', () async {
          await prodDao.create(rootProduct);
          root.products.add(rootProduct);
          //print(instance.dump());
          Product fetched = await prodDao.getById(rootProduct.id);
          Category fetchedRootCat = await catDao.getByProductId(rootProduct.id);
          expect(fetched, rootProduct);
          expect(fetchedRootCat.productsPreviewToJson(),
              root.productsPreviewToJson());
        });
        test('Root: Update', () async {
          await prodDao.create(rootProduct);
          await prodDao.update(toUpdateRootProduct);
          root.products.add(toUpdateRootProduct);

          //print(instance.dump());
          Product fetched = await prodDao.getById(rootProduct.id);
          Category fetchedRootCat = await catDao.getByProductId(rootProduct.id);
          expect(fetched, toUpdateRootProduct);
          expect(fetchedRootCat.productsPreviewToJson(),
              root.productsPreviewToJson());
        });
        test('Root: Delete', () async {
          await prodDao.create(rootProduct);
          await prodDao.delete(rootProduct.id);
          print(instance.dump());
          var exception;
          try {
            await prodDao.getById(rootProduct.id);
          } catch (e) {
            exception = e;
          }
          Category fetchedRootCat = await catDao.getByProductId(rootProduct.id);
          expect(exception, isInstanceOf<NoSuchMethodError>());
          expect(fetchedRootCat.productsPreviewToJson(),
              root.productsPreviewToJson());
        });
      });
    });

    group('Sub category first level case', () {
      test('First level: Create', () async {
        await prodDao.create(subCatProduct);
        subCat.products.add(subCatProduct);
        print(instance.dump());
        Product fetched = await prodDao.getById(subCatProduct.id);
        Category fetchedSubCat = await catDao.getByProductId(subCatProduct.id);
        expect(fetched, subCatProduct);
        expect(fetchedSubCat.productsPreviewToJson(),
            subCat.productsPreviewToJson());
      });
      test('First level: Update', () async {
        await prodDao.create(subCatProduct);
        await prodDao.update(toUpdateSubCatProduct);
        subCat.products.add(toUpdateSubCatProduct);

        print(instance.dump());
        Product fetched = await prodDao.getById(toUpdateSubCatProduct.id);
        Category fetchedSubCat =
            await catDao.getByProductId(toUpdateSubCatProduct.id);
        expect(fetched, toUpdateSubCatProduct);
        expect(fetchedSubCat.productsPreviewToJson(),
            subCat.productsPreviewToJson());
      });
      test('First level: Delete', () async {
        await prodDao.create(subCatProduct);
        await prodDao.delete(subCatProduct.id);
        print(instance.dump());
        var exception;
        try {
          await prodDao.getById(subCatProduct.id);
        } catch (e) {
          exception = e;
        }
        Category fetchedSubCat = await catDao.getByProductId(subCatProduct.id);
        expect(exception, isInstanceOf<NoSuchMethodError>());
        expect(fetchedSubCat.productsPreviewToJson(),
            subCat.productsPreviewToJson());
      });
    });
  });
}
