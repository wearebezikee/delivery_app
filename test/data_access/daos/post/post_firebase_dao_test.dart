import 'package:cloud_firestore_mocks/cloud_firestore_mocks.dart';
import 'package:delivery_app/data_access/daos/post/post_firebase_dao.dart';
import 'package:delivery_app/model/posts/category_post.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/model/posts/product_post.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  MockFirestoreInstance instance;
  PostFirebaseDao dao;
  Post post;
  Post post2;
  Post postToUpdate;

  group('Post Firebase Dao', () {
    group('CRUD', () {
      group('Simple case', () {
        setUp(() {
          instance = MockFirestoreInstance();
          dao = PostFirebaseDao(instance);

          post = Post(
              title: "title",
              type: Post.TYPE_SIMPLE,
              description: "description",
              imageUrl: "imageUrl",
              date: DateTime.now());

          postToUpdate = Post(
              title: "title new",
              type: Post.TYPE_SIMPLE,
              description: "description new",
              imageUrl: "imageUrlNew",
              date: DateTime.now());

          post2 = Post(
              title: "title2",
              type: Post.TYPE_SIMPLE,
              description: "description2",
              imageUrl: "imageUrl2",
              date: DateTime.now());
        });
        test('Simple: Create', () async {
          await dao.create(post);
          //print(instance.dump());
          Post fetched = await dao.getById(post.id);
          expect(post, fetched);
        });

        test('Simple: Update', () async {
          post = await dao.create(post);
          postToUpdate.id = post.id;
          await dao.update(postToUpdate);
          //print(instance.dump());
          Post fetched = await dao.getById(postToUpdate.id);
          expect(postToUpdate, fetched);
        });

        test('Simple: Delete', () async {
          post = await dao.create(post);
          //print(instance.dump());
          await dao.delete(post.id);
          var exception;
          try {
            await dao.getById(post.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Simple: Get All', () async {
          post = await dao.create(post);
          post2 = await dao.create(post2);
          //print(instance.dump());
          List<Post> postList = await dao.getAll();
          expect(postList, [post, post2]);
        });
      });

      group('Product case', () {
        setUp(() {
          instance = MockFirestoreInstance();
          dao = PostFirebaseDao(instance);

          post = ProductPost(
              title: "title",
              description: "description",
              imageUrl: "imageUrl",
              productId: "productId",
              date: DateTime.now());

          postToUpdate = ProductPost(
              title: "titleNew",
              description: "descriptionNew",
              imageUrl: "imageUrlNew",
              productId: "productIdNew",
              date: DateTime.now());

          post2 = ProductPost(
              title: "title2",
              description: "description2",
              imageUrl: "imageUrl2",
              productId: "productId2",
              date: DateTime.now());
        });
        test('Product: Create', () async {
          post = await dao.create(post);
          //print(instance.dump());
          Post fetched = await dao.getById(post.id);
          expect(post, fetched);
        });

        test('Product: Update', () async {
          post = await dao.create(post);
          postToUpdate.id = post.id;
          await dao.update(postToUpdate);
          //print(instance.dump());
          Post fetched = await dao.getById(postToUpdate.id);
          expect(postToUpdate, fetched);
        });

        test('Product: Delete', () async {
          post = await dao.create(post);
          //print(instance.dump());
          await dao.delete(post.id);
          var exception;
          try {
            await dao.getById(post.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Product: Get All', () async {
          post = await dao.create(post);
          post2 = await dao.create(post2);
          //print(instance.dump());
          List<Post> postList = await dao.getAll();
          expect(postList, [post, post2]);
        });
      });

      group('Category case', () {
        setUp(() {
          instance = MockFirestoreInstance();
          dao = PostFirebaseDao(instance);

          post = CategoryPost(
              title: "title",
              description: "description",
              imageUrl: "imageUrl",
              categoryId: "categoryId",
              date: DateTime.now());

          postToUpdate = CategoryPost(
              title: "titleNew",
              description: "descriptionNew",
              imageUrl: "imageUrlNew",
              categoryId: "categoryIdNew",
              date: DateTime.now());

          post2 = CategoryPost(
              title: "title2",
              description: "description2",
              imageUrl: "imageUrl2",
              categoryId: "categoryId2",
              date: DateTime.now());
        });
        test('Category: Create', () async {
          post = await dao.create(post);
          //print(instance.dump());
          Post fetched = await dao.getById(post.id);
          expect(post, fetched);
        });

        test('Category: Update', () async {
          post = await dao.create(post);
          postToUpdate.id = post.id;
          await dao.update(postToUpdate);
          //print(instance.dump());
          Post fetched = await dao.getById(postToUpdate.id);
          expect(postToUpdate, fetched);
        });

        test('Category: Delete', () async {
          post = await dao.create(post);
          //print(instance.dump());
          await dao.delete(post.id);
          var exception;
          try {
            await dao.getById(post.id);
          } catch (e) {
            exception = e;
          }
          expect(exception, isInstanceOf<NoSuchMethodError>());
        });
        test('Category: Get All', () async {
          post = await dao.create(post);
          post2 = await dao.create(post2);
          //print(instance.dump());
          List<Post> postList = await dao.getAll();
          expect(postList, [post, post2]);
        });
      });
    });
  });
}
