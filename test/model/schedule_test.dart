import 'package:delivery_app/model/schedule.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  Schedule schedule = Schedule(
    dayOfWeek: Schedule.DAY_OF_WEEK_FRIDAY,
    startHour:
        TimeOfDay.fromDateTime(DateTime(2020, 1, 1, 12, 30, 0, 0, 0)), //12:30pm
    endHour:
        TimeOfDay.fromDateTime(DateTime(2020, 1, 1, 20, 30, 0, 0, 0)), //10:13pm
  );

  group('Schedule Model', () {
    test('isDateInside', () {
      expect(
          schedule.isDateInside(DateTime.parse("2020-06-26 15:50:04Z")), true);
      expect(
          schedule.isDateInside(DateTime.parse("2020-06-26 12:29:59Z")), false);
      expect(
          schedule.isDateInside(DateTime.parse("2020-06-26 20:31:00Z")), false);
      expect(
          schedule.isDateInside(DateTime.parse("2020-06-27 13:31:00Z")), false);
    });
  });
}
