import 'package:delivery_app/model/product.dart';
import 'package:equatable/equatable.dart';

class Category extends Equatable {
  static const String ROOT_ID = "root";

  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_NAME = "name";
  static const String ATTRIBUTE_IMAGE_URL = "imageUrl";
  static const String ATTRIBUTE_CATEGORIES = "categories";
  static const String ATTRIBUTE_PRODUCTS = "products";

  String id;
  String name;
  String imageUrl;
  List<Category> categories;
  List<Product> products;

  Category({this.id, this.name, this.imageUrl, this.categories, this.products});

  Category.empty() {
    this.id = "";
    this.name = "";
    this.imageUrl = "";
    this.categories = List<Category>();
    this.products = List<Product>();
  }

  Category.fromJson(dynamic json) {
    id = json[ATTRIBUTE_ID];
    name = json[ATTRIBUTE_NAME];
    imageUrl = json[ATTRIBUTE_IMAGE_URL];
    categories = _parseCategories(json[ATTRIBUTE_CATEGORIES] ?? {});
    products = _parseProducts(json[ATTRIBUTE_PRODUCTS] ?? {});
  }

  List<Category> _parseCategories(dynamic json) {
    List<Category> output = List<Category>();
    for (int i = 0; i < json.length; i++) {
      var actual = json[i];
      output.add(Category.fromJson(actual));
    }
    return output;
  }

  List<Product> _parseProducts(dynamic json) {
    List<Product> output = List<Product>();
    for (int i = 0; i < json.length; i++) {
      var actual = json[i];
      output.add(Product.fromJson(actual));
    }
    return output;
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_NAME: name,
      ATTRIBUTE_IMAGE_URL: imageUrl,
      ATTRIBUTE_CATEGORIES: _decodeCategories(), //stringify Categories
      ATTRIBUTE_PRODUCTS: _decodeProducts(), //Stringify products
    };
  }

  dynamic toJsonPreview() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_NAME: name,
      ATTRIBUTE_IMAGE_URL: imageUrl,
    };
  }

  List<dynamic> _decodeCategories() {
    List<dynamic> output = List<dynamic>();
    for (int i = 0; i < categories.length; i++) {
      var actual = categories[i];
      output.add(actual.toJson());
    }
    return output;
  }

  List<dynamic> _decodeProducts() {
    List<dynamic> output = List<dynamic>();
    for (int i = 0; i < products.length; i++) {
      var actual = products[i];
      output.add(actual.toJson());
    }
    return output;
  }

  List<dynamic> categoriesPreviewToJson() {
    List<dynamic> output = List<dynamic>();
    for (int i = 0; i < categories.length; i++) {
      var actual = categories[i];
      output.add(actual.toJsonPreview());
    }
    return output;
  }

  List<dynamic> productsPreviewToJson() {
    List<dynamic> output = List<dynamic>();
    for (int i = 0; i < products.length; i++) {
      var actual = products[i];
      output.add(actual.toJsonPreview());
    }
    return output;
  }

  @override
  List<Object> get props => [id, name, imageUrl, categories, products];

  bool isRoot() {
    return id == ROOT_ID;
  }
}
