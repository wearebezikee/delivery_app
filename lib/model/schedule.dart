import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Schedule extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_DAY_OF_WEEK = "dayOfWeek";
  static const String ATTRIBUTE_START_HOUR = "startHour";
  static const String ATTRIBUTE_END_HOUR = "endHour";

  static const String DAY_OF_WEEK_MONDAY = "monday";
  static const String DAY_OF_WEEK_TUESDAY = "tuesday";
  static const String DAY_OF_WEEK_WEDNESDAY = "wednesday";
  static const String DAY_OF_WEEK_THURSDAY = "thursday";
  static const String DAY_OF_WEEK_FRIDAY = "friday";
  static const String DAY_OF_WEEK_SATURDAY = "saturday";
  static const String DAY_OF_WEEK_SUNDAY = "sunday";

  String id;
  String dayOfWeek;
  TimeOfDay startHour;
  TimeOfDay endHour;

  Schedule({this.id, this.dayOfWeek, this.startHour, this.endHour});

  Schedule.empty() {
    id = "";
    dayOfWeek = "";
    startHour = null;
    endHour = null;
  }

  Schedule.fromJson(dynamic json) {
    id = json[ATTRIBUTE_ID];
    dayOfWeek = json[ATTRIBUTE_DAY_OF_WEEK];
    startHour = _parseTimeOfDay(json[ATTRIBUTE_START_HOUR]);
    endHour = _parseTimeOfDay(json[ATTRIBUTE_END_HOUR]);
  }

  TimeOfDay _parseTimeOfDay(String timeOfDay) {
    List<String> aux = timeOfDay.split(":");
    TimeOfDay output =
        TimeOfDay(hour: int.parse(aux[0]), minute: int.parse(aux[1]));
    return output;
  }

  String _decodeTimeOfDay(TimeOfDay timeOfDay) {
    return timeOfDay.hour.toString() + ":" + timeOfDay.minute.toString();
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_DAY_OF_WEEK: dayOfWeek,
      ATTRIBUTE_START_HOUR: _decodeTimeOfDay(startHour),
      ATTRIBUTE_END_HOUR: _decodeTimeOfDay(endHour),
    };
  }

  @override
  List<Object> get props => [id, dayOfWeek, startHour];

  static String intToWeekDay(int weekday) {
    switch (weekday) {
      case 1:
        return DAY_OF_WEEK_MONDAY;
        break;
      case 2:
        return DAY_OF_WEEK_TUESDAY;
        break;
      case 3:
        return DAY_OF_WEEK_WEDNESDAY;
        break;
      case 4:
        return DAY_OF_WEEK_THURSDAY;
        break;
      case 5:
        return DAY_OF_WEEK_FRIDAY;
        break;
      case 6:
        return DAY_OF_WEEK_SATURDAY;
        break;
      case 7:
        return DAY_OF_WEEK_SUNDAY;
        break;
      default:
        return null;
        break;
    }
  }

  bool isDateInside(DateTime input) {
    TimeOfDay inputTimeOfDay = TimeOfDay.fromDateTime(input);
    return _timeOfDayToInt(inputTimeOfDay) >= _timeOfDayToInt(startHour) &&
        _timeOfDayToInt(inputTimeOfDay) <= _timeOfDayToInt(endHour) &&
        intToWeekDay(input.weekday) == dayOfWeek;
  }

  int _timeOfDayToInt(TimeOfDay input) {
    return (input.hour * 100) + input.minute;
  }
}
