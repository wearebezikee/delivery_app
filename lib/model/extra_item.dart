import 'package:equatable/equatable.dart';

class ExtraItem extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_TITLE = "title";
  static const String ATTRIBUTE_PRICE = "price";

  String id;
  String title;
  int price;

  ExtraItem({this.id, this.title, this.price});

  ExtraItem.fromJson(dynamic json) {
    id = json[ATTRIBUTE_ID];
    title = json[ATTRIBUTE_TITLE];
    price = json[ATTRIBUTE_PRICE];
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_TITLE: title,
      ATTRIBUTE_PRICE: price,
    };
  }

  @override
  List<Object> get props => [id, title, price];
}
