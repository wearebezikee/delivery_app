import 'package:delivery_app/model/product.dart';
import 'package:equatable/equatable.dart';

class CartItem extends Equatable {
  static const String ATTRIBUTE_PRODUCT = "product";
  static const String ATTRIBUTE_QUANTITY = "quantity";

  Product product;
  int quantity;

  CartItem({this.product, this.quantity});

  CartItem.fromJson(dynamic json) {
    product = Product.fromJson(json[ATTRIBUTE_PRODUCT]);
    quantity = json[ATTRIBUTE_QUANTITY];
  }

  dynamic toJson() {
    return {ATTRIBUTE_PRODUCT: product.toJson(), ATTRIBUTE_QUANTITY: quantity};
  }

  @override
  List<Object> get props => [product, quantity];
}
