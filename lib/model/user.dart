import 'package:equatable/equatable.dart';

import 'address.dart';

class User extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_NAME = "name";
  static const String ATTRIBUTE_EMAIL = "email";
  static const String ATTRIBUTE_IMAGE_URL = "imageUrl";
  static const String ATTRIBUTE_ADDRESSES = "addresses";
  static const String ATTRIBUTE_PHONE_NUMBER = "phoneNumber";
  static const String ATTRIBUTE_ROLE = "role";

  static const String ROLE_CLIENT = "client";
  static const String ROLE_ADMIN = "admin";

  String id;
  String name;
  String email;
  String imageUrl;
  List<Address> addresses;
  String phoneNumber;
  String role;

  User({
    this.id,
    this.name,
    this.email,
    this.imageUrl,
    this.addresses,
    this.phoneNumber,
    this.role,
  });

  User.empty() {
    id = "";
    name = "";
    email = "";
    imageUrl = "";
    addresses = null;
    phoneNumber = "";
    role = "";
  }

  User.fromJson(dynamic json) {
    id = json[ATTRIBUTE_ID];
    name = json[ATTRIBUTE_NAME];
    email = json[ATTRIBUTE_EMAIL];
    imageUrl = json[ATTRIBUTE_IMAGE_URL];
    addresses = _parseAddresses(json[ATTRIBUTE_ADDRESSES]);
    phoneNumber = json[ATTRIBUTE_PHONE_NUMBER];
    role = json[ATTRIBUTE_ROLE];
  }

  List<Address> _parseAddresses(dynamic json) {
    List<Address> output = List<Address>();
    for (int i = 0; i < json.length; i++) {
      var actual = json[i];
      output.add(Address.fromJson(actual));
    }
    return output;
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_NAME: name,
      ATTRIBUTE_EMAIL: email,
      ATTRIBUTE_IMAGE_URL: imageUrl,
      ATTRIBUTE_ADDRESSES: _decodeAddresses(),
      ATTRIBUTE_PHONE_NUMBER: phoneNumber,
      ATTRIBUTE_ROLE: role,
    };
  }

  List<dynamic> _decodeAddresses() {
    List<dynamic> output = List<dynamic>();
    for (int i = 0; i < addresses.length; i++) {
      var actual = addresses[i];
      output.add(actual.toJson());
    }
    return output;
  }

  @override
  List<Object> get props => [id, name, email, imageUrl, addresses, phoneNumber, role];

  void clone(User toClone) {
    id = toClone.id;
    name = toClone.name;
    email = toClone.email;
    imageUrl = toClone.imageUrl;
    addresses = toClone.addresses;
    phoneNumber = toClone.phoneNumber;
    role = toClone.role;
  }

  bool isEmptyLoggedUser() {
    return name == "" || imageUrl == null || addresses == null || phoneNumber == "" || role == "";
  }
}
