import 'package:equatable/equatable.dart';

abstract class Payment extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_TYPE = "type";

  static const String TYPE_ZELLE = "Zelle";
  static const String TYPE_TRANSFER = "Transferencia";
  static const String TYPE_CASH = "Efectivo";
  static const String TYPE_PAGO_MOVIL = "Pago Movil";

  Payment({this.id, this.type});

  String id;
  String type;

  dynamic toJson();
}
