import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method/payment_method.dart';

class PagoMovilPaymentMethod extends PaymentMethod {
  static const String ATTRIBUTE_CONFIRMATION_IMAGE_URL = "confirmationImageUrl";
  static const String ATTRIBUTE_REFERENCE_NUMBER = "referenceNumber";

  PagoMovilPaymentMethod(
      {String id, this.confirmationImageUrl, this.referenceNumber})
      : super(id: id, type: Payment.TYPE_PAGO_MOVIL);

  String confirmationImageUrl;
  String referenceNumber;

  PagoMovilPaymentMethod.fromJson(dynamic json) {
    id = json[Payment.ATTRIBUTE_ID];
    type = json[Payment.ATTRIBUTE_TYPE];
    confirmationImageUrl = json[ATTRIBUTE_CONFIRMATION_IMAGE_URL];
    referenceNumber = json[ATTRIBUTE_REFERENCE_NUMBER];
  }

  @override
  dynamic toJson() {
    return {
      Payment.ATTRIBUTE_ID: id,
      Payment.ATTRIBUTE_TYPE: type,
      ATTRIBUTE_CONFIRMATION_IMAGE_URL: confirmationImageUrl,
      ATTRIBUTE_REFERENCE_NUMBER: referenceNumber
    };
  }

  @override
  List<Object> get props => [id, type, confirmationImageUrl, referenceNumber];
}
