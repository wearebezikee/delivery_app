import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method/payment_method.dart';

class ZellePaymentMethod extends PaymentMethod {
  static const String ATTRIBUTE_CONFIRMATION_IMAGE_URL = "confirmationImageUrl";

  String confirmationImageUrl;

  ZellePaymentMethod({String id, this.confirmationImageUrl})
      : super(id: id, type: Payment.TYPE_ZELLE);

  ZellePaymentMethod.fromJson(dynamic json) {
    id = json[Payment.ATTRIBUTE_ID];
    type = json[Payment.ATTRIBUTE_TYPE];
    confirmationImageUrl = json[ATTRIBUTE_CONFIRMATION_IMAGE_URL];
  }

  @override
  dynamic toJson() {
    return {
      Payment.ATTRIBUTE_ID: id,
      Payment.ATTRIBUTE_TYPE: type,
      ATTRIBUTE_CONFIRMATION_IMAGE_URL: confirmationImageUrl,
    };
  }

  @override
  List<Object> get props => [id, type, confirmationImageUrl];
}
