import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method/payment_method.dart';

class CashPaymentMethod extends PaymentMethod {
  static const String ATTRIBUTE_CASH_AMOUNT = "cashAmount";

  int cashAmount;

  CashPaymentMethod({String id, this.cashAmount})
      : super(id: id, type: Payment.TYPE_CASH);

  CashPaymentMethod.fromJson(dynamic json) {
    id = json[Payment.ATTRIBUTE_ID];
    type = json[Payment.ATTRIBUTE_TYPE];
    cashAmount = json[ATTRIBUTE_CASH_AMOUNT];
  }

  @override
  dynamic toJson() {
    return {
      Payment.ATTRIBUTE_ID: id,
      Payment.ATTRIBUTE_TYPE: type,
      ATTRIBUTE_CASH_AMOUNT: cashAmount,
    };
  }

  @override
  List<Object> get props => [id, type, cashAmount];
}
