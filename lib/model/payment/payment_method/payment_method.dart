import 'package:delivery_app/model/payment/payment.dart';

abstract class PaymentMethod extends Payment {
  PaymentMethod({String id, String type}) : super(id: id, type: type);
}
