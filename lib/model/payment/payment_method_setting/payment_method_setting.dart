import 'package:delivery_app/model/payment/payment.dart';

abstract class PaymentMethodSetting extends Payment {
  static const String ATTRIBUTE_ACTIVE = "active";
  static const String ATTRIBUTE_TITLE = "title";

  bool active;
  String title;

  PaymentMethodSetting({String id, String type, this.active, this.title})
      : super(id: id, type: type);
}
