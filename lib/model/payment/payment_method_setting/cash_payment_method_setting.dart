import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';

class CashPaymentMethodSetting extends PaymentMethodSetting {
  CashPaymentMethodSetting({String id, bool active, String title})
      : super(id: id, type: Payment.TYPE_CASH, active: active, title: title);

  CashPaymentMethodSetting.fromJson(dynamic json) {
    id = json[Payment.ATTRIBUTE_ID];
    type = json[Payment.ATTRIBUTE_TYPE];
    active = json[PaymentMethodSetting.ATTRIBUTE_ACTIVE];
    title = json[PaymentMethodSetting.ATTRIBUTE_TITLE];
  }

  @override
  dynamic toJson() {
    return {
      Payment.ATTRIBUTE_ID: id,
      Payment.ATTRIBUTE_TYPE: type,
      PaymentMethodSetting.ATTRIBUTE_ACTIVE: active,
      PaymentMethodSetting.ATTRIBUTE_TITLE: title,
    };
  }

  @override
  List<Object> get props => [id, type, active, title];
}
