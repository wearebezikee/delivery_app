import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';

class ZellePaymentMethodSetting extends PaymentMethodSetting {
  static const String ATTRIBUTE_EMAIL = "email";
  static const String ATTRIBUTE_DESCRIPTION = "description";

  String email;
  String description;

  ZellePaymentMethodSetting({
    String id,
    bool active,
    String title,
    this.email,
    this.description,
  }) : super(id: id, type: Payment.TYPE_ZELLE, active: active, title: title);

  ZellePaymentMethodSetting.fromJson(dynamic json) {
    id = json[Payment.ATTRIBUTE_ID];
    type = json[Payment.ATTRIBUTE_TYPE];
    active = json[PaymentMethodSetting.ATTRIBUTE_ACTIVE];
    title = json[PaymentMethodSetting.ATTRIBUTE_TITLE];
    email = json[ATTRIBUTE_EMAIL];
    description = json[ATTRIBUTE_DESCRIPTION];
  }

  @override
  dynamic toJson() {
    return {
      Payment.ATTRIBUTE_ID: id,
      Payment.ATTRIBUTE_TYPE: type,
      PaymentMethodSetting.ATTRIBUTE_ACTIVE: active,
      PaymentMethodSetting.ATTRIBUTE_TITLE: title,
      ATTRIBUTE_EMAIL: email,
      ATTRIBUTE_DESCRIPTION: description,
    };
  }

  @override
  List<Object> get props => [id, type, active, title, email, description];
}
