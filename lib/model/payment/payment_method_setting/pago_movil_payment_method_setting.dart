import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';

class PagoMovilPaymentMethodSetting extends PaymentMethodSetting {
  static const String ATTRIBUTE_BANK_NAME = "bankName";
  static const String ATTRIBUTE_PHONE_NUMBER = "phoneNumber";
  static const String ATTRIBUTE_ID_NUMBER = "idNumber";
  static const String ATTRIBUTE_EMAIL = "email";

  String bankName;
  String phoneNumber;
  int idNumber;
  String email;

  PagoMovilPaymentMethodSetting(
      {String id,
      bool active,
      String title,
      this.bankName,
      this.phoneNumber,
      this.idNumber,
      this.email})
      : super(
            id: id,
            type: Payment.TYPE_PAGO_MOVIL,
            active: active,
            title: title);

  PagoMovilPaymentMethodSetting.fromJson(dynamic json) {
    id = json[Payment.ATTRIBUTE_ID];
    type = json[Payment.ATTRIBUTE_TYPE];
    active = json[PaymentMethodSetting.ATTRIBUTE_ACTIVE];
    title = json[PaymentMethodSetting.ATTRIBUTE_TITLE];
    bankName = json[ATTRIBUTE_BANK_NAME];
    phoneNumber = json[ATTRIBUTE_PHONE_NUMBER];
    idNumber = json[ATTRIBUTE_ID_NUMBER];
    email = json[ATTRIBUTE_EMAIL];
  }

  @override
  dynamic toJson() {
    return {
      Payment.ATTRIBUTE_ID: id,
      Payment.ATTRIBUTE_TYPE: type,
      PaymentMethodSetting.ATTRIBUTE_ACTIVE: active,
      PaymentMethodSetting.ATTRIBUTE_TITLE: title,
      ATTRIBUTE_BANK_NAME: bankName,
      ATTRIBUTE_PHONE_NUMBER: phoneNumber,
      ATTRIBUTE_ID_NUMBER: idNumber,
      ATTRIBUTE_EMAIL: email,
    };
  }

  @override
  List<Object> get props =>
      [id, type, active, title, bankName, phoneNumber, idNumber, email];
}
