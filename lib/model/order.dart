import 'package:delivery_app/domain_logic/factories/payment_method_factory.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/extra_item.dart';
import 'package:delivery_app/model/order_status.dart';
import 'package:delivery_app/model/payment/payment_method/payment_method.dart';
import 'package:equatable/equatable.dart';

class Order extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_USER_ID = "userId";
  static const String ATTRIBUTE_USER_NAME = "userName";
  static const String ATTRIBUTE_PHONE = "userPhone";
  static const String ATTRIBUTE_ADDRESS = "address";
  static const String ATTRIBUTE_PICKUP = "pickup";
  static const String ATTRIBUTE_PRODUCTS = "products";
  static const String ATTRIBUTE_PAYMENT_METHOD = "paymentMethod";
  static const String ATTRIBUTE_EXTRAS = "extras";
  static const String ATTRIBUTE_TOTAL_PRICE = "totalPrice";
  static const String ATTRIBUTE_STATUS_HISTORY = "statusHistory";
  static const String ATTRIBUTE_OBSERVATION = "observation";
  static const String ATTRIBUTE_TIP = "tip";

  String id;
  String userId;
  String userName;
  String userPhone;
  Address address;
  bool pickup;

  List<CartItem> products;

  PaymentMethod paymentMethod;
  //TODO solve empty
  List<ExtraItem> extras;

  double totalPrice;
  List<OrderStatus> statusHistory;

  String observation;
  double tip;

  Order(
      {this.id,
      this.userId,
      this.userName,
      this.userPhone,
      this.address,
      this.pickup,
      this.products,
      this.paymentMethod,
      this.extras,
      this.totalPrice,
      this.statusHistory,
      this.observation,
      this.tip});

  Order.fromJson(dynamic json) {
    this.id = json[ATTRIBUTE_ID];
    this.userId = json[ATTRIBUTE_USER_ID];
    this.userName = json[ATTRIBUTE_USER_NAME];
    this.userPhone = json[ATTRIBUTE_PHONE];
    this.address = json[ATTRIBUTE_ADDRESS] != null
        ? Address.fromJson(json[ATTRIBUTE_ADDRESS])
        : null;
    this.pickup = json[ATTRIBUTE_PICKUP];
    this.products = _parseProducts(json[ATTRIBUTE_PRODUCTS]);
    this.paymentMethod =
        PaymentMethodFactory.instance(json[ATTRIBUTE_PAYMENT_METHOD]);
    this.extras = _parseExtras(json[ATTRIBUTE_EXTRAS]);
    this.totalPrice = json[ATTRIBUTE_TOTAL_PRICE];
    this.statusHistory = _parseStatusHistory(json[ATTRIBUTE_STATUS_HISTORY]);
    this.observation = json[ATTRIBUTE_OBSERVATION];
    this.tip = json[ATTRIBUTE_TIP];
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_USER_ID: userId,
      ATTRIBUTE_USER_NAME: userName,
      ATTRIBUTE_PHONE: userPhone,
      ATTRIBUTE_ADDRESS: address.toJson(),
      ATTRIBUTE_PICKUP: pickup,
      ATTRIBUTE_PRODUCTS: _decodeProducts(),
      ATTRIBUTE_PAYMENT_METHOD: paymentMethod.toJson(),
      ATTRIBUTE_EXTRAS: _decodeExtras(),
      ATTRIBUTE_TOTAL_PRICE: totalPrice,
      ATTRIBUTE_STATUS_HISTORY: _decodeStatusHistory(),
      ATTRIBUTE_OBSERVATION: observation,
      ATTRIBUTE_TIP: tip,
    };
  }

  List<CartItem> _parseProducts(List<dynamic> json) {
    List<CartItem> output = List<CartItem>();
    for (dynamic actual in json) {
      output.add(CartItem.fromJson(actual));
    }
    return output;
  }

  List<ExtraItem> _parseExtras(List<dynamic> json) {
    List<ExtraItem> output = List<ExtraItem>();
    for (dynamic actual in json) {
      output.add(ExtraItem.fromJson(actual));
    }
    return output;
  }

  List<OrderStatus> _parseStatusHistory(List<dynamic> json) {
    List<OrderStatus> output = List<OrderStatus>();
    for (dynamic actual in json) {
      output.add(OrderStatus.fromJson(actual));
    }
    return output;
  }

  List<dynamic> _decodeProducts() {
    List<dynamic> output = List<dynamic>();
    for (CartItem actual in products) {
      output.add(actual.toJson());
    }
    return output;
  }

  List<dynamic> _decodeExtras() {
    if (extras == null) {
      return List();
    }
    List<dynamic> output = List<dynamic>();
    for (ExtraItem actual in extras) {
      output.add(actual.toJson());
    }
    return output;
  }

  List<dynamic> _decodeStatusHistory() {
    List<dynamic> output = List<dynamic>();
    for (OrderStatus actual in statusHistory) {
      output.add(actual.toJson());
    }
    return output;
  }

  @override
  List<Object> get props => [
        id,
        userId,
        userName,
        userPhone,
        address,
        pickup,
        products,
        paymentMethod,
        extras,
        totalPrice,
        statusHistory,
        observation,
        tip
      ];
}
