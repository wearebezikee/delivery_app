import 'package:equatable/equatable.dart';

class Address extends Equatable {
  static const String ATTRIBUTE_ADDRESS1 = "address1";
  static const String ATTRIBUTE_ADDRESS2 = "address2";
  static const String ATTRIBUTE_STREET = "street";
  static const String ATTRIBUTE_TOWN = "town";
  static const String ATTRIBUTE_CITY = "city";

  String address1;
  String address2;
  String street;
  String town;
  String city;

  Address({
    this.address1,
    this.address2,
    this.street,
    this.town,
    this.city,
  });

  Address.fromJson(dynamic json) {
    address1 = json[ATTRIBUTE_ADDRESS1];
    address2 = json[ATTRIBUTE_ADDRESS2];
    street = json[ATTRIBUTE_STREET];
    town = json[ATTRIBUTE_TOWN];
    city = json[ATTRIBUTE_CITY];
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ADDRESS1: address1,
      ATTRIBUTE_ADDRESS2: address2,
      ATTRIBUTE_STREET: street,
      ATTRIBUTE_TOWN: town,
      ATTRIBUTE_CITY: city,
    };
  }

  @override
  List<Object> get props => [address1, address2, street, town, city];
}
