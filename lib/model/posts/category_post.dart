import 'package:delivery_app/model/posts/post.dart';

class CategoryPost extends Post {
  static const String ATTRIBUTE_CATEGORY_ID = "categoryId";

  String categoryId;

  CategoryPost(
      {String id,
      String title,
      String description,
      String imageUrl,
      DateTime date,
      this.categoryId})
      : super(
            id: id,
            type: Post.TYPE_CATEGORY,
            title: title,
            description: description,
            imageUrl: imageUrl,
            date: date);

  CategoryPost.fromJson(dynamic json) : super.fromJson(json) {
    this.categoryId = json[ATTRIBUTE_CATEGORY_ID];
  }

  dynamic toJson() {
    return {
      Post.ATTRIBUTE_ID: id,
      Post.ATTRIBUTE_TYPE: type,
      Post.ATTRIBUTE_TITLE: title,
      Post.ATTRIBUTE_DESCRIPTION: description,
      Post.ATTRIBUTE_IMAGE_URL: imageUrl,
      Post.ATTRIBUTE_DATE: date.toString(),
      ATTRIBUTE_CATEGORY_ID: categoryId
    };
  }

  @override
  List<Object> get props =>
      [id, type, title, description, imageUrl, date, categoryId];
}
