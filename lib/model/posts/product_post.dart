import 'package:delivery_app/model/posts/post.dart';

class ProductPost extends Post {
  static const String ATTRIBUTE_PRODUCT_ID = "productId";

  String productId;

  ProductPost(
      {String id,
      String title,
      String description,
      String imageUrl,
      DateTime date,
      this.productId})
      : super(
            id: id,
            type: Post.TYPE_PRODUCT,
            title: title,
            description: description,
            imageUrl: imageUrl,
            date: date);

  ProductPost.fromJson(dynamic json) : super.fromJson(json) {
    this.productId = json[ATTRIBUTE_PRODUCT_ID];
  }

  dynamic toJson() {
    return {
      Post.ATTRIBUTE_ID: id,
      Post.ATTRIBUTE_TYPE: type,
      Post.ATTRIBUTE_TITLE: title,
      Post.ATTRIBUTE_DESCRIPTION: description,
      Post.ATTRIBUTE_IMAGE_URL: imageUrl,
      Post.ATTRIBUTE_DATE: date.toString(),
      ATTRIBUTE_PRODUCT_ID: productId
    };
  }

  @override
  List<Object> get props =>
      [id, type, title, description, imageUrl, date, productId];
}
