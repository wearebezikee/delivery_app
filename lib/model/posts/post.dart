import 'package:equatable/equatable.dart';

class Post extends Equatable {
  static const String TYPE_SIMPLE = "simple";
  static const String TYPE_PRODUCT = "productPost";
  static const String TYPE_CATEGORY = "categoryPost";

  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_TYPE = "type";
  static const String ATTRIBUTE_TITLE = "title";
  static const String ATTRIBUTE_DESCRIPTION = "description";
  static const String ATTRIBUTE_IMAGE_URL = "imageUrl";
  static const String ATTRIBUTE_DATE = "date";

  String id;
  String type;
  String title;
  String description;
  String imageUrl;
  DateTime date;

  Post.empty() {
    this.id = "";
    this.type = "";
    this.title = "";
    this.description = "";
    this.imageUrl = "";
    this.date = null;
  }

  Post(
      {this.id,
      String type,
      this.title,
      this.description,
      this.imageUrl,
      this.date})
      : this.type = type == null ? TYPE_SIMPLE : type;

  Post.fromJson(dynamic json) {
    this.id = json[ATTRIBUTE_ID];
    this.type = json[ATTRIBUTE_TYPE];
    this.title = json[ATTRIBUTE_TITLE];
    this.description = json[ATTRIBUTE_DESCRIPTION];
    this.imageUrl = json[ATTRIBUTE_IMAGE_URL];
    this.date = DateTime.parse(json[ATTRIBUTE_DATE]);
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_TYPE: type,
      ATTRIBUTE_TITLE: title,
      ATTRIBUTE_DESCRIPTION: description,
      ATTRIBUTE_IMAGE_URL: imageUrl,
      ATTRIBUTE_DATE: date.toString(),
    };
  }

  @override
  List<Object> get props => [id, type, title, description, imageUrl, date];
}
