import 'package:equatable/equatable.dart';

class OrderStatus extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_PRIORITY = "priority";
  static const String ATTRIBUTE_STATUS = "status";
  static const String ATTRIBUTE_TITLE = "title";
  static const String ATTRIBUTE_DATE = "date";

  static const String STATUS_SUCCESS = "success";
  static const String STATUS_PENDING = "pending";
  static const String STATUS_FAILED = "failed";

  //static const String STATUS_IN_PROGRESS = "inProgress";??

  String id;
  int priority;
  String title;
  String status;
  DateTime date;

  OrderStatus({this.id, this.priority, this.title, this.status, this.date});

  OrderStatus.fromJson(dynamic json) {
    this.id = json[ATTRIBUTE_ID];
    this.priority = json[ATTRIBUTE_PRIORITY];
    this.title = json[ATTRIBUTE_TITLE];
    this.status = json[ATTRIBUTE_STATUS];
    this.date = DateTime.parse(json[ATTRIBUTE_DATE]);
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_PRIORITY: priority,
      ATTRIBUTE_TITLE: title,
      ATTRIBUTE_STATUS: status,
      ATTRIBUTE_DATE: date.toString(),
    };
  }

  @override
  List<Object> get props => [id, priority, title, status, date];
}
