import 'package:equatable/equatable.dart';

class Product extends Equatable {
  static const String ATTRIBUTE_ID = "id";
  static const String ATTRIBUTE_TITLE = "title";
  static const String ATTRIBUTE_IMAGE_URL = "imageUrl";
  static const String ATTRIBUTE_PRICE = "price";
  static const String ATTRIBUTE_DESCRIPTION = "description";

  String id;
  String title;
  String imageUrl;
  double price;
  String description;

  Product({this.id, this.title, this.imageUrl, this.price, this.description});

  Product.empty() {
    id = "";
    title = "";
    imageUrl = "";
    price = 0;
    description = "";
  }

  Product.fromJson(dynamic json) {
    id = json[ATTRIBUTE_ID];
    title = json[ATTRIBUTE_TITLE];
    imageUrl = json[ATTRIBUTE_IMAGE_URL];
    price = json[ATTRIBUTE_PRICE].toDouble();
    description = json[ATTRIBUTE_DESCRIPTION];
  }

  dynamic toJson() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_TITLE: title,
      ATTRIBUTE_IMAGE_URL: imageUrl,
      ATTRIBUTE_PRICE: price,
      ATTRIBUTE_DESCRIPTION: description,
    };
  }

  dynamic toJsonPreview() {
    return {
      ATTRIBUTE_ID: id,
      ATTRIBUTE_TITLE: title,
      ATTRIBUTE_IMAGE_URL: imageUrl,
      ATTRIBUTE_PRICE: price
    };
  }

  @override
  List<Object> get props => [id, title, imageUrl, price, description];
}
