import 'package:delivery_app/domain_logic/process/authentication/firebase/firebase_authentication_process.dart';
import 'package:firebase_auth/firebase_auth.dart';

class EmailSignUpFirebaseAuthenticationProcess extends FirebaseAuthenticationProcess {
  @override
  Future<String> process({String email, String password}) async {
    AuthResult result =
        await firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);

    FirebaseUser user = result.user;
    return user.uid;
  }
}
