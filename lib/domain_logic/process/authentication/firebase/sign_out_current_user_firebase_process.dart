import 'package:delivery_app/domain_logic/process/authentication/firebase/firebase_process.dart';

class SignOutCurrentUserFirebaseProcess extends FirebaseProcess {
  Future<void> process() async {
    await firebaseAuth.signOut();
  }
}
