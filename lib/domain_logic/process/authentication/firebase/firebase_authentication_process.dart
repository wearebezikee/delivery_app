import 'package:delivery_app/domain_logic/process/authentication/firebase/firebase_process.dart';

abstract class FirebaseAuthenticationProcess extends FirebaseProcess {
  Future<String> process({String email, String password});
}
