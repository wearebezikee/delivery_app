import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'firebase_authentication_process.dart';

class GoogleFirebaseAuthenticationProcess extends FirebaseAuthenticationProcess {
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  @override
  Future<String> process({String email, String password}) async {
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final FirebaseUser user = (await firebaseAuth.signInWithCredential(credential)).user;
    
    return user.uid;
  }
}
