import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/domain_logic/process/authentication/firebase/firebase_process.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/user.dart';
import 'package:firebase_auth/firebase_auth.dart';

class GetCurrentUserFirebaseProcess extends FirebaseProcess {
  User output = User.empty();
  UserRepository _userRepository = locator<UserRepository>();

  Future<User> process() async {
    FirebaseUser current = await firebaseAuth.currentUser();

    AppResponse<User> userResponse =
        await _userRepository.getFullUser(current.uid);

    if (userResponse.isSuccess()) {
      return userResponse.payload;
    } else {
      output.id = current.uid;
      output.name = current.displayName;
      output.imageUrl = current.photoUrl;
      output.email = current.email;
      output.phoneNumber = current.phoneNumber;
    }

    return output;
  }
}
