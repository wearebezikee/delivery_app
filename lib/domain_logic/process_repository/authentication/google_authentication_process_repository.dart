import 'package:delivery_app/domain_logic/process/authentication/firebase/google_firebase_authentication_process.dart';
import 'package:delivery_app/misc/app_response.dart';

import 'authentication_process_repository.dart';

class GoogleAuthenticationProcessRepository extends AuthenticationProcessRepository {
  GoogleFirebaseAuthenticationProcess _process = GoogleFirebaseAuthenticationProcess();

  @override
  Future<AppResponse<String>> process({String email, String password}) async {
    try {
      return AppResponse.success(await _process.process());
    } catch (e) {
      return AppResponse.failure(e);
    }
  }
}
