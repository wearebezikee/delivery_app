import 'package:delivery_app/domain_logic/process/authentication/firebase/email_sign_up_firebase__authentication_process.dart';
import 'package:delivery_app/misc/app_response.dart';

import 'authentication_process_repository.dart';

class EmailSignUpAuthenticationProcessRepository extends AuthenticationProcessRepository {
  EmailSignUpFirebaseAuthenticationProcess _process = EmailSignUpFirebaseAuthenticationProcess();

  @override
  Future<AppResponse<String>> process({String email, String password}) async {
    try {
      return AppResponse.success(await _process.process(email: email, password: password));
    } catch (e) {
      return AppResponse.failure(e);
    }
  }
}
