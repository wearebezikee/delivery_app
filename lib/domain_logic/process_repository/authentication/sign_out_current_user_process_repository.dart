import 'package:delivery_app/domain_logic/process/authentication/firebase/sign_out_current_user_firebase_process.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/model/user.dart';

class SignOutCurrentUserProcessRepository {
  SignOutCurrentUserFirebaseProcess _process = SignOutCurrentUserFirebaseProcess();

  Future<AppResponse<void>> process() async {
    try {
      await _process.process();
      LoggedUserHelper.setLoggedUser(User.empty());
      return AppResponse.success(null);
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
