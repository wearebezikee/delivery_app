import 'package:delivery_app/domain_logic/process/authentication/firebase/get_current_user_firebase_process.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/user.dart';

class GetCurrentUserProcessRepository {
  GetCurrentUserFirebaseProcess _process = GetCurrentUserFirebaseProcess();

  Future<AppResponse<User>> process() async {
    try {
      return AppResponse<User>.success(await _process.process());
    } catch (error) {
      return AppResponse<User>.failure(error);
    }
  }
}
