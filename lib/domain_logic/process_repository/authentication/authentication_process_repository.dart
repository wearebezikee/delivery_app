import 'package:delivery_app/misc/app_response.dart';

abstract class AuthenticationProcessRepository {
  Future<AppResponse<String>> process({String email, String password});
}
