import 'package:delivery_app/data_access/repositories/schedule/schedule_repository.dart';
import 'package:delivery_app/domain_logic/services/service.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/schedule.dart';

class IsShopOpenService extends Service {
  ScheduleRepository _scheduleRepository = locator<ScheduleRepository>();
  DateTime _currentDate = DateTime.now();

  @override
  Future<bool> process() async {
    AppResponse<List<Schedule>> scheduleResponse =
        await _scheduleRepository.getAll();

    if (scheduleResponse.isSuccess()) {
      for (int i = 0; i < scheduleResponse.payload.length; i++) {
        Schedule current = scheduleResponse.payload[i];
        if (current.isDateInside(_currentDate)) {
          return true;
        }
      }
    }

    return false;
  }
}
