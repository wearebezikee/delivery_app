import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/authentication_process_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/app_navigator/app_navigator.dart';
import 'package:flutter/material.dart';

import '../service.dart';

class AuthenticationService extends Service {
  UserRepository _userRepository = locator<UserRepository>();
  AppNavigator _appNavigator = locator<AppNavigator>();

  Future<void> process(
      {@required
          AuthenticationProcessRepository authenticationProcessRepository,
      String email,
      String password}) async {
    AppResponse<String> authResponse = await authenticationProcessRepository
        .process(email: email, password: password);
    if (authResponse.isSuccess()) {
      AppResponse<User> userResponse =
          await _userRepository.getFullUser(authResponse.payload);
      if (userResponse.isSuccess()) {
        locator<User>().clone(userResponse.payload);
        _appNavigator.toDashboard();
      } else {
        _appNavigator.toCreateUserForm(authResponse.payload);
      }
    } else {
      MyNotification.showError(subtitle: authResponse.error.toString());
    }
  }
}
