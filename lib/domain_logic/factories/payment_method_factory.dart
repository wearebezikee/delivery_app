import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method/cash_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/pago_movil_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/transfer_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/zelle_payment_method.dart';

class PaymentMethodFactory {
  static PaymentMethod instance(dynamic json) {
    switch (json[Payment.ATTRIBUTE_TYPE]) {
      case Payment.TYPE_ZELLE:
        return ZellePaymentMethod.fromJson(json);
        break;
      case Payment.TYPE_TRANSFER:
        return TransferPaymentMethod.fromJson(json);
        break;
      case Payment.TYPE_CASH:
        return CashPaymentMethod.fromJson(json);
        break;
      case Payment.TYPE_PAGO_MOVIL:
        return PagoMovilPaymentMethod.fromJson(json);
        break;
    }
  }
}
