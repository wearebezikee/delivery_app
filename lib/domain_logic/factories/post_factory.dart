import 'package:delivery_app/model/posts/category_post.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/model/posts/product_post.dart';

class PostFactory {
  static Post instance(dynamic json) {
    switch (json[Post.ATTRIBUTE_TYPE]) {
      case Post.TYPE_SIMPLE:
        return Post.fromJson(json);
        break;
      case Post.TYPE_PRODUCT:
        return ProductPost.fromJson(json);
        break;
      case Post.TYPE_CATEGORY:
        return CategoryPost.fromJson(json);
        break;
    }
  }
}
