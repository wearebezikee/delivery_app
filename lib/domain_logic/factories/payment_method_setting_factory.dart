import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/cash_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/pago_movil_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/transfer_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/zelle_payment_method_setting.dart';

class PaymentMethodSettingFactory {
  static PaymentMethodSetting instance(dynamic json) {
    switch (json[Payment.ATTRIBUTE_TYPE]) {
      case Payment.TYPE_ZELLE:
        return ZellePaymentMethodSetting.fromJson(json);
        break;
      case Payment.TYPE_TRANSFER:
        return TransferPaymentMethodSetting.fromJson(json);
        break;
      case Payment.TYPE_CASH:
        return CashPaymentMethodSetting.fromJson(json);
        break;
      case Payment.TYPE_PAGO_MOVIL:
        return PagoMovilPaymentMethodSetting.fromJson(json);
        break;
    }
  }
}
