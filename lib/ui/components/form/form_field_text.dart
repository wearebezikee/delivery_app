import 'package:flutter/material.dart';

class FormFieldText extends StatelessWidget {
  final Widget preffix;
  final Widget suffix;
  final Function onTap;
  final String hint;
  final bool obscureText;
  final Color backgroundColor;
  final EdgeInsetsGeometry contentPadding;
  final TextEditingController textController;
  final int maxLines;

  const FormFieldText({
    this.obscureText,
    this.preffix,
    this.suffix,
    this.onTap,
    this.hint,
    this.textController,
    this.backgroundColor,
    this.contentPadding,
    this.maxLines = 1,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor ?? Theme.of(context).colorScheme.onSurface,
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: TextField(
        autocorrect: false,
        controller: textController,
        obscureText: obscureText ?? false,
        style: Theme.of(context).textTheme.headline2,
        keyboardType: TextInputType.text,
        maxLines: maxLines,
        onTap: onTap,
        decoration: InputDecoration(
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          hintText: hint,
          hintStyle: Theme.of(context).textTheme.caption,
          border: InputBorder.none,
          prefixIcon: preffix,
          suffixIcon: suffix,
          contentPadding: contentPadding,
        ),
      ),
    );
  }
}
