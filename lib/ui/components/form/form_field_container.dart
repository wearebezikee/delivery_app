import 'package:flutter/material.dart';

class FormFieldContainer extends StatelessWidget {
  final Widget child;
  final bool withDivider;
  final EdgeInsets margin;
  final double height;

  const FormFieldContainer({
    this.child,
    this.withDivider = false,
    this.margin,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: margin,
            height: height ?? MediaQuery.of(context).size.height * 0.07,
            child: child ?? Container(),
          ),
          withDivider
              ? Divider(
                  color: Color(0xffF1F3F8),
                  thickness: 1,
                  height: 0,
                )
              : Container(),
        ],
      ),
    );
  }
}
