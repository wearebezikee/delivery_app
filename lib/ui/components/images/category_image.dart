import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CategoryImage extends StatelessWidget {
  final String imageUrl;
  final File fileImage;
  static const String _DEFAULT_CATEGORY_URL =
      "https://firebasestorage.googleapis.com/v0/b/develop-delivery-app.appspot.com/o/images%2Fcategories%2Froot.png?alt=media&token=63b15bf3-3347-4ba6-9b3c-e279368ce265";

  CategoryImage(this.imageUrl) : this.fileImage = null;

  CategoryImage.fromFileImage(this.fileImage) : this.imageUrl = "";

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundImage: fileImage != null
          ? FileImage(fileImage)
          : this.imageUrl != ""
              ? CachedNetworkImageProvider(imageUrl)
              : CachedNetworkImageProvider(_DEFAULT_CATEGORY_URL),
      backgroundColor: Colors.transparent,
    );
  }
}
