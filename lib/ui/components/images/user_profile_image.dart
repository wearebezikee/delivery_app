import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class UserProfileImage extends StatelessWidget {
  final String imageUrl;
  final File fileImage;
  static const String _DEFAULT_PROFILE_IMAGE_URL =
      "https://firebasestorage.googleapis.com/v0/b/develop-delivery-app.appspot.com/o/kisspng-ninja-ico-icon-black-ninja-5a6dee08a2ac71.3005231415171538006663.png?alt=media&token=478d6154-936e-4bde-b60d-15fa9c37de41";

  UserProfileImage(this.imageUrl) : this.fileImage = null;

  UserProfileImage.fromFileImage(this.fileImage) : this.imageUrl = "";

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundImage: fileImage != null
          ? FileImage(fileImage)
          : this.imageUrl != ""
              ? CachedNetworkImageProvider(imageUrl)
              : CachedNetworkImageProvider(_DEFAULT_PROFILE_IMAGE_URL),
      backgroundColor: Colors.transparent,
      radius: 25,
    );
  }
}
