import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PostImage extends StatelessWidget {
  final String imageUrl;
  final File fileImage;
  static const String _DEFAULT_POST_URL =
      "https://firebasestorage.googleapis.com/v0/b/develop-delivery-app.appspot.com/o/images%2Fproducts%2Fdefault_product.jpg?alt=media&token=054d5a3d-4177-4c19-b512-432a68ea73dc";

  PostImage(this.imageUrl) : this.fileImage = null;

  PostImage.fromFileImage(this.fileImage) : this.imageUrl = "";

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundImage: fileImage != null
          ? FileImage(fileImage)
          : this.imageUrl != ""
              ? CachedNetworkImageProvider(imageUrl)
              : CachedNetworkImageProvider(_DEFAULT_POST_URL),
      backgroundColor: Colors.transparent,
      radius: 25,
    );
  }
}
