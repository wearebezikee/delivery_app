import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class RoundedImage extends StatelessWidget {
  final double height;
  final double width;
  final String imageUrl;
  final bool withShadow;
  final bool topLeft;
  final bool topRight;
  final bool bottomLeft;
  final bool bottomRight;
  final bool withGradient;
  final double borderRadius;

  const RoundedImage({
    this.height,
    this.width,
    this.imageUrl,
    this.withShadow = false,
    this.topLeft = false,
    this.topRight = false,
    this.bottomLeft = false,
    this.bottomRight = false,
    this.withGradient = false,
    this.borderRadius = 0,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Container(
        width: width ?? MediaQuery.of(context).size.width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.transparent,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(imageUrl),
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(topLeft ? borderRadius : 0),
            topRight: Radius.circular(topRight ? borderRadius : 0),
            bottomLeft: Radius.circular(bottomLeft ? borderRadius : 0),
            bottomRight: Radius.circular(bottomRight ? borderRadius : 0),
          ),
          boxShadow: withShadow
              ? [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 0.0), //(x,y)
                    blurRadius: 2.0,
                  ),
                ]
              : [],
        ),
      ),
      withGradient
          ? Container(
              height: height,
              decoration: BoxDecoration(
                  color: Colors.white,
                  gradient: LinearGradient(
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.center,
                      colors: [
                        Colors.grey.withOpacity(0.6),
                        Colors.grey.withOpacity(0.0),
                      ],
                      stops: [
                        0.0,
                        1.0
                      ])),
            )
          : Container()
    ]);
  }
}
