import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class PaymentImage extends StatelessWidget {
  final String imageUrl;
  final File fileImage;

  PaymentImage(this.imageUrl) : this.fileImage = null;

  PaymentImage.fromFileImage(this.fileImage) : this.imageUrl = "";

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.4,
      width: MediaQuery.of(context).size.height * 0.4,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: fileImage != null
              ? FileImage(fileImage)
              : CachedNetworkImageProvider(imageUrl),
        ),
      ),
    );
  }
}
