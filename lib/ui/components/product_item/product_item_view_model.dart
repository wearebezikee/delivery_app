import 'package:delivery_app/data_access/repositories/product/product_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/ui/screens/product/product_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductItemViewModel extends ChangeNotifier {
  Product _product;

  ProductItemViewModel({@required Product product})
      : assert(product != null),
        _product = product;

  bool _loading = false;

  bool get loading => _loading;

  ProductRepository _productRepository = locator<ProductRepository>();

  Future<void> onTapped(BuildContext context) async {
    _loading = true;
    notifyListeners();

    AppResponse<Product> response =
        await _productRepository.getById(_product.id);

    if (response.isSuccess()) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => ProductScreen(product: response.payload)));
    }
    _loading = false;
    notifyListeners();
  }
}
