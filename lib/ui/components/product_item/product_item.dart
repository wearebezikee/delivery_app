import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/ui/components/buttons/primary_button.dart';
import 'package:delivery_app/ui/components/images/rounded_image.dart';
import 'package:delivery_app/ui/components/product_item/product_item_view_model.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/cart/cart_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductItem extends StatelessWidget {
  final Product product;

  ProductItem({this.product});

  ProductItemViewModel _createViewModel() {
    return ProductItemViewModel(product: product);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ProductItemViewModel>(
        create: (context) => _createViewModel(),
        child: Consumer<ProductItemViewModel>(
            builder: (context, productViewModel, staticChild) {
          return Consumer<CartScreenViewModel>(
            builder: (context, cartViewModel, child) {
              return GestureDetector(
                onTap: () => productViewModel.onTapped(context),
                child: RoundedContainer(
                  height: MediaQuery.of(context).size.height * 0.15,
                  margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.1),
                  color: Colors.white,
                  topLeft: true,
                  topRight: true,
                  bottomLeft: true,
                  bottomRight: true,
                  withElevation: true,
                  borderRadius: 5,
                  child: Row(
                    children: <Widget>[
                      RoundedImage(
                        width: MediaQuery.of(context).size.height * 0.16,
                        height: MediaQuery.of(context).size.height * 0.15,
                        imageUrl: product.imageUrl,
                        topLeft: true,
                        topRight: true,
                        bottomLeft: true,
                        bottomRight: true,
                        borderRadius: 5,
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.06),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.035),
                                child: Text(
                                  product.title,
                                  style: Theme.of(context).textTheme.headline1,
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Text(
                                      product.price.toString() + ' \$',
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        right:
                                            MediaQuery.of(context).size.width *
                                                0.03,
                                        bottom:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: PrimaryButton(
                                      child: Text(
                                        'Agregar',
                                        style: Theme.of(context)
                                            .textTheme
                                            .subtitle1,
                                      ),
                                      action: () =>
                                          cartViewModel.addProduct(product),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      /* Column(
                      children: <Widget>[
                        Container(
                          child: PrimaryButton(
                            child: Text(
                              'Agregar',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                            action: () {},
                          ),
                        )
                      ],
                    ), */
                    ],
                  ),
                ),

                /* RoundedContainer(
                height: MediaQuery.of(context).size.height * 0.24,
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1),
                color: Colors.white,
                topLeft: true,
                topRight: true,
                bottomLeft: true,
                bottomRight: true,
                withElevation: true,
                borderRadius: 5,
                child: Column(
                  children: <Widget>[
                    RoundedImage(
                      imageUrl: product.imageUrl,
                      topLeft: true,
                      topRight: true,
                      bottomLeft: true,
                      bottomRight: true,
                      borderRadius: 5,
                      height: MediaQuery.of(context).size.height * 0.15,
                    ),
                  ],
                ),
              ), */
              );
            },
          );
        }));
  }
}
