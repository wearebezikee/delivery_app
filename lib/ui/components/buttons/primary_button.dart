import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  final Widget child;
  final Function action;
  final Color color;

  const PrimaryButton({@required this.child, this.action, this.color});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      onPressed: action != null ? action : () {},
      color: color ?? Theme.of(context).buttonColor,
      child: child,
    );
  }
}
