import 'package:flutter/material.dart';

class RoundedContainer extends StatelessWidget {
  final double height;
  final double width;
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final Widget child;
  final Color color;
  final bool withElevation;
  final bool topLeft;
  final bool topRight;
  final bool bottomLeft;
  final bool bottomRight;
  final double borderRadius;

  const RoundedContainer(
      {this.height,
      this.child,
      this.withElevation = false,
      this.color,
      this.padding,
      this.topLeft = false,
      this.topRight = false,
      this.bottomLeft = false,
      this.bottomRight = false,
      this.margin,
      this.borderRadius,
      this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      padding: padding,
      margin: margin,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(topLeft ? borderRadius : 0),
          topRight: Radius.circular(topRight ? borderRadius : 0),
          bottomLeft: Radius.circular(bottomLeft ? borderRadius : 0),
          bottomRight: Radius.circular(bottomRight ? borderRadius : 0),
        ),
        boxShadow: withElevation
            ? [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 0.0), //(x,y)
                  blurRadius: 1.0,
                ),
              ]
            : [],
      ),
      child: child,
    );
  }
}
