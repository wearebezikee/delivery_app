import 'package:flutter/material.dart';

class CircledItem extends StatelessWidget {
  final ImageProvider image;
  final String text;
  final Function onTap;

  const CircledItem({this.image, this.text, this.onTap});

  @override
  Widget build(BuildContext context) {
    double radius = (MediaQuery.of(context).size.width * 0.33) / 2;
    return InkWell(
      onTap: onTap,
      child: Container(
        width: radius * 2,
        child: Column(
          children: <Widget>[
            CircleAvatar(
              radius: radius * 0.8,
              backgroundImage: image,
              backgroundColor: Colors.transparent,
            ),
            Container(
              height: 15,
            ),
            Container(
              width: radius * 2,
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body1.copyWith(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
