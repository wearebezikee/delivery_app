import 'package:flutter/material.dart';

class CardListTile extends StatelessWidget {
  final Widget leading;
  final Widget title;
  final Function onTap;

  const CardListTile({Key key, this.leading, this.title, this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: ListTile(
        leading: leading ?? Container(),
        title: title ?? Container(),
        onTap: () => onTap() ?? () {},
      ),
    );
    ;
  }
}
