import 'package:delivery_app/ui/app_bar/components/shopping_cart_app_bar_icon.dart';
import 'package:flutter/material.dart';

class TransparentAppBar extends StatelessWidget with PreferredSizeWidget {
  final bool withCart;
  final String title;

  TransparentAppBar({this.title, this.withCart = false});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text(title ?? ''),
        actions: <Widget>[
          withCart ? ShoppingCartAppBarIcon() : Container(),
        ]);
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
