import 'package:flutter/material.dart';

class DeliveryAppBar extends StatelessWidget with PreferredSizeWidget {
  final List<Widget> actions;
  final String title;

  DeliveryAppBar({@required this.title, this.actions});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme
          .of(context)
          .primaryColor
          .withOpacity(0.7),
      actions: actions,
      title: Text(title),
      centerTitle: true,
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(20),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
