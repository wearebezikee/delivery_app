import 'package:delivery_app/ui/app_bar/components/shopping_cart_app_bar_icon.dart';
import 'package:flutter/material.dart';

class BigSliverAppBar extends StatelessWidget {
  final String title;
  final bool withCart;
  final Widget content;

  const BigSliverAppBar({this.title, this.withCart = false, this.content});
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      forceElevated: true,
      expandedHeight: MediaQuery.of(context).size.height * 0.5,
      pinned: true,
      actions: <Widget>[withCart ? ShoppingCartAppBarIcon() : Container()],
      backgroundColor: Colors.white,
      flexibleSpace: content,
    );
  }
}
