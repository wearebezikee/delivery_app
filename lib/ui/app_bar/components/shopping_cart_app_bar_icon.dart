import 'package:badges/badges.dart';
import 'package:delivery_app/ui/screens/cart/cart_screen.dart';
import 'package:delivery_app/ui/screens/cart/cart_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class ShoppingCartAppBarIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<CartScreenViewModel>(
        builder: (context, viewModel, staticChild) {
      return GestureDetector(
        child: Container(
          margin: EdgeInsets.only(right: 25),
          child: _buildIcon(context, viewModel),
        ),
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => CartScreen()));
        },
      );
    });
  }

  Widget _buildIcon(BuildContext context, CartScreenViewModel viewModel) {
    Widget icon = Icon(FontAwesomeIcons.shoppingCart);
    if (viewModel.totalItems > 0) {
      icon = Badge(
        position: BadgePosition.topRight(top: 6),
        badgeColor: Theme.of(context).primaryColor,
        badgeContent: Text(
          viewModel.totalItems.toString(),
          style: TextStyle(color: Colors.black),
        ),
        child: Icon(FontAwesomeIcons.shoppingCart),
      );
    }
    return icon;
  }
}
