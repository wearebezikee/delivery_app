import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DrawerAppBarIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(right: 25),
        child: Icon(FontAwesomeIcons.bars),
      ),
      onTap: () {
        Scaffold.of(context).openEndDrawer();
      },
    );
  }
}
