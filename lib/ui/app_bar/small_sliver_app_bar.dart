import 'package:delivery_app/ui/app_bar/components/drawer_app_bar_icon.dart';
import 'package:delivery_app/ui/app_bar/components/shopping_cart_app_bar_icon.dart';
import 'package:flutter/material.dart';

class SmallSliverAppBar extends StatelessWidget {
  final String title;
  final bool withCart;
  final bool withDrawer;

  const SmallSliverAppBar({this.title, this.withCart, this.withDrawer});
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      forceElevated: true,
      expandedHeight: kToolbarHeight * 2,
      pinned: true,
      actions: <Widget>[
        withCart ? ShoppingCartAppBarIcon() : Container(),
        withDrawer ? DrawerAppBarIcon() : Container(),
      ],
      backgroundColor: Colors.white,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          title ?? '',
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ),
    );
  }
}
