import 'package:flutter/material.dart';

class SmallAppBar extends StatelessWidget with PreferredSizeWidget {
  final List<Widget> actions;
  final String title;

  SmallAppBar({@required this.title, this.actions});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      actions: actions,
      title: Container(
        margin: EdgeInsets.only(top: 20),
        child: Text(
          title,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ),
      centerTitle: true,
      elevation: 10,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(20),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight * 2);
}
