import 'package:delivery_app/ui/app_bar/small_sliver_app_bar.dart';
import 'package:flutter/material.dart';

class SmallSliverAppBarScreen extends StatelessWidget {
  final double height = kToolbarHeight * 2.5;
  final Widget body;
  final String title;
  final bool withCart;
  final bool withDrawer;
  final Widget drawer;

  const SmallSliverAppBarScreen(
      {@required this.body,
      this.title,
      this.withCart = false,
      this.drawer,
      this.withDrawer = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      endDrawer: drawer,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SmallSliverAppBar(
                title: title,
                withCart: withCart,
                withDrawer: withDrawer,
              ),
            ),
          ];
        },
        body: Padding(
          padding: const EdgeInsets.only(top: kToolbarHeight * 1.3),
          child: body,
        ),
      ),
    );
  }
}
