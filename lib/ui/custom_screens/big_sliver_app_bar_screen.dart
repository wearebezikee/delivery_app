import 'package:delivery_app/ui/app_bar/big_sliver_app_bar.dart';
import 'package:flutter/material.dart';

class BigSliverAppBarScreen extends StatelessWidget {
  final Widget body;
  final String title;
  final bool withCart;
  final Widget header;

  const BigSliverAppBarScreen(
      {@required this.body, this.title, this.withCart = false, this.header});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: BigSliverAppBar(
                title: innerBoxIsScrolled ? 'yes' : 'no',
                withCart: withCart,
                content: header,
              ),
            ),
          ];
        },
        body: Padding(
          padding: const EdgeInsets.only(top: kToolbarHeight * 1.3),
          child: body,
        ),
      ),
    );
  }
}
