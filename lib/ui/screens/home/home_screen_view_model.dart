import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/data_access/repositories/post/post_repository.dart';
import 'package:delivery_app/data_access/repositories/product/product_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/posts/category_post.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/model/posts/product_post.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/home_navigator/home_navigator.dart';
import 'package:delivery_app/ui/screens/category/category_screen.dart';
import 'package:delivery_app/ui/screens/product/product_screen.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreenViewModel extends ChangeNotifier {
  PostRepository _postRepository = locator<PostRepository>();
  CategoryRepository _categoryRepository = locator<CategoryRepository>();
  ProductRepository _productRepository = locator<ProductRepository>();
  HomeNavigator _homeNavigator = locator<HomeNavigator>();
  RefreshController _refreshController = RefreshController();

  RefreshController get refreshController => _refreshController;

  List<Post> _posts = List();

  get posts => _posts;
  bool _isLoading = false;

  get isLoading => _isLoading;

  loadPosts() async {
    _isLoading = true;
    notifyListeners();

    AppResponse response = await _postRepository.getAll();
    if (response.isSuccess()) {
      _posts = response.payload;
    } else {
      //TODO handle error
    }
    _isLoading = false;
    notifyListeners();
    _refreshController.refreshCompleted();
  }

  Future<void> onPostTapped(Post post, BuildContext context) async {
    switch (post.type) {
      case Post.TYPE_SIMPLE:
        break;
      case Post.TYPE_CATEGORY:
        AppResponse<Category> categoryResponse = await _categoryRepository
            .getById((post as CategoryPost).categoryId);
        if (categoryResponse.isSuccess()) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  CategoryScreen(category: categoryResponse.payload)));
        }
        break;
      case Post.TYPE_PRODUCT:
        AppResponse<Product> productResponse =
            await _productRepository.getById((post as ProductPost).productId);

        if (productResponse.isSuccess()) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) =>
                  ProductScreen(product: productResponse.payload)));
        }
        break;
    }
  }

  void onEditPostTapped(Post post) {
    _homeNavigator.toEditPost(post);
  }
}
