import 'package:cached_network_image/cached_network_image.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getflutter/getflutter.dart';

class PostItem extends StatelessWidget {
  final Post post;
  final Function action;
  final Function adminFunction;

  const PostItem({@required this.post, this.action, this.adminFunction});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: GFCard(
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        image: Image(
          image: CachedNetworkImageProvider(post.imageUrl),
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height * 0.25,
          width: double.infinity,
        ),
        content: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(),
                Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.09),
                  child: Text(
                    post.title,
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                InkWell(
                  onTap: adminFunction,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.1,
                    height: MediaQuery.of(context).size.height * 0.05,
                    child: Icon(
                      FontAwesomeIcons.pen,
                      size: 18,
                    ),
                  ),
                )
              ],
            ),
            Divider(
              thickness: 1,
            ),
            Text(
              post.description,
              style: Theme.of(context).textTheme.headline3,
            ),
          ],
        ),
      ),
      onTap: action,
    );
  }
}
