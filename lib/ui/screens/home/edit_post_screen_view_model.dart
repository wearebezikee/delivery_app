import 'package:delivery_app/data_access/repositories/post/post_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/navigators/home_navigator/home_navigator.dart';
import 'package:flutter/material.dart';

class EditPostScreenViewModel extends ChangeNotifier {
  Post _post;
  Post get post => _post;

  EditPostScreenViewModel({@required Post post}) {
    assert(post != null);
    _post = post;
    _titleController.text = _post.title;
    _descriptionController.text = _post.description;
  }

  TextEditingController _titleController = TextEditingController();

  TextEditingController get titleController => _titleController;

  TextEditingController _descriptionController = TextEditingController();

  TextEditingController get descriptionController => _descriptionController;

  PostRepository _postRepository = locator<PostRepository>();
  HomeNavigator _homeNavigator = locator<HomeNavigator>();

  bool _editLoading = false;
  bool get editLoading => _editLoading;
  bool _deleteLoading = false;
  bool get deleteLoading => _deleteLoading;

  Future<void> onEditTapped() async {
    _editLoading = true;
    notifyListeners();
    if (_validateForm()) {
      _post.title = _titleController.text;
      _post.description = _descriptionController.text;

      AppResponse response = await _postRepository.update(_post);
      if (response.isSuccess()) {
        _homeNavigator.popScreen();
      }
    }
    _editLoading = false;
    notifyListeners();
  }

  bool _validateForm() {
    try {
      if (_titleController.text.length < 3) {
        throw FormFieldValidationException.tooShortPostTitle();
      }
      if (_descriptionController.text.length < 3) {
        throw FormFieldValidationException.tooShortPostDescription();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }

  Future<void> onDeleteTapped() async {
    _deleteLoading = true;
    notifyListeners();
    AppResponse response = await _postRepository.delete(_post.id);
    if (response.isSuccess()) {
      _homeNavigator.popScreen();
    }
    _deleteLoading = false;
    notifyListeners();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }
}
