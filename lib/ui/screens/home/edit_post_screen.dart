import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/images/post_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:provider/provider.dart';

import 'edit_post_screen_view_model.dart';

class EditPostScreen extends StatelessWidget {
  Post post;

  EditPostScreen({this.post});

  EditPostScreenViewModel _createViewModel() {
    return EditPostScreenViewModel(post: post);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<EditPostScreenViewModel>(
        create: (context) => _createViewModel(),
        child: Consumer<EditPostScreenViewModel>(
            builder: (context, viewModel, staticChild) {
          return SmallSliverAppBarScreen(
            title: "Editar post",
            body: ImageCmsScreen(
                image: PostImage(viewModel.post.imageUrl),
                body: _buildForm(viewModel, context)),
          );
        }));
  }

  Widget _buildForm(EditPostScreenViewModel viewModel, BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  backgroundColor: Colors.white,
                  preffix: Text("Título"),
                  textController: viewModel.titleController,
                ),
              ),
            ),
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  backgroundColor: Colors.white,
                  preffix: Text("Descripción"),
                  textController: viewModel.descriptionController,
                ),
              ),
            ),
          ],
        ),
        viewModel.editLoading
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: MyProgressIndicator(),
              )
            : Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: Container(
                  width: MediaQuery.of(context).size.width -
                      (2 * horizontalMargin),
                  child: PrimaryButtonText(
                    text: "Editar",
                    action: viewModel.onEditTapped,
                  ),
                ),
              ),
        viewModel.deleteLoading
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: MyProgressIndicator(),
              )
            : Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: Container(
                  width: MediaQuery.of(context).size.width -
                      (2 * horizontalMargin),
                  child: PrimaryButtonText(
                    color: Colors.red,
                    text: "Eliminar",
                    action: viewModel.onDeleteTapped,
                  ),
                ),
              ),
      ],
    );
  }
}
