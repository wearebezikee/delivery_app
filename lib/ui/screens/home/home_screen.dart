import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/screens/home/components/product_post_item.dart';
import 'package:delivery_app/ui/screens/home/home_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeScreen extends StatelessWidget {
  HomeScreenViewModel _createViewModel() {
    return HomeScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      body: ChangeNotifierProvider<HomeScreenViewModel>(
          create: (context) => _createViewModel()..loadPosts(),
          child: Consumer<HomeScreenViewModel>(
            builder: (context, viewModel, staticChild) {
              return SmartRefresher(
                enablePullDown: true,
                header: WaterDropMaterialHeader(
                  color: Theme.of(context).primaryColor,
                  backgroundColor: Theme.of(context).backgroundColor,
                ),
                controller: viewModel.refreshController,
                onRefresh: viewModel.loadPosts,
                child: _buildPosts(viewModel, context),
              );
            },
          )),
      title: 'Noticias',
    );
  }

  Widget _buildPosts(HomeScreenViewModel viewModel, BuildContext context) {
    if (viewModel.posts.length == 0) {
      return _buildEmptyPosts();
    } else {
      return _buildLoadedPosts(viewModel, context);
    }
  }

  Widget _buildEmptyPosts() {
    return Center(child: Container(child: Text("Nothing to show yet.")));
  }

  Widget _buildLoadedPosts(
      HomeScreenViewModel viewModel, BuildContext context) {
    List<Widget> posts = List();
    for (Post post in viewModel.posts) {
      posts.add(PostItem(
        post: post,
        action: () {
          viewModel.onPostTapped(post, context);
        },
        adminFunction: () {
          viewModel.onEditPostTapped(post);
        },
      ));
    }
    return ListView(
      padding: EdgeInsets.symmetric(vertical: 10),
      children: posts,
    );
  }
}
