import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'mange_orders_screen_view_model.dart';

class ManageOrdersScreen extends StatelessWidget {
  ManageOrdersScreenViewModel _createViewModel() {
    return ManageOrdersScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Órdenes",
      body: ChangeNotifierProvider<ManageOrdersScreenViewModel>(
        create: (context) => _createViewModel()..onViewCreated(),
        child: Consumer<ManageOrdersScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            return SingleChildScrollView(
              child: _buildDataTable(context, viewModel),
            );
          },
        ),
      ),
    );
  }

  DataTable _buildDataTable(
      BuildContext context, ManageOrdersScreenViewModel viewModel) {
    return DataTable(
        columnSpacing: 10,
        horizontalMargin: horizontalMargin / 2,
        dividerThickness: 2,
        columns: _defineDataColumns(context),
        rows: _buildDataRows(viewModel));
  }

  List<DataColumn> _defineDataColumns(BuildContext context) {
    return [
      DataColumn(
          label: Text(
        "Usuario",
        style: Theme.of(context).textTheme.subtitle2,
      )),
      DataColumn(
          label: Text(
        "Pick up",
        style: Theme.of(context).textTheme.subtitle2,
      )),
      DataColumn(
          label: Text(
        "Precio",
        style: Theme.of(context).textTheme.subtitle2,
      )),
      DataColumn(
          label: Text(
        "Estado",
        style: Theme.of(context).textTheme.subtitle2,
      )),
      DataColumn(
          label: Text(
        "Fecha",
        style: Theme.of(context).textTheme.subtitle2,
      )),
    ];
  }

  List<DataRow> _buildDataRows(ManageOrdersScreenViewModel viewModel) {
    List<DataRow> output = List<DataRow>();

    for (Order actual in viewModel.orders) {
      output.add(DataRow(
          onSelectChanged: (bool selected) {
            if (selected) {
              viewModel.onViewOrderDetailTapped(actual);
            }
          },
          cells: <DataCell>[
            DataCell(Container(width: 100, child: Text(actual.userName))),
            DataCell(
                Container(width: 50, child: Text(actual.pickup ? 'Si' : 'No'))),
            DataCell(Container(
                width: 50, child: Text(actual.totalPrice.toString()))),
            DataCell(Container(
                width: 50, child: Text(actual.statusHistory[0].title))),
            DataCell(Container(
                width: 50,
                child: Text(actual.statusHistory[0].date.toString()))),
          ]));
    }

    return output;
  }
}
