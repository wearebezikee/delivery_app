import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/order/order_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/extra_item.dart';
import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/model/order_status.dart';
import 'package:delivery_app/model/payment/payment_method/zelle_payment_method.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class ManageOrdersScreenViewModel extends ChangeNotifier {
  List<Order> _orders = List<Order>();

  List<Order> get orders => _orders;

  bool _loading = false;

  bool get loading => _loading;

  OrderRepository _orderRepository = locator<OrderRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  Future<void> onViewCreated() async {
    await _loadOrders();
  }

  Future<void> _loadOrders() async {
    _loading = true;
    notifyListeners();

    AppResponse<List<Order>> getAllResponse = await _orderRepository.getAll();

    if (getAllResponse.isSuccess()) {
      _orders = getAllResponse.payload;
    } else {
      //TODO handle error
    }
    _loading = false;
    notifyListeners();
  }

  Future<void> onEditTapped(Order toEdit) async {
    _loading = true;
    notifyListeners();

    AppResponse deleteResponse = await _orderRepository.delete(toEdit.id);

    if (deleteResponse.isSuccess()) {
      _orders.remove(toEdit);
    } else {
      //TODO handle error
    }

    _loading = false;
    notifyListeners();
  }

  void onViewOrderDetailTapped(Order order) {
    _settingsNavigator.toOrderDetail(order);
  }
}
