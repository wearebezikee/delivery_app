import 'package:delivery_app/data_access/repositories/payment_method_setting/payment_method_setting_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class ManagePaymentMethodSettingsScreenViewModel extends ChangeNotifier {
  List<PaymentMethodSetting> _paymentMethodSettings =
      List<PaymentMethodSetting>();

  List<PaymentMethodSetting> get paymentMethodSettings =>
      _paymentMethodSettings;

  bool _loading = false;

  bool get loading => _loading;

  PaymentMethodSettingRepository _paymentMethodSettingRepository =
      locator<PaymentMethodSettingRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  Future<void> onViewCreated() async {
    await _loadPaymentMethodSettings();
  }

  Future<void> _loadPaymentMethodSettings() async {
    _loading = true;
    notifyListeners();

    AppResponse<List<PaymentMethodSetting>> getAllResponse =
        await _paymentMethodSettingRepository.getAll();

    if (getAllResponse.isSuccess()) {
      _paymentMethodSettings = getAllResponse.payload;
    } else {
      //TODO handle error
    }
    _loading = false;
    notifyListeners();
  }

  void onCreateTapped() {
    _settingsNavigator.toCreatePaymentMethodSetting();
  }

  Future<void> onDeleteTapped(PaymentMethodSetting toDelete) async {
    _loading = true;
    notifyListeners();

    AppResponse deleteResponse =
        await _paymentMethodSettingRepository.delete(toDelete.id);

    if (deleteResponse.isSuccess()) {
      _paymentMethodSettings.remove(toDelete);
    } else {
      //TODO handle error
    }

    _loading = false;
    notifyListeners();
  }
}
