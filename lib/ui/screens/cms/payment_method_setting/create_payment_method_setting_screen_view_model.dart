import 'package:delivery_app/data_access/repositories/payment_method_setting/payment_method_setting_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/regex_validator.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/cash_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/pago_movil_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/transfer_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/zelle_payment_method_setting.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class CreatePaymentMethodSettingScreenViewModel extends ChangeNotifier {
  TextEditingController _titleController = TextEditingController();

  TextEditingController get titleController => _titleController;

  TextEditingController _bankNameController = TextEditingController();

  TextEditingController get bankNameController => _bankNameController;

  TextEditingController _accountNumberController = TextEditingController();

  TextEditingController get accountNumberController => _accountNumberController;

  TextEditingController _idNumberController = TextEditingController();

  TextEditingController get idNumberController => _idNumberController;

  TextEditingController _emailController = TextEditingController();

  TextEditingController get emailController => _emailController;

  TextEditingController _phoneNumberController = TextEditingController();

  TextEditingController get phoneNumberController => _phoneNumberController;

  TextEditingController _descriptionController = TextEditingController();

  TextEditingController get descriptionController => _descriptionController;

  String _type;

  String get type => _type;

  bool _loading = false;

  bool get loading => _loading;

  PaymentMethodSettingRepository _paymentMethodSettingRepository =
      locator<PaymentMethodSettingRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  Future<void> onCreateTapped() async {
    if (_validateForm()) {
      _loading = true;
      notifyListeners();

      PaymentMethodSetting toAdd;

      switch (_type) {
        case Payment.TYPE_CASH:
          toAdd = CashPaymentMethodSetting(
              title: _titleController.text, active: true);
          break;
        case Payment.TYPE_TRANSFER:
          toAdd = TransferPaymentMethodSetting(
              title: _titleController.text,
              active: true,
              bankName: bankNameController.text,
              email: emailController.text,
              idNumber: int.parse(idNumberController.text),
              accountNumber: int.parse(accountNumberController.text));
          break;
        case Payment.TYPE_PAGO_MOVIL:
          toAdd = PagoMovilPaymentMethodSetting(
              title: _titleController.text,
              active: true,
              idNumber: int.parse(idNumberController.text),
              email: emailController.text,
              bankName: bankNameController.text,
              phoneNumber: phoneNumberController.text);
          break;
        case Payment.TYPE_ZELLE:
          toAdd = ZellePaymentMethodSetting(
              title: _titleController.text,
              active: true,
              email: emailController.text,
              description: descriptionController.text);
          break;
        default:
          return;
          break;
      }

      AppResponse createResponse =
          await _paymentMethodSettingRepository.create(toAdd);

      if (createResponse.isSuccess()) {
        _settingsNavigator.popScreen();
        _settingsNavigator.popScreen();
      } else {
        //TODO handle error
      }
    }
    _loading = false;
    notifyListeners();
  }

  void onTypeSelected(String type) {
    _type = type;
    notifyListeners();
  }

  bool _validateForm() {
    //TODO create form validations!!!
    try {
      if (_titleController.text.length < 3) {
        throw FormFieldValidationException.paymentMethodSettingTitleTooShort();
      }

      if ((type == Payment.TYPE_TRANSFER || type == Payment.TYPE_PAGO_MOVIL) &&
          _bankNameController.text.length < 3) {
        throw FormFieldValidationException
            .paymentMethodSettingBankNameTooShort();
      }

      if (type == Payment.TYPE_TRANSFER &&
          _accountNumberController.text.length < 10) {
        throw FormFieldValidationException
            .paymentMethodSettingAccountNumberTooShort();
      }

      if ((type == Payment.TYPE_TRANSFER || type == Payment.TYPE_PAGO_MOVIL) &&
          _idNumberController.text.length < 5) {
        throw FormFieldValidationException
            .paymentMethodSettingIdNumberTooShort();
      }

      if (type != Payment.TYPE_CASH &&
          !RegexValidator.isEmail(_emailController.text)) {
        throw FormFieldValidationException.notAnEmail();
      }

      if (type == Payment.TYPE_PAGO_MOVIL &&
          !RegexValidator.isPhoneNumber(_phoneNumberController.text)) {
        throw FormFieldValidationException.notAPhoneNumber();
      }

      if (type == Payment.TYPE_ZELLE &&
          _descriptionController.text.length < 3) {
        throw FormFieldValidationException
            .paymentMethodSettingDescriptionTooShort();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }
}
