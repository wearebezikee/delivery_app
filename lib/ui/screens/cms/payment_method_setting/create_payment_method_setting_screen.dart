import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'create_payment_method_setting_screen_view_model.dart';

class CreatePaymentMethodSettingScreen extends StatelessWidget {
  CreatePaymentMethodSettingScreenViewModel _createViewModel() {
    return CreatePaymentMethodSettingScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Metodos de pago",
      body: ChangeNotifierProvider<CreatePaymentMethodSettingScreenViewModel>(
        create: (context) => _createViewModel(),
        child: Consumer<CreatePaymentMethodSettingScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            return ListView(
              children: <Widget>[
                _buildTypeDropdown(context, viewModel),
                _buildPaymentMethodSettingsForm(context, viewModel),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: horizontalMargin),
                  child: PrimaryButtonText(
                    text: "Crear",
                    action: viewModel.onCreateTapped,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildPaymentMethodSettingsForm(BuildContext context,
      CreatePaymentMethodSettingScreenViewModel viewModel) {
    switch (viewModel.type) {
      case Payment.TYPE_CASH:
        return _buildPaymentMethodSettingsFormCash(context, viewModel);
        break;
      case Payment.TYPE_TRANSFER:
        return _buildPaymentMethodSettingsFormTransfer(context, viewModel);
        break;
      case Payment.TYPE_PAGO_MOVIL:
        return _buildPaymentMethodSettingsFormPagoMovil(context, viewModel);
        break;
      case Payment.TYPE_ZELLE:
        return _buildPaymentMethodSettingsFormZelle(context, viewModel);
        break;

      default:
        return Container(
          height: 100,
          child: Center(
            child: Text(
              "Seleccione un nuevo método de pago!",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline3,
            ),
          ),
        );
        break;
    }
  }

  Widget _buildTypeDropdown(BuildContext context,
      CreatePaymentMethodSettingScreenViewModel viewModel) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.1),
      child: DropdownButton<String>(
        value: viewModel.type,
        isExpanded: true,
        icon: Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: Theme.of(context).textTheme.bodyText2,
        underline: Container(
          height: 2,
          color: Colors.green,
        ),
        onChanged: (String newType) {
          viewModel.onTypeSelected(newType);
        },
        items: <String>[
          Payment.TYPE_ZELLE,
          Payment.TYPE_PAGO_MOVIL,
          Payment.TYPE_TRANSFER,
          Payment.TYPE_CASH
        ].map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }

  Widget _buildPaymentMethodSettingsFormCash(BuildContext context,
      CreatePaymentMethodSettingScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Titulo:"),
              textController: viewModel.titleController,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPaymentMethodSettingsFormTransfer(BuildContext context,
      CreatePaymentMethodSettingScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Titulo:"),
              textController: viewModel.titleController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Banco:"),
              textController: viewModel.bankNameController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Numero de cuenta:"),
              textController: viewModel.accountNumberController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Numero de identificacion:"),
              textController: viewModel.idNumberController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Email:"),
              textController: viewModel.emailController,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPaymentMethodSettingsFormPagoMovil(BuildContext context,
      CreatePaymentMethodSettingScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Titulo:"),
              textController: viewModel.titleController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Banco:"),
              textController: viewModel.bankNameController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Numero de telefono"),
              textController: viewModel.phoneNumberController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Identificacion:"),
              textController: viewModel.idNumberController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Email:"),
              textController: viewModel.emailController,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildPaymentMethodSettingsFormZelle(BuildContext context,
      CreatePaymentMethodSettingScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Titulo:"),
              textController: viewModel.titleController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Email: "),
              textController: viewModel.emailController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Description:"),
              textController: viewModel.descriptionController,
            ),
          ),
        )
      ],
    );
  }
}
