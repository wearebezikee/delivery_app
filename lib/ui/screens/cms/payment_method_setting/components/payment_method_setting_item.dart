import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/cash_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/pago_movil_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/transfer_payment_method_setting.dart';
import 'package:delivery_app/model/payment/payment_method_setting/zelle_payment_method_setting.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getflutter/components/card/gf_card.dart';

class PaymentMethodSettingItem extends StatelessWidget {
  PaymentMethodSetting paymentMethod;
  final Function onDeleteTapped;

  PaymentMethodSettingItem({this.paymentMethod, this.onDeleteTapped});

  @override
  Widget build(BuildContext context) {
    return GFCard(
      content: Column(children: _buildContent(context)),
    );
  }

  List<Widget> _buildContent(BuildContext context) {
    switch (paymentMethod.type) {
      case Payment.TYPE_CASH:
        return _buildCashContent(context);
        break;
      case Payment.TYPE_TRANSFER:
        return _buildTransferContent(context);
        break;
      case Payment.TYPE_PAGO_MOVIL:
        return _buildPagoMovilContent(context);
        break;
      case Payment.TYPE_ZELLE:
        return _buildZelleContent(context);
        break;
    }
  }

  List<Widget> _buildCashContent(BuildContext context) {
    CashPaymentMethodSetting paymentMethodAux =
        paymentMethod as CashPaymentMethodSetting;
    return [
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Tipo: ")),
            Text(paymentMethodAux.type),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Title: ")),
            Text(paymentMethodAux.title),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Activo: ")),
            Text(paymentMethodAux.active.toString()),
          ],
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          InkWell(
            onTap: onDeleteTapped,
            child: Icon(
              FontAwesomeIcons.trash,
              size: 15,
              color: Colors.red,
            ),
          )
        ],
      )
    ];
  }

  List<Widget> _buildZelleContent(BuildContext context) {
    ZellePaymentMethodSetting paymentMethodAux =
        paymentMethod as ZellePaymentMethodSetting;
    return [
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Tipo: ")),
            Text(paymentMethodAux.type),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Title: ")),
            Text(paymentMethodAux.title),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Activo: ")),
            Text(paymentMethodAux.active.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Email: ")),
            Text(paymentMethodAux.email),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Description: ")),
            Text(paymentMethodAux.description),
          ],
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          InkWell(
            onTap: onDeleteTapped,
            child: Icon(
              FontAwesomeIcons.trash,
              size: 15,
              color: Colors.red,
            ),
          )
        ],
      )
    ];
  }

  List<Widget> _buildTransferContent(BuildContext context) {
    TransferPaymentMethodSetting paymentMethodAux =
        paymentMethod as TransferPaymentMethodSetting;
    return [
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Tipo: ")),
            Text(paymentMethodAux.type),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Title: ")),
            Text(paymentMethodAux.title),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Activo: ")),
            Text(paymentMethodAux.active.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Banco: ")),
            Text(paymentMethodAux.bankName),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Cuenta: ")),
            Text(paymentMethodAux.accountNumber.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Identificacion: ")),
            Text(paymentMethodAux.idNumber.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Email: ")),
            Text(paymentMethodAux.email),
          ],
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          InkWell(
            onTap: onDeleteTapped,
            child: Icon(
              FontAwesomeIcons.trash,
              size: 15,
              color: Colors.red,
            ),
          )
        ],
      )
    ];
  }

  List<Widget> _buildPagoMovilContent(BuildContext context) {
    PagoMovilPaymentMethodSetting paymentMethodAux =
        paymentMethod as PagoMovilPaymentMethodSetting;
    return [
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Tipo: ")),
            Text(paymentMethodAux.type),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Title: ")),
            Text(paymentMethodAux.title),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Activo: ")),
            Text(paymentMethodAux.active.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Banco: ")),
            Text(paymentMethodAux.bankName),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Telefono: ")),
            Text(paymentMethodAux.phoneNumber.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Identificacion: ")),
            Text(paymentMethodAux.idNumber.toString()),
          ],
        ),
      ),
      Container(
        height: 20,
        child: Row(
          children: <Widget>[
            Container(width: 80, child: Text("Email: ")),
            Text(paymentMethodAux.email),
          ],
        ),
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          InkWell(
            onTap: onDeleteTapped,
            child: Icon(
              FontAwesomeIcons.trash,
              size: 15,
              color: Colors.red,
            ),
          )
        ],
      )
    ];
  }
}
