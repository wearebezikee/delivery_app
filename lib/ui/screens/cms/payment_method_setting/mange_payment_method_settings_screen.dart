import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/payment_method_setting/components/payment_method_setting_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'mange_payment_method_settings_screen_view_model.dart';

class ManagePaymentMethodSettingsScreen extends StatelessWidget {
  ManagePaymentMethodSettingsScreenViewModel _createViewModel() {
    return ManagePaymentMethodSettingsScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Metodos de pago",
      body: ChangeNotifierProvider<ManagePaymentMethodSettingsScreenViewModel>(
        create: (context) => _createViewModel()..onViewCreated(),
        child: Consumer<ManagePaymentMethodSettingsScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            return ListView(
              children: <Widget>[
                viewModel.loading
                    ? Container(
                        height: MediaQuery.of(context).size.height - 200,
                        child: Center(child: MyProgressIndicator()))
                    : _buildPaymentMethodSettingSection(context, viewModel),
                viewModel.loading
                    ? Container()
                    : Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: horizontalMargin),
                        child: PrimaryButtonText(
                          text: "Agregar",
                          action: viewModel.onCreateTapped,
                        ))
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildPaymentMethodSettingSection(BuildContext context,
      ManagePaymentMethodSettingsScreenViewModel viewModel) {
    List<Widget> listOfPaymentMethodSetting = List<Widget>();
    if (viewModel.paymentMethodSettings.length == 0) {
      return Container(
        height: 100,
        child: Center(
          child: Text(
            "Agrega un nuevo método de pago!",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
      );
    } else {
      for (PaymentMethodSetting actual in viewModel.paymentMethodSettings) {
        listOfPaymentMethodSetting.add(PaymentMethodSettingItem(
          paymentMethod: actual,
          onDeleteTapped: () {
            viewModel.onDeleteTapped(actual);
          },
        ));
      }
    }
    return Column(
      children: listOfPaymentMethodSetting,
    );
  }
}
