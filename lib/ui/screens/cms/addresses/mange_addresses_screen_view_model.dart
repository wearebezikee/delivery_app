import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class ManageAddressesScreenViewModel extends ChangeNotifier {
  bool _loading = false;

  bool get loading => _loading;
  UserRepository _userRepository = locator<UserRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  User _loggedUser = LoggedUserHelper.getLoggedUser();

  User get loggedUser => _loggedUser;

  Future<void> onDeleteTapped(Address address) async {
    _loading = true;
    notifyListeners();

    for (int i = 0; i < _loggedUser.addresses.length; i++) {
      Address actual = _loggedUser.addresses[i];
      if (actual == address) {
        _loggedUser.addresses.remove(actual);
      }
    }

    AppResponse response = await _userRepository.update(_loggedUser);

    //TODO handle error???
    _loading = false;
    notifyListeners();
  }

  void onCreateAddressTapped() {
    _settingsNavigator.toCreateAddress();
  }
}
