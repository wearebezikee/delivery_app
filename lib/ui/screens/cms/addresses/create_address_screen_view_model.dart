import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class CreateAddressScreenViewModel extends ChangeNotifier {
  TextEditingController _address1Controller = TextEditingController();

  TextEditingController get address1Controller => _address1Controller;

  TextEditingController _address2Controller = TextEditingController();

  TextEditingController get address2Controller => _address2Controller;

  TextEditingController _streetController = TextEditingController();

  TextEditingController get streetController => _streetController;

  TextEditingController _townController = TextEditingController();

  TextEditingController get townController => _townController;

  TextEditingController _cityController = TextEditingController();

  TextEditingController get cityController => _cityController;

  bool _loading = false;

  bool get loading => _loading;

  UserRepository _userRepository = locator<UserRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  Future<void> onCreateTapped(BuildContext context) async {
    if (_validateForm()) {
      _loading = true;
      notifyListeners();

      Address toAdd = Address(
          address1: _address1Controller.text,
          address2: _address2Controller.text,
          city: _cityController.text,
          street: _streetController.text,
          town: _townController.text);
      User loggedUser = LoggedUserHelper.getLoggedUser();

      loggedUser.addresses.add(toAdd);

      AppResponse updateResponse = await _userRepository.update(loggedUser);

      if (updateResponse.isSuccess()) {
        Navigator.of(context).pop();
      } else {
        //TODO handle error
      }
    }
    _loading = false;
    notifyListeners();
  }

  bool _validateForm() {
    try {
      if (_address1Controller.text.length < 10) {
        throw FormFieldValidationException.tooShortAddress();
      }
      if (_streetController.text.length < 1) {
        throw FormFieldValidationException.noStreet();
      }
      if (_townController.text.length < 1) {
        throw FormFieldValidationException.noTown();
      }
      if (_cityController.text.length < 1) {
        throw FormFieldValidationException.noCity();
      }
      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }
}
