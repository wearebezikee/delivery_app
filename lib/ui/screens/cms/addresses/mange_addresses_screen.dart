import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/addresses/component/address_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'mange_addresses_screen_view_model.dart';

class ManageAddressesScreen extends StatelessWidget {
  ManageAddressesScreenViewModel _createViewModel() {
    return ManageAddressesScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Direcciones",
      body: ChangeNotifierProvider<ManageAddressesScreenViewModel>(
        create: (context) => _createViewModel(),
        child: Consumer<ManageAddressesScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            return ListView(
              children: <Widget>[
                viewModel.loading
                    ? Container(
                        height: MediaQuery.of(context).size.height - 200,
                        child: Center(child: MyProgressIndicator()))
                    : _buildAddressesSection(context, viewModel),
                viewModel.loading
                    ? Container()
                    : Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: horizontalMargin),
                        child: PrimaryButtonText(
                            text: "Agregar",
                            action: viewModel.onCreateAddressTapped),
                      ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildAddressesSection(
      BuildContext context, ManageAddressesScreenViewModel viewModel) {
    List<Widget> listOfAddresses = List<Widget>();
    if (viewModel.loggedUser.addresses.length == 0) {
      return Container(
        height: 100,
        child: Center(
          child: Text(
            "Agrega una nueva dirección!",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
      );
    } else {
      for (Address address in viewModel.loggedUser.addresses) {
        listOfAddresses.add(AddressItem(
          address: address,
          onDeleteTapped: () {
            viewModel.onDeleteTapped(address);
          },
        ));
      }
    }
    return Column(
      children: listOfAddresses,
    );
  }
}
