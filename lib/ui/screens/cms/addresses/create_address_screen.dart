import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:delivery_app/ui/app_bar/delivery_app_bar.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:provider/provider.dart';

import 'create_address_screen_view_model.dart';

class CreateAddressScreen extends StatelessWidget {
  User loggedUser = LoggedUserHelper.getLoggedUser();
  SettingsNavigator settingsNavigator = locator<SettingsNavigator>();

  CreateAddressScreenViewModel _createViewModel() {
    return CreateAddressScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Direcciones",
      body: ChangeNotifierProvider<CreateAddressScreenViewModel>(
        create: (context) => _createViewModel(),
        child: Consumer<CreateAddressScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            return ListView(
              children: <Widget>[
                _buildAddressesForm(context, viewModel),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: horizontalMargin),
                  child: PrimaryButtonText(
                    text: "Crear",
                    action: () => viewModel.onCreateTapped(context),
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildAddressesForm(
      BuildContext context, CreateAddressScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Dirección 1"),
              textController: viewModel.address1Controller,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Dirección 2"),
              textController: viewModel.address2Controller,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Calle"),
              textController: viewModel.streetController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Municipio"),
              textController: viewModel.townController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Ciudad"),
              textController: viewModel.cityController,
            ),
          ),
        )
      ],
    );
  }
}
