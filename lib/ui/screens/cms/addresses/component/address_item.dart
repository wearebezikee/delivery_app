import 'package:delivery_app/model/address.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getflutter/components/card/gf_card.dart';

class AddressItem extends StatelessWidget {
  final Address address;
  final Function onDeleteTapped;

  AddressItem({this.address, this.onDeleteTapped});

  @override
  Widget build(BuildContext context) {
    return GFCard(
      content: Column(
        children: <Widget>[
          Container(
            height: 20,
            child: Row(
              children: <Widget>[
                Container(width: 80, child: Text("Dirección 1: ")),
                Text(address.address1),
              ],
            ),
          ),
          address.address2.length > 0
              ? Container(
                  height: 20,
                  child: Row(
                    children: <Widget>[
                      Container(width: 80, child: Text("Dirección 2: ")),
                      Text(address.address2),
                    ],
                  ),
                )
              : Container(),
          Container(
            height: 20,
            child: Row(
              children: <Widget>[
                Container(width: 80, child: Text("Calle: ")),
                Text(address.street),
              ],
            ),
          ),
          Container(
            height: 20,
            child: Row(
              children: <Widget>[
                Container(width: 80, child: Text("Municipio: ")),
                Text(address.town),
              ],
            ),
          ),
          Container(
            height: 20,
            child: Row(
              children: <Widget>[
                Container(width: 80, child: Text("Ciudad: ")),
                Text(address.city),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              InkWell(
                onTap: onDeleteTapped,
                child: Icon(
                  FontAwesomeIcons.trash,
                  size: 15,
                  color: Colors.red,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
