import 'package:delivery_app/data_access/repositories/schedule/schedule_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/schedule.dart';
import 'package:flutter/material.dart';

class ManageScheduleScreenViewModel extends ChangeNotifier {
  List<Schedule> _schedules;

  List<Schedule> get schedules => _schedules;

  ScheduleRepository _scheduleRepository = locator<ScheduleRepository>();

  TextEditingController _startHourController = TextEditingController();

  TextEditingController get startHourController => _startHourController;

  TextEditingController _endHourController = TextEditingController();

  TextEditingController get endHourController => _endHourController;

  TextEditingController _dayOfWeekController = TextEditingController();

  TextEditingController get dayOfWeekController => _dayOfWeekController;

  bool _loading = false;

  bool get loading => _loading;

  void onViewCreated() {
    fetchSchedules();
  }

  Future<void> fetchSchedules() async {
    _loading = true;
    notifyListeners();
    AppResponse<List<Schedule>> getAllResponse =
        await _scheduleRepository.getAll();

    if (getAllResponse.isSuccess()) {
      _schedules = getAllResponse.payload;
    }
    _loading = false;
    notifyListeners();
  }

  Future<void> onCreateTapped() async {
    if (_validateForm()) {
      _loading = true;
      notifyListeners();

      AppResponse<Schedule> createResponse = await _scheduleRepository.create(
          Schedule(
              dayOfWeek: _dayOfWeekController.text,
              endHour: TimeOfDay.fromDateTime(
                  DateTime.parse(_endHourController.text)),
              startHour: TimeOfDay.fromDateTime(
                  DateTime.parse(_startHourController.text))));

      if (createResponse.isSuccess()) {
        await fetchSchedules();
      }
      _loading = false;
      notifyListeners();
    }
  }

  Future<void> onDeleteTapped(String idToDelete) async {
    _loading = true;
    notifyListeners();

    AppResponse<void> deleteResponse =
        await _scheduleRepository.delete(idToDelete);

    if (deleteResponse.isSuccess()) {
      await fetchSchedules();
    }
    _loading = false;
    notifyListeners();
  }

  bool _validateForm() {
    //TODO correct type validations
    try {
      if (_startHourController.text == "") {
        throw FormFieldValidationException.noStartHour();
      }
      if (_endHourController.text == "") {
        throw FormFieldValidationException.noEndHour();
      }
      if (_dayOfWeekController.text == "") {
        throw FormFieldValidationException.noDayOfWeek();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }
}
