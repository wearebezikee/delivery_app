import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/schedule.dart';
import 'package:delivery_app/ui/app_bar/delivery_app_bar.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:provider/provider.dart';

import 'manage_schedule_screen_view_model.dart';

class ManageScheduleScreen extends StatelessWidget {
  ManageScheduleScreenViewModel _createViewModel() {
    return ManageScheduleScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Horarios",
      body: ChangeNotifierProvider<ManageScheduleScreenViewModel>(
          create: (context) => _createViewModel()..onViewCreated(),
          child: Consumer<ManageScheduleScreenViewModel>(
              builder: (context, viewModel, staticChild) {
            if (viewModel.loading) {
              return Center(
                child: MyProgressIndicator(),
              );
            } else {
              return Container(
                child: ListView(
                  children: <Widget>[
                    viewModel.schedules.length == 0
                        ? _buildEmptySchedules(context)
                        : _buildSchedulesList(viewModel, context),
                    _buildCreateForm(viewModel, context)
                  ],
                ),
              );
            }
          })),
    );
  }

  Widget _buildEmptySchedules(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 40),
      child: Center(
        child: Text(
          "Crea un horario!",
          style: Theme.of(context).textTheme.headline3,
        ),
      ),
    );
  }

  Widget _buildSchedulesList(
      ManageScheduleScreenViewModel viewModel, BuildContext context) {
    List<Row> schedulesList = List<Row>();

    for (Schedule actual in viewModel.schedules) {
      schedulesList.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          FormFieldContainer(
            withDivider: true,
            //TODO function that prints pretty time of day
            child: Center(
              child: Text(actual.dayOfWeek +
                  " from " +
                  actual.startHour.hour.toString() +
                  " : " +
                  actual.startHour.minute.toString() +
                  " to " +
                  actual.endHour.hour.toString() +
                  " : " +
                  actual.endHour.minute.toString()),
            ),
          ),
          InkWell(
            child: Icon(
              FontAwesomeIcons.trash,
              color: Colors.red,
            ),
            onTap: () {
              viewModel.onDeleteTapped(actual.id);
            },
          )
        ],
      ));
    }
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(top: 25),
        width: MediaQuery.of(context).size.width - (2 * horizontalMargin),
        child: Column(
          children: schedulesList,
        ),
      ),
    );
  }

  Widget _buildCreateForm(
      ManageScheduleScreenViewModel viewModel, BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              RoundedContainer(
                withElevation: true,
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1,
                    vertical: 10),
                child: FormFieldContainer(
                  withDivider: true,
                  child: FormFieldText(
                    backgroundColor: Colors.white,
                    preffix: Text("Día de la semana"),
                    textController: viewModel.dayOfWeekController,
                  ),
                ),
              ),
              RoundedContainer(
                withElevation: true,
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1,
                    vertical: 10),
                child: FormFieldContainer(
                  withDivider: true,
                  child: FormFieldText(
                    backgroundColor: Colors.white,
                    preffix: Text("Hora de inicio"),
                    textController: viewModel.startHourController,
                  ),
                ),
              ),
              RoundedContainer(
                withElevation: true,
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1,
                    vertical: 10),
                child: FormFieldContainer(
                  withDivider: true,
                  child: FormFieldText(
                    backgroundColor: Colors.white,
                    preffix: Text("Hora fin"),
                    textController: viewModel.endHourController,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 15),
                width:
                    MediaQuery.of(context).size.width - (2 * horizontalMargin),
                child: PrimaryButtonText(
                  text: "Crear",
                  action: () => viewModel.onCreateTapped(),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
