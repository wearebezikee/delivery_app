import 'dart:io';

import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/data_access/repositories/post/post_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/posts/category_post.dart';
import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/model/posts/product_post.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ManageTimelineScreenViewModel extends ChangeNotifier {
  Post _post;

  Post get post => _post;

  Category _root;
  Category _categorySelected;
  Product _productSelected;

  Category get root => _root;

  Category get categorySelected => _categorySelected;

  Product get productSelected => _productSelected;

  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  bool _rootLoading = false;

  bool get rootLoading => _rootLoading;

  File _selectedImage;

  File get selectedImage => _selectedImage;

  TextEditingController _titleController = TextEditingController();

  TextEditingController get titleController => _titleController;

  TextEditingController _descriptionController = TextEditingController();

  TextEditingController get descriptionController => _descriptionController;

  ImageResourceRepository _imageResourceRepository =
      locator<ImageResourceRepository>();
  PostRepository _postRepository = locator<PostRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  bool _loadingCreate = false;

  bool get loadingCreate => _loadingCreate;

  ManageTimelineScreenViewModel() {
    _post = Post.empty();
  }

  onViewCreated() async {
    _rootLoading = true;
    notifyListeners();
    AppResponse<Category> rootResponse =
        await _categoryRepository.getFullTreeFrom(Category.ROOT_ID);

    if (rootResponse.isSuccess()) {
      _root = rootResponse.payload;
    } else {
      //TODO handle error
    }
    _rootLoading = false;
    notifyListeners();
  }

  Future<void> pickImage() async {
    PickedFile pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    _selectedImage = File(pickedFile.path);
    notifyListeners();
  }

  void selectCategory(Category selected) {
    if (_categorySelected == selected) {
      _categorySelected = null;
    } else {
      _categorySelected = selected;
      _productSelected = null;
    }
    notifyListeners();
    _settingsNavigator.popScreen();
  }

  void selectProduct(Product selected) {
    if (_productSelected == selected) {
      _productSelected = null;
    } else {
      _productSelected = selected;
      _categorySelected = null;
    }
    notifyListeners();
    _settingsNavigator.popScreen();
  }

  Future<void> onCreatePostTapped() async {
    _loadingCreate = true;
    notifyListeners();
    if (_validateForm()) {
      _post = _instancePostFromForm();
      if (_selectedImage != null) {
        AppResponse<String> responseImageUrl = await _imageResourceRepository
            .uploadPostImage(_selectedImage, _titleController.text);
        if (responseImageUrl.isSuccess()) {
          _post.imageUrl = responseImageUrl.payload;
          _post.date = DateTime.now();
        }
      }

      AppResponse createResponse = await _postRepository.create(_post);
      if (createResponse.isSuccess()) {
        _settingsNavigator.popScreen();
      } else {
        MyNotification.showError(subtitle: createResponse.error.toString());
      }
      _loadingCreate = false;
      notifyListeners();
    }
  }

  bool _validateForm() {
    try {
      if (_titleController.text.length < 3) {
        throw FormFieldValidationException.tooShortPostTitle();
      }
      if (_descriptionController.text.length < 3) {
        throw FormFieldValidationException.tooShortPostDescription();
      }
      if (_selectedImage == null &&
          _categorySelected == null &&
          _productSelected == null) {
        throw FormFieldValidationException.noImageSelected();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }

  Post _instancePostFromForm() {
    if (_productSelected != null) {
      return ProductPost(
          title: _titleController.text,
          description: _descriptionController.text,
          productId: _productSelected.id);
    } else if (_categorySelected != null) {
      return CategoryPost(
          title: _titleController.text,
          description: _descriptionController.text,
          categoryId: _categorySelected.id);
    } else {
      return Post(
        title: _titleController.text,
        description: _descriptionController.text,
      );
    }
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }
}
