import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/ui/app_bar/delivery_app_bar.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/images/post_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/category_tree/full_category_tree.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:getflutter/components/drawer/gf_drawer.dart';
import 'package:provider/provider.dart';

import 'manage_timeline_screen_view_model.dart';

class ManageTimelineScreen extends StatelessWidget {
  ManageTimelineScreenViewModel _createViewModel() {
    return ManageTimelineScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ManageTimelineScreenViewModel>(
      create: (context) => _createViewModel()..onViewCreated(),
      child: Consumer<ManageTimelineScreenViewModel>(
        builder: (context, viewModel, staticChild) {
          return SmallSliverAppBarScreen(
              title: 'Crear post',
              withDrawer: true,
              drawer: GFDrawer(
                child: ListView(
                  padding: EdgeInsets.only(top: 25),
                  children: [
                    viewModel.rootLoading
                        ? Center(
                            child: Container(
                            margin: EdgeInsets.only(
                                top: (MediaQuery.of(context).size.height / 2) -
                                    80),
                            height: 80,
                            width: 80,
                            child: MyProgressIndicator(),
                          ))
                        : FullCategoryTree(
                            rootCategory: viewModel.root,
                            categorySelected: viewModel.categorySelected,
                            productSelected: viewModel.productSelected,
                            categoryAction: viewModel.selectCategory,
                            productAction: viewModel.selectProduct)
                  ],
                ),
              ),
              body: ImageCmsScreen(
                image: _buildImage(viewModel),
                imageAction: viewModel.pickImage,
                body: _buildForm(viewModel, context),
              ));
        },
      ),
    );
  }

  Widget _buildImage(ManageTimelineScreenViewModel viewModel) {
    return viewModel.selectedImage == null
        ? PostImage(viewModel.post.imageUrl)
        : PostImage.fromFileImage(viewModel.selectedImage);
  }

  Widget _buildForm(
      ManageTimelineScreenViewModel viewModel, BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  backgroundColor: Colors.white,
                  preffix: Text("Título"),
                  textController: viewModel.titleController,
                ),
              ),
            ),
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  backgroundColor: Colors.white,
                  preffix: Text("Descripción"),
                  textController: viewModel.descriptionController,
                ),
              ),
            ),
          ],
        ),
        viewModel.loadingCreate
            ? MyProgressIndicator()
            : Container(
                margin: EdgeInsets.only(top: 15),
                width:
                    MediaQuery.of(context).size.width - (2 * horizontalMargin),
                child: PrimaryButtonText(
                  text: "Crear",
                  action: viewModel.onCreatePostTapped,
                ),
              ),
      ],
    );
  }
}
