import 'package:flutter/material.dart';

class ImageCmsScreen extends StatelessWidget {
  final Widget image;
  final Function imageAction;
  final Widget body;

  ImageCmsScreen({this.image, this.imageAction, this.body});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Center(
            child: Container(
                margin: EdgeInsets.symmetric(vertical: 25),
                height: 200,
                width: 200,
                child: InkWell(
                  onTap: imageAction,
                  child: image,
                ))),
        body
      ],
    );
  }
}
