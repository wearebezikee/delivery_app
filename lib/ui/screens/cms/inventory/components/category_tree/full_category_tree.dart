import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/category_tree/category_tree_item.dart';
import 'package:flutter/material.dart';

class FullCategoryTree extends StatelessWidget {
  final Category rootCategory;
  final Function productAction;
  final Function categoryAction;
  final Product productSelected;
  final Category categorySelected;

  FullCategoryTree(
      {@required this.rootCategory,
      @required this.productAction,
      @required this.categoryAction,
      @required this.productSelected,
      @required this.categorySelected});

  @override
  Widget build(BuildContext context) {
    return _buildCategoryTree(rootCategory);
  }

  CategoryTreeItem _buildCategoryTree(Category categoryToBuild) {
    List<Widget> content = List<Widget>();

    for (int i = 0; i < categoryToBuild.categories.length; i++) {
      Category actual = categoryToBuild.categories[i];
      content.add(_buildCategoryTree(actual));
    }
    for (int i = 0; i < categoryToBuild.products.length; i++) {
      Product actual = categoryToBuild.products[i];
      content.add(CategoryTreeItem(
        title: actual.title,
        action: (){productAction(actual);},
        selected: productSelected == actual,
      ));
    }

    return CategoryTreeItem(
      title: categoryToBuild.name,
      child:
          Column(mainAxisAlignment: MainAxisAlignment.end, children: content),
      action: (){categoryAction(categoryToBuild);},
      selected: categorySelected == categoryToBuild,
    );
  }
}
