import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:getflutter/components/accordian/gf_accordian.dart';

class CategoryTreeItem extends StatelessWidget {
  final Widget child;
  final String title;
  final bool selected;
  final action;

  CategoryTreeItem(
      {this.child, @required this.title, @required this.action, @required this.selected});

  @override
  Widget build(BuildContext context) {
    return child != null
        ? GFAccordion(
            titleChild: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                InkWell(
                  onTap: action,
                  child: Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 25,
                    height: 25,
                    child: Icon(
                      selected ? FontAwesomeIcons.checkSquare : FontAwesomeIcons.square,
                      size: 22,
                    ),
                  ),
                ),
                Text(title),
              ],
            ),
            contentChild: child,
          )
        : InkWell(
            onTap: action,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(left: 20, top: 5),
                  child: Container(
                    margin: EdgeInsets.only(right: 10),
                    width: 25,
                    height: 25,
                    child: Icon(
                      selected ? FontAwesomeIcons.checkCircle : FontAwesomeIcons.circle,
                      size: 22,
                    ),
                  ),
                ),
                Text(title),
              ],
            ),
          );
  }
}
