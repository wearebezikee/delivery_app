import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_routes.dart';
import 'package:delivery_app/ui/app_bar/delivery_app_bar.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/category_tree/full_category_tree.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/drawer/gf_drawer.dart';
import 'package:provider/provider.dart';

import 'manage_inventory_screen_view_model.dart';

class ManageInventoryScreen extends StatelessWidget {
  ManageInventoryScreenViewModel createViewModel() {
    return ManageInventoryScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ManageInventoryScreenViewModel>(
      create: (context) => createViewModel()..onViewCreated(),
      child: Consumer<ManageInventoryScreenViewModel>(
          builder: (context, viewModel, staticChild) {
        return SmallSliverAppBarScreen(
          withDrawer: true,
          title: "Inventario",
          drawer: GFDrawer(
            child: ListView(
              children: [
                viewModel.rootLoading
                    ? Center(
                        child: Container(
                        margin: EdgeInsets.only(
                            top: (MediaQuery.of(context).size.height / 2) - 80),
                        height: 80,
                        width: 80,
                        child: MyProgressIndicator(),
                      ))
                    : FullCategoryTree(
                        rootCategory: viewModel.root,
                        categorySelected: viewModel.categorySelected,
                        productSelected: viewModel.productSelected,
                        categoryAction: viewModel.selectCategory,
                        productAction: viewModel.selectProduct)
              ],
            ),
          ),
          body: Navigator(
            key: locator<InventoryCmsNavigator>().navigatorKey,
            initialRoute: InventoryCmsRoutes.home,
            onGenerateRoute: locator<InventoryCmsNavigator>().onGenerateRoute,
          ),
        );

        /* Scaffold(
          appBar: DeliveryAppBar(
            title: "Inventory",
          ),
          endDrawer: GFDrawer(
            child: ListView(
              padding: EdgeInsets.only(top: 25),
              children: [
                viewModel.rootLoading
                    ? Center(
                        child: Container(
                        margin: EdgeInsets.only(
                            top: (MediaQuery.of(context).size.height / 2) - 80),
                        height: 80,
                        width: 80,
                        child: MyProgressIndicator(),
                      ))
                    : FullCategoryTree(
                        rootCategory: viewModel.root,
                        categorySelected: viewModel.categorySelected,
                        productSelected: viewModel.productSelected,
                        categoryAction: viewModel.selectCategory,
                        productAction: viewModel.selectProduct)
              ],
            ),
          ),
          body: Navigator(
            key: locator<InventoryCmsNavigator>().navigatorKey,
            initialRoute: InventoryCmsRoutes.home,
            onGenerateRoute: locator<InventoryCmsNavigator>().onGenerateRoute,
          ),
        ); */
      }),
    );
  }
}
