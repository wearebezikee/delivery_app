import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class ManageInventoryScreenViewModel extends ChangeNotifier {
  Category _root;
  Category _categorySelected;
  Product _productSelected;

  Category get root => _root;

  Category get categorySelected => _categorySelected;

  Product get productSelected => _productSelected;

  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  bool _rootLoading = false;

  bool get rootLoading => _rootLoading;

  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();
  InventoryCmsNavigator _inventoryCmsNavigator =
      locator<InventoryCmsNavigator>();

  onViewCreated() async {
    _rootLoading = true;
    notifyListeners();
    AppResponse<Category> rootResponse =
        await _categoryRepository.getFullTreeFrom(Category.ROOT_ID);

    if (rootResponse.isSuccess()) {
      _root = rootResponse.payload;
      _inventoryCmsNavigator.toCategoryView(root);
    } else {
      //TODO handle error
    }
    _rootLoading = false;
    notifyListeners();
  }

  void selectCategory(Category selected) {
    if (_categorySelected == selected) {
      _categorySelected = null;
      _inventoryCmsNavigator.toCategoryView(root);
    } else {
      _categorySelected = selected;
      _productSelected = null;
      _inventoryCmsNavigator.toCategoryView(_categorySelected);
    }
    notifyListeners();
    _settingsNavigator.popScreen();
  }

  void selectProduct(Product selected) {
    if (_productSelected == selected) {
      _productSelected = null;
      _inventoryCmsNavigator.toCategoryView(root);
    } else {
      _productSelected = selected;
      _categorySelected = null;
      _inventoryCmsNavigator.toProductView(_productSelected);
    }
    notifyListeners();
    _settingsNavigator.popScreen();
  }
}
