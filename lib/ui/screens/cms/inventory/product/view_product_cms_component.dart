import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/images/product_image.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:flutter/material.dart';

class ViewProductCmsComponent extends StatelessWidget {
  final Product product;

  ViewProductCmsComponent({this.product});
  InventoryCmsNavigator _inventoryCmsNavigator =
      locator<InventoryCmsNavigator>();

  @override
  Widget build(BuildContext context) {
    return ImageCmsScreen(
      image: ProductImage(product.imageUrl),
      body: Column(
        children: <Widget>[
          Container(
            child: Text(
              product.title,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(
              product.price.toString() + ' \$',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 15),
            child: Text(
              product.description,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 25),
            width: MediaQuery.of(context).size.width - (horizontalMargin * 2),
            child: PrimaryButtonText(
              text: "Editar",
              action: () {
                _inventoryCmsNavigator.toProductUpdate(product);
              },
            ),
          )
        ],
      ),
    );
  }
}
