import 'dart:io';

import 'package:delivery_app/data_access/repositories/product/product_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class UpdateProductCmsComponentViewModel extends ChangeNotifier {
  Product _product;

  UpdateProductCmsComponentViewModel({@required Product product}) {
    assert(product != null);
    _product = product;
    _titleController.text = product.title;
    _descriptionController.text = product.description;
    _priceController.text = product.price.toString();
  }

  Product get product => _product;

  TextEditingController _titleController = TextEditingController();

  TextEditingController get titleController => _titleController;

  TextEditingController _priceController = TextEditingController();

  TextEditingController get priceController => _priceController;

  TextEditingController _descriptionController = TextEditingController();

  TextEditingController get descriptionController => _descriptionController;

  bool _loadingEdit = false;

  bool get loadingEdit => _loadingEdit;

  bool _loadingDelete = false;

  bool get loadingDelete => _loadingDelete;

  File _selectedImage;

  File get selectedImage => _selectedImage;

  ProductRepository _productRepository = locator<ProductRepository>();
  ImageResourceRepository _imageResourceRepository =
      locator<ImageResourceRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  Future<void> pickImage() async {
    PickedFile pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    _selectedImage = File(pickedFile.path);
    notifyListeners();
  }

  Future<void> onUpdateProductTapped() async {
    _loadingEdit = true;
    notifyListeners();

    if (_validateForm()) {
      if (_selectedImage != null) {
        AppResponse<String> responseImageUrl = await _imageResourceRepository
            .uploadProductImage(_selectedImage, _product.id);
        if (responseImageUrl.isSuccess()) {
          _product.imageUrl = responseImageUrl.payload;
        } else {
          MyNotification.showError(subtitle: responseImageUrl.error.toString());
        }
      }

      _product.title = _titleController.text;
      _product.price = double.parse(_priceController.text);
      _product.description = _descriptionController.text;

      AppResponse response = await _productRepository.update(_product);

      if (response.isSuccess()) {
        MyNotification.showSuccess(subtitle: "Product Updated!");
        _settingsNavigator.popScreen();
      } else {
        MyNotification.showError(subtitle: response.error.toString());
      }
    }
    _loadingEdit = false;
    notifyListeners();
  }

  bool _validateForm() {
    try {
      if (_titleController.text.length < 3) {
        throw FormFieldValidationException.tooShortProductName();
      }
      if (_priceController.text.length < 1) {
        throw FormFieldValidationException.noPrice();
      }
      if (_descriptionController.text.length < 3) {
        throw FormFieldValidationException.tooShortProductDescription();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }

  Future<void> onDeleteProductTapped() async {
    _loadingDelete = true;
    notifyListeners();

    AppResponse deleteResponse = await _productRepository.delete(product.id);

    if (deleteResponse.isSuccess()) {
      MyNotification.showSuccess(subtitle: "Product Deleted!");
      _settingsNavigator.popScreen();
    } else {
      MyNotification.showError(subtitle: deleteResponse.error.toString());
    }
  }
}
