import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/images/product_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:delivery_app/ui/screens/cms/inventory/product/update_product_cms_component_view_model.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:provider/provider.dart';

class UpdateProductCmsComponent extends StatelessWidget {
  final Product product;

  UpdateProductCmsComponent({this.product});

  UpdateProductCmsComponentViewModel createViewModel() {
    return UpdateProductCmsComponentViewModel(product: product);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<UpdateProductCmsComponentViewModel>(
        create: (context) => createViewModel(),
        child: Consumer<UpdateProductCmsComponentViewModel>(
            builder: (context, viewModel, staticChild) {
          return ImageCmsScreen(
            image: _buildImage(viewModel),
            imageAction: viewModel.pickImage,
            body: _buildForm(viewModel, context),
          );
        }));
  }

  Widget _buildImage(UpdateProductCmsComponentViewModel viewModel) {
    return viewModel.selectedImage == null
        ? ProductImage(viewModel.product.imageUrl)
        : ProductImage.fromFileImage(viewModel.selectedImage);
  }

  Widget _buildForm(
      UpdateProductCmsComponentViewModel viewModel, BuildContext context) {
    return Column(
      children: <Widget>[
        Column(
          children: <Widget>[
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  preffix: Text("Nombre"),
                  textController: viewModel.titleController,
                ),
              ),
            ),
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  preffix: Text("Precio"),
                  textController: viewModel.priceController,
                ),
              ),
            ),
            RoundedContainer(
              withElevation: true,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1,
                  vertical: 10),
              child: FormFieldContainer(
                withDivider: true,
                child: FormFieldText(
                  preffix: Text("Descripción"),
                  textController: viewModel.descriptionController,
                ),
              ),
            ),
          ],
        ),
        viewModel.loadingEdit
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: MyProgressIndicator(),
              )
            : Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: Container(
                  width: MediaQuery.of(context).size.width -
                      (2 * horizontalMargin),
                  child: PrimaryButtonText(
                    text: "Editar",
                    action: viewModel.onUpdateProductTapped,
                  ),
                ),
              ),
        viewModel.loadingDelete
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: MyProgressIndicator(),
              )
            : Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: Container(
                  width: MediaQuery.of(context).size.width -
                      (2 * horizontalMargin),
                  child: PrimaryButtonText(
                    color: Colors.red,
                    text: "Eliminar",
                    action: viewModel.onDeleteProductTapped,
                  ),
                ),
              ),
      ],
    );
  }
}
