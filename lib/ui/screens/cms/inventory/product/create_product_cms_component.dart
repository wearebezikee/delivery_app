import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/images/product_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:provider/provider.dart';

import 'create_product_cms_component_view_model.dart';

class CreateProductCmsComponent extends StatelessWidget {
  final String parentCategoryId;

  CreateProductCmsComponent({this.parentCategoryId});

  CreateProductCmsComponentViewModel createViewModel() {
    return CreateProductCmsComponentViewModel(
        parentCategoryId: parentCategoryId);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CreateProductCmsComponentViewModel>(
        create: (context) => createViewModel(),
        child: Consumer<CreateProductCmsComponentViewModel>(
            builder: (context, viewModel, staticChild) {
          return ImageCmsScreen(
            image: _buildImage(viewModel),
            imageAction: viewModel.pickImage,
            body: _buildForm(viewModel, context),
          );
        }));
  }

  Widget _buildImage(CreateProductCmsComponentViewModel viewModel) {
    return viewModel.selectedImage == null
        ? ProductImage(viewModel.product.imageUrl)
        : ProductImage.fromFileImage(viewModel.selectedImage);
  }

  Widget _buildForm(
      CreateProductCmsComponentViewModel viewModel, BuildContext context) {
    return Column(
      //TODO time and dayofwwek dropdowns.
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              preffix: Text("Nombre"),
              textController: viewModel.titleController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              preffix: Text("Precio"),
              textController: viewModel.priceController,
            ),
          ),
        ),
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1,
              vertical: 10),
          child: FormFieldContainer(
            withDivider: true,
            child: FormFieldText(
              preffix: Text("Descripción"),
              textController: viewModel.descriptionController,
            ),
          ),
        ),
        viewModel.loading
            ? MyProgressIndicator()
            : Container(
                width:
                    MediaQuery.of(context).size.width - (2 * horizontalMargin),
                child: PrimaryButtonText(
                  text: "Crear",
                  action: viewModel.onCreateProductTapped,
                ),
              ),
      ],
    );
  }
}
