import 'dart:io';

import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/product/product_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CreateProductCmsComponentViewModel extends ChangeNotifier {
  Product _product;

  CreateProductCmsComponentViewModel({@required String parentCategoryId}) {
    assert(parentCategoryId != null);
    _product = Product.empty();
    _product.id = parentCategoryId;
  }

  Product get product => _product;

  TextEditingController _titleController = TextEditingController();

  TextEditingController get titleController => _titleController;

  TextEditingController _priceController = TextEditingController();

  TextEditingController get priceController => _priceController;

  TextEditingController _descriptionController = TextEditingController();

  TextEditingController get descriptionController => _descriptionController;

  bool _loading = false;

  bool get loading => _loading;

  File _selectedImage;

  File get selectedImage => _selectedImage;

  ProductRepository _productRepository = locator<ProductRepository>();
  ImageResourceRepository _imageResourceRepository =
      locator<ImageResourceRepository>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();

  Future<void> pickImage() async {
    PickedFile pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    _selectedImage = File(pickedFile.path);
    notifyListeners();
  }

  Future<void> onCreateProductTapped() async {
    _loading = true;
    notifyListeners();

    if (_validateForm()) {
      _product.title = _titleController.text;
      _product.id =
          _product.id + CategoryFirebaseDao.ID_SEPARATOR + _product.title;
      if (_selectedImage != null) {
        AppResponse<String> responseImageUrl = await _imageResourceRepository
            .uploadProductImage(_selectedImage, _product.id);
        if (responseImageUrl.isSuccess()) {
          _product.imageUrl = responseImageUrl.payload;
          _product.price = double.parse(_priceController.text);
          _product.description = _descriptionController.text;

          AppResponse response = await _productRepository.create(_product);

          if (response.isSuccess()) {
            MyNotification.showSuccess(subtitle: "Product Created!");
            _settingsNavigator.popScreen();
          } else {
            MyNotification.showError(subtitle: response.error.toString());
          }
        } else {
          MyNotification.showError(subtitle: responseImageUrl.error.toString());
        }
      }
    }
    _loading = false;
    notifyListeners();
  }

  bool _validateForm() {
    try {
      if (_titleController.text.length < 3) {
        throw FormFieldValidationException.tooShortProductName();
      }
      if (_priceController.text.length < 1) {
        throw FormFieldValidationException.noPrice();
      }
      if (_descriptionController.text.length < 3) {
        throw FormFieldValidationException.tooShortProductDescription();
      }
      if (_selectedImage == null) {
        throw FormFieldValidationException.noImageSelected();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }
}
