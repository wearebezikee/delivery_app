import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/images/product_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:flutter/material.dart';
import 'package:getflutter/components/card/gf_card.dart';
import 'package:provider/provider.dart';

import 'create_category_cms_component_view_model.dart';

class CreateCategoryCmsComponent extends StatelessWidget {
  final String parentCategoryId;

  CreateCategoryCmsComponent({this.parentCategoryId});

  CreateCategoryCmsComponentViewModel createViewModel() {
    return CreateCategoryCmsComponentViewModel(
        parentCategoryId: parentCategoryId);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
        value: createViewModel(),
        child: Consumer<CreateCategoryCmsComponentViewModel>(
            builder: (context, viewModel, staticChild) {
          return ImageCmsScreen(
              image: _buildImage(viewModel),
              imageAction: viewModel.pickImage,
              body: _buildForm(viewModel, context));
        }));
  }

  Widget _buildImage(CreateCategoryCmsComponentViewModel viewModel) {
    return viewModel.selectedImage == null
        ? ProductImage(viewModel.category.imageUrl)
        : ProductImage.fromFileImage(viewModel.selectedImage);
  }

  Widget _buildForm(
      CreateCategoryCmsComponentViewModel viewModel, BuildContext context) {
    return Column(
      children: <Widget>[
        RoundedContainer(
          withElevation: true,
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1),
          child: FormFieldContainer(
            child: FormFieldText(
              backgroundColor: Colors.white,
              preffix: Text("Nombre"),
              textController: viewModel.nameController,
            ),
          ),
        ),
        viewModel.loadingCreate
            ? Padding(
                padding: const EdgeInsets.symmetric(vertical: 7),
                child: MyProgressIndicator(),
              )
            : Container(
                margin: EdgeInsets.only(top: 25),
                width:
                    MediaQuery.of(context).size.width - (2 * horizontalMargin),
                child: PrimaryButtonText(
                  text: "Crear",
                  action: viewModel.onCreateCategoryTapped,
                ),
              ),
      ],
    );
  }
}
