import 'dart:io';

import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

class CreateCategoryCmsComponentViewModel extends ChangeNotifier {
  Category _category;

  CreateCategoryCmsComponentViewModel({@required String parentCategoryId}) {
    assert(parentCategoryId != null);
    _category = Category.empty();
    _category.id = parentCategoryId;
  }

  Category get category => _category;

  File _selectedImage;

  File get selectedImage => _selectedImage;

  TextEditingController _nameController = TextEditingController();

  TextEditingController get nameController => _nameController;

  ImageResourceRepository _imageResourceRepository =
      locator<ImageResourceRepository>();
  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  bool _loadingCreate = false;

  bool get loadingCreate => _loadingCreate;

  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();
  InventoryCmsNavigator _inventoryCmsNavigator =
      locator<InventoryCmsNavigator>();

  Future<void> pickImage() async {
    PickedFile pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    _selectedImage = File(pickedFile.path);
    notifyListeners();
  }

  Future<void> onCreateCategoryTapped() async {
    _loadingCreate = true;
    notifyListeners();

    if (_validateForm()) {
      if (_selectedImage != null) {
        _category.name = _nameController.text;
        _category.id =
            _category.id + CategoryFirebaseDao.ID_SEPARATOR + _category.name;
        AppResponse<String> responseImageUrl = await _imageResourceRepository
            .uploadCategoryImage(_selectedImage, _category.id);
        if (responseImageUrl.isSuccess()) {
          _category.imageUrl = responseImageUrl.payload;

          AppResponse response = await _categoryRepository.create(_category);
          if (response.isSuccess()) {
            MyNotification.showSuccess(subtitle: "Category created!");
            _settingsNavigator.popScreen();
          } else {
            MyNotification.showError(subtitle: response.error.toString());
          }
        } else {
          MyNotification.showError(subtitle: responseImageUrl.error.toString());
        }
      }
    }
    _loadingCreate = false;
    notifyListeners();
  }

  bool _validateForm() {
    try {
      if (_nameController.text.length < 3) {
        throw FormFieldValidationException.tooShortCategoryName();
      }
      if (_selectedImage == null) {
        throw FormFieldValidationException.noImageSelected();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }
}
