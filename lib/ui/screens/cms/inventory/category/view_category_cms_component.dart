import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/images/category_image.dart';
import 'package:delivery_app/ui/screens/cms/inventory/components/image_cms_screen.dart';
import 'package:flutter/material.dart';

class ViewCategoryCmsComponent extends StatelessWidget {
  final Category category;

  ViewCategoryCmsComponent({this.category});

  InventoryCmsNavigator _inventoryCmsNavigator =
      locator<InventoryCmsNavigator>();

  @override
  Widget build(BuildContext context) {
    return ImageCmsScreen(
      image: CategoryImage(category.imageUrl),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.02),
            child: Text(
              category.name,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          category.isRoot()
              ? Container()
              : Container(
                  margin: EdgeInsets.only(
                      bottom: MediaQuery.of(context).size.height * 0.02),
                  width: MediaQuery.of(context).size.width -
                      (horizontalMargin * 2),
                  child: PrimaryButtonText(
                    text: "Editar categoría",
                    color: Theme.of(context).colorScheme.primary,
                    action: () {
                      _inventoryCmsNavigator.toCategoryUpdate(category);
                    },
                  ),
                ),
          Container(
            margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).size.height * 0.02),
            width: MediaQuery.of(context).size.width - (horizontalMargin * 2),
            child: PrimaryButtonText(
              text: "Crear sub categoría",
              color: Theme.of(context).colorScheme.primary,
              action: () {
                _inventoryCmsNavigator.toCategoryCreate(category.id);
              },
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - (horizontalMargin * 2),
            child: PrimaryButtonText(
              text: "Crear producto",
              color: Theme.of(context).colorScheme.primary,
              action: () {
                _inventoryCmsNavigator.toProductCreate(category.id);
              },
            ),
          ),
        ],
      ),
    );
  }
}
