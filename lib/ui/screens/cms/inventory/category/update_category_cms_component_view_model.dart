import 'dart:io';

import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_picker/image_picker.dart';

class UpdateCategoryCmsComponentViewModel extends ChangeNotifier {
  Category _category;

  UpdateCategoryCmsComponentViewModel({@required Category category}) {
    assert(category != null);
    _category = category;
    _nameController.text = _category.name;
  }

  Category get category => _category;

  File _selectedImage;

  File get selectedImage => _selectedImage;

  TextEditingController _nameController = TextEditingController();

  TextEditingController get nameController => _nameController;

  ImageResourceRepository _imageResourceRepository =
      locator<ImageResourceRepository>();
  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  bool _loadingEdit = false;

  bool get loadingEdit => _loadingEdit;

  bool _loadingDelete = false;

  bool get loadingDelete => _loadingDelete;

  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();
  InventoryCmsNavigator _inventoryCmsNavigator =
      locator<InventoryCmsNavigator>();

  Future<void> pickImage() async {
    PickedFile pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    _selectedImage = File(pickedFile.path);
    notifyListeners();
  }

  Future<void> onUpdateCategoryTapped() async {
    _loadingEdit = true;
    notifyListeners();

    if (_validateForm()) {
      if (_selectedImage != null) {
        AppResponse<String> responseImageUrl = await _imageResourceRepository
            .uploadCategoryImage(_selectedImage, _category.id);
        if (responseImageUrl.isSuccess()) {
          _category.imageUrl = responseImageUrl.payload;
        } else {
          MyNotification.showError(subtitle: responseImageUrl.error.toString());
        }
      }
      _category.name = _nameController.text;
      AppResponse response = await _categoryRepository.update(_category);
      if (response.isSuccess()) {
        MyNotification.showSuccess(subtitle: "Category modified!");
        _settingsNavigator.popScreen();
      } else {
        MyNotification.showError(subtitle: response.error.toString());
      }
    }
    _loadingEdit = false;
    notifyListeners();
  }

  Future<void> onDeleteProductTapped() async {
    _loadingDelete = true;
    notifyListeners();

    AppResponse deleteResponse = await _categoryRepository.delete(_category.id);

    if (deleteResponse.isSuccess()) {
      MyNotification.showSuccess(subtitle: "Category Deleted!");
      _settingsNavigator.popScreen();
    } else {
      MyNotification.showError(subtitle: deleteResponse.error.toString());
    }
  }

  bool _validateForm() {
    try {
      if (_nameController.text.length < 3) {
        throw FormFieldValidationException.tooShortCategoryName();
      }

      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }
}
