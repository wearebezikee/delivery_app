import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/icon_title_row.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_information_fields/pago_movil_payment_method_information.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_container.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_information_fields/transfer_payment_method_information.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_information_fields/zelle_payment_method_information.dart';

import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class PaymentMethods extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return RoundedContainer(
          topLeft: true,
          topRight: true,
          bottomLeft: true,
          bottomRight: true,
          withElevation: true,
          color: Colors.white,
          borderRadius: 5,
          margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.1,
          ),
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.03),
          child: Column(
            children: <Widget>[
              IconTitleRow(
                iconData: FontAwesomeIcons.wallet,
                title: "Método de pago",
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.15,
                child: ListView(
                  padding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.05),
                  scrollDirection: Axis.horizontal,
                  children: _buildPaymentMethods(context, viewModel),
                ),
              ),
              _buildPaymentMethodInformation(context, viewModel),
            ],
          ),
        );
      },
    );
  }

  List<Widget> _buildPaymentMethods(
      BuildContext context, PaymentDetailScreenViewModel viewModel) {
    List<Widget> output = List();
    for (int i = 0; i < viewModel.paymentMethods.length; i++) {
      output.add(
        PaymentContainer(
          action: () => viewModel.selectPaymentMethod(i),
          iconData: FontAwesomeIcons.moneyBill,
          title: viewModel.paymentMethods[i].title,
          selected: viewModel.selectedPaymentMethod == i,
        ),
      );
      if (i < viewModel.paymentMethods.length - 1) {
        output.add(Container(
          width: MediaQuery.of(context).size.width * 0.05,
        ));
      }
    }
    return output;
  }

  Widget _buildPaymentMethodInformation(
      BuildContext context, PaymentDetailScreenViewModel viewModel) {
    PaymentMethodSetting paymentMethod =
        viewModel.paymentMethods[viewModel.selectedPaymentMethod];
    switch (paymentMethod.type) {
      case Payment.TYPE_CASH:
        return Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05,
                  vertical: MediaQuery.of(context).size.height * 0.02),
              alignment: Alignment.centerLeft,
              child: Text(
                'Cantidad de efectivo',
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
            FormFieldContainer(
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05),
              child: FormFieldText(
                textController: viewModel.cashAmmountController,
                backgroundColor: Theme.of(context).backgroundColor,
                contentPadding: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.05),
              ),
            ),
          ],
        );
        break;
      case Payment.TYPE_PAGO_MOVIL:
        return PagoMovilPaymentMethodInformation(paymentMethod: paymentMethod);
        break;
      case Payment.TYPE_TRANSFER:
        return TransferPaymentMethodInformation(paymentMethod: paymentMethod);
        break;
      case Payment.TYPE_ZELLE:
        return ZellePaymentMethodInformation(paymentMethod: paymentMethod);
        break;
      default:
        return Container();
    }
  }
}
