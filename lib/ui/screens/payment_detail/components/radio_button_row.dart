import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:flutter/material.dart';

class RadioButtonRow extends StatelessWidget {
  final int selectedButton;
  final int buttonValue;
  final String title;
  final Function action;

  const RadioButtonRow(
      {@required this.selectedButton,
      @required this.action,
      @required this.buttonValue,
      @required this.title});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => action(buttonValue),
      child: RoundedContainer(
        topLeft: true,
        topRight: true,
        bottomLeft: true,
        bottomRight: true,
        borderRadius: 5,
        color: Theme.of(context).backgroundColor,
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.05,
            vertical: MediaQuery.of(context).size.width * 0.007),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  left: MediaQuery.of(context).size.width * 0.05),
              child: Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontWeight: FontWeight.normal),
              ),
            ),
            Radio(
              value: buttonValue,
              groupValue: selectedButton,
              activeColor: Theme.of(context).primaryColor,
              onChanged: (value) => action(value),
            ),
          ],
        ),
      ),
    );
  }
}
