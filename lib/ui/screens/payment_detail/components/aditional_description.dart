import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/icon_title_row.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class AditionalDescription extends StatelessWidget {
  const AditionalDescription({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return RoundedContainer(
          topLeft: true,
          topRight: true,
          bottomLeft: true,
          bottomRight: true,
          withElevation: true,
          color: Colors.white,
          borderRadius: 5,
          margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.1,
            vertical: MediaQuery.of(context).size.height * 0.02,
          ),
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.03),
          child: Column(
            children: <Widget>[
              IconTitleRow(
                iconData: FontAwesomeIcons.pencilAlt,
                title: "Descripción adicional",
              ),
              FormFieldContainer(
                height: MediaQuery.of(context).size.height * 0.1,
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.05),
                child: FormFieldText(
                  textController: viewModel.descriptionController,
                  backgroundColor: Theme.of(context).backgroundColor,
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.05),
                  maxLines: null,
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
