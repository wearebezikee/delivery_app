import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PaymentContainer extends StatelessWidget {
  final String title;
  final IconData iconData;
  final bool selected;
  final Function action;

  const PaymentContainer(
      {@required this.title,
      @required this.iconData,
      this.selected = false,
      @required this.action});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => action(),
      child: RoundedContainer(
        width: MediaQuery.of(context).size.height * 0.15,
        color: Theme.of(context).backgroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                    right: MediaQuery.of(context).size.width * 0.015,
                    top: MediaQuery.of(context).size.width * 0.015,
                  ),
                  child: selected
                      ? Icon(
                          FontAwesomeIcons.checkCircle,
                          size: MediaQuery.of(context).size.width * 0.045,
                          color: Theme.of(context).primaryColor,
                        )
                      : Container(
                          height: MediaQuery.of(context).size.width * 0.045),
                ),
              ],
            ),
            Icon(
              iconData,
              size: MediaQuery.of(context).size.width * 0.09,
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.015,
            ),
            Container(
              child: Text(
                title,
                style: Theme.of(context).textTheme.bodyText2,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
