import 'package:delivery_app/ui/screens/payment_detail/components/address_item.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddressesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return Column(
          children: <Widget>[
            for (Widget item in _buildAdresses(viewModel)) item,
          ],
        );
      },
    );
  }

  List<Widget> _buildAdresses(PaymentDetailScreenViewModel viewModel) {
    List<Widget> output = List();
    for (int i = 0; i < viewModel.addresses.length; i++) {
      output.add(
        AddressItem(
          title: viewModel.addresses[i].address1,
          buttonValue: i,
          selectedButton: viewModel.selectedAddress,
          action: viewModel.changeAddress,
        ),
      );
    }
    return output;
  }
}
