import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:flutter/material.dart';

class PaymentInformationField extends StatelessWidget {
  final String title;
  final TextEditingController textController;

  const PaymentInformationField({
    @required this.title,
    @required this.textController,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.02),
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        FormFieldContainer(
          child: FormFieldText(
            textController: textController,
            backgroundColor: Theme.of(context).backgroundColor,
            contentPadding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.05),
          ),
        ),
      ],
    );
  }
}
