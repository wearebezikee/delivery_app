import 'package:flutter/material.dart';

class ProductRow extends StatelessWidget {
  final String name;
  final int quantity;
  final double price;

  const ProductRow({
    @required this.name,
    @required this.quantity,
    @required this.price,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.005),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                name,
                style: Theme.of(context).textTheme.headline3,
              ),
              Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.035),
                child: Text(
                  '(' + quantity.toString() + ')',
                  style: Theme.of(context).textTheme.headline3,
                ),
              ),
            ],
          ),
          Text(
            (price * quantity).toString() + ' \$',
            style: Theme.of(context).textTheme.headline3,
          ),
        ],
      ),
    );
  }
}
