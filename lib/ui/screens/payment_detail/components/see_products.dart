import 'package:delivery_app/misc/cart_helper.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/product_row.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SeeProducts extends StatefulWidget {
  @override
  _SeeProductsState createState() => _SeeProductsState();
}

class _SeeProductsState extends State<SeeProducts> {
  bool showProducts;

  @override
  void initState() {
    super.initState();
    showProducts = false;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return RoundedContainer(
          topLeft: true,
          topRight: true,
          bottomLeft: true,
          bottomRight: true,
          withElevation: true,
          borderRadius: 5,
          padding: EdgeInsets.symmetric(
              vertical: MediaQuery.of(context).size.height * 0.02),
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.1),
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.01),
                child: _buildProductRow(viewModel),
              ),
              showProducts ? _buildProductsColumn(viewModel) : Container(),
            ],
          ),
        );
      },
    );
  }

  Widget _buildProductRow(PaymentDetailScreenViewModel viewModel) {
    return InkWell(
      onTap: () {
        setState(() {
          showProducts = !showProducts;
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin:
                EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Ver productos",
                  style: Theme.of(context).textTheme.headline2,
                ),
                Container(height: MediaQuery.of(context).size.height * 0.02),
                Text(
                  viewModel.getTotalProducts().toString() + " productos",
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * 0.03),
            child: Icon(
              showProducts
                  ? FontAwesomeIcons.angleDown
                  : FontAwesomeIcons.angleRight,
              size: MediaQuery.of(context).size.height * 0.05,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProductsColumn(PaymentDetailScreenViewModel viewModel) {
    List<Widget> productRows = List();
    productRows.add(Divider(
      thickness: 1,
    ));
    for (CartItem item in CartHelper.getCart()) {
      productRows.add(ProductRow(
        name: item.product.title,
        quantity: item.quantity,
        price: item.product.price,
      ));
    }
    productRows.add(Divider(
      thickness: 1,
    ));
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05),
      child: Column(
        children: productRows,
      ),
    );
  }
}
