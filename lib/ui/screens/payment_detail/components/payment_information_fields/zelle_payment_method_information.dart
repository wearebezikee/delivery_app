import 'dart:math';

import 'package:delivery_app/model/payment/payment_method_setting/zelle_payment_method_setting.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/images/payment_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/information_field.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_information_field.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_information_fields/image_upload_field.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

class ZellePaymentMethodInformation extends StatefulWidget {
  final ZellePaymentMethodSetting paymentMethod;

  const ZellePaymentMethodInformation({@required this.paymentMethod});

  @override
  _ZellePaymentMethodInformationState createState() =>
      _ZellePaymentMethodInformationState();
}

class _ZellePaymentMethodInformationState
    extends State<ZellePaymentMethodInformation> {
  bool showInformation;

  @override
  void initState() {
    super.initState();
    showInformation = false;
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return Container(
          margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.05,
          ),
          child: Column(
            children: <Widget>[
              _buildInformationRow(),
              showInformation ? _buildBankInformation() : Container(),
              _buildPaymentInformation(viewModel),
            ],
          ),
        );
      },
    );
  }

  Widget _buildInformationRow() {
    return InkWell(
      onTap: () {
        setState(() {
          showInformation = !showInformation;
        });
      },
      child: Container(
        padding:
            EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Información', style: Theme.of(context).textTheme.headline1),
            Icon(
              showInformation
                  ? FontAwesomeIcons.angleUp
                  : FontAwesomeIcons.angleDown,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBankInformation() {
    return Column(
      children: <Widget>[
        Divider(
          thickness: 1,
        ),
        InformationField(
          title: 'Email',
          information: widget.paymentMethod.email,
        ),
        InformationField(
          title: 'Descripción',
          information: widget.paymentMethod.description,
        ),
        Divider(
          thickness: 1,
        ),
      ],
    );
  }

  Widget _buildPaymentInformation(PaymentDetailScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(
              horizontal: MediaQuery.of(context).size.width * 0.05,
              vertical: MediaQuery.of(context).size.height * 0.02),
          alignment: Alignment.centerLeft,
          child: Text(
            'Comprobante de pago',
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        ImageUploadField(),
      ],
    );
  }
}
