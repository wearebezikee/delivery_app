import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/images/payment_image.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ImageUploadField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return Column(
          children: <Widget>[
            viewModel.confirmationImage != null
                ? PaymentImage.fromFileImage(viewModel.confirmationImage)
                : Container(),
            PrimaryButtonText(
              action: () {
                viewModel.showBottomSheet(context);
              },
              text: 'Subir Comprobante',
              textStyle: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: Colors.white),
            ),
          ],
        );
      },
    );
  }
}
