import 'package:flutter/material.dart';

class InformationField extends StatelessWidget {
  final String title;
  final String information;

  const InformationField({this.title, this.information});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.02,
            bottom: MediaQuery.of(context).size.height * 0.01,
          ),
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline2,
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: Text(
            information,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
      ],
    );
  }
}
