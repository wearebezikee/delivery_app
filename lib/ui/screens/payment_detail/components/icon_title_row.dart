import 'package:flutter/material.dart';

class IconTitleRow extends StatelessWidget {
  final String title;
  final IconData iconData;

  const IconTitleRow({@required this.title, @required this.iconData});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        bottom: MediaQuery.of(context).size.height * 0.02,
      ),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.05,
                right: MediaQuery.of(context).size.width * 0.02),
            child: Icon(
              iconData,
              size: 20,
            ),
          ),
          Text(
            title,
            style: Theme.of(context).textTheme.headline2,
          ),
        ],
      ),
    );
  }
}
