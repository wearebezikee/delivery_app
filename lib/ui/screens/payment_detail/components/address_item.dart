import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:flutter/material.dart';

class AddressItem extends StatelessWidget {
  final String title;
  final int buttonValue;
  final int selectedButton;
  final Function action;

  const AddressItem(
      {this.title, this.buttonValue, this.selectedButton, this.action});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => action(buttonValue),
      child: RoundedContainer(
        topLeft: true,
        topRight: true,
        bottomLeft: true,
        bottomRight: true,
        borderRadius: 5,
        color: Theme.of(context).backgroundColor,
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.05,
            vertical: MediaQuery.of(context).size.width * 0.007),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Container(
                margin: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.05),
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .headline5
                      .copyWith(fontWeight: FontWeight.normal),
                ),
              ),
            ),
            Radio(
              value: buttonValue,
              groupValue: selectedButton,
              activeColor: Theme.of(context).primaryColor,
              onChanged: (value) => action(value),
            ),
          ],
        ),
      ),
    );
  }
}
