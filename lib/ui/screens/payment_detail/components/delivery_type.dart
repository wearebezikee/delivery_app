import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/cms/addresses/create_address_screen.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/addresses_list.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/icon_title_row.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/radio_button_row.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class DeliveryType extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<PaymentDetailScreenViewModel>(
      builder: (context, viewModel, staticChild) {
        return RoundedContainer(
          topLeft: true,
          topRight: true,
          bottomLeft: true,
          bottomRight: true,
          withElevation: true,
          color: Colors.white,
          borderRadius: 5,
          margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.1,
            vertical: MediaQuery.of(context).size.height * 0.02,
          ),
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.03),
          child: Column(
            children: <Widget>[
              IconTitleRow(
                iconData: FontAwesomeIcons.shoppingBag,
                title: "Tipo de entrega",
              ),
              RadioButtonRow(
                title: "Entrega a domicilio",
                action: viewModel.changeDeliveryType,
                buttonValue: 1,
                selectedButton: viewModel.selectedDeliveryType,
              ),
              RadioButtonRow(
                title: "Recoger en local",
                action: viewModel.changeDeliveryType,
                buttonValue: 2,
                selectedButton: viewModel.selectedDeliveryType,
              ),
              viewModel.showAddressField
                  ? IconTitleRow(
                      iconData: FontAwesomeIcons.mapMarkerAlt,
                      title: "Dirección de entrega",
                    )
                  : Container(),
              viewModel.showAddressField
                  ? Column(
                      children: <Widget>[
                        AddressesList(),
                        Container(
                          margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01),
                          child: PrimaryButtonText(
                            action: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(
                                      builder: (context) =>
                                          CreateAddressScreen()))
                                  .then(
                                    (value) => viewModel.loadData(),
                                  );
                            },
                            text: 'Nueva dirección',
                            textStyle: Theme.of(context)
                                .textTheme
                                .headline5
                                .copyWith(color: Colors.white),
                          ),
                        ),
                      ],
                    )
                  : Container(),
            ],
          ),
        );
      },
    );
  }
}
