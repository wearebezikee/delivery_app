import 'dart:io';
import 'dart:math';

import 'package:delivery_app/data_access/repositories/order/order_repository.dart';
import 'package:delivery_app/data_access/repositories/payment_method_setting/payment_method_setting_repository.dart';
import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/exceptions/payment_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/cart_helper.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/address.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/model/order_status.dart';
import 'package:delivery_app/model/payment/payment.dart';
import 'package:delivery_app/model/payment/payment_method/cash_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/pago_movil_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/transfer_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method/zelle_payment_method.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/menu_navigator/menu_navigator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PaymentDetailScreenViewModel extends ChangeNotifier {
  final PaymentMethodSettingRepository paymentMethodRepository =
      locator<PaymentMethodSettingRepository>();
  final ImageResourceRepository _imageResourceRepository =
      locator<ImageResourceRepository>();
  final OrderRepository _orderRepository = locator<OrderRepository>();
  final UserRepository _userRepository = locator<UserRepository>();

  final MenuNavigator navigator = locator<MenuNavigator>();
  int _selectedDeliveryType = 1;
  int _selectedAddress = 0;
  int _selectedPaymentMethod = 0;
  bool _showAddressField = true;
  bool _isLoading = false;
  List<PaymentMethodSetting> _paymentMethods = List();
  List<Address> _addresses = List();
  File confirmationImage;
  TextEditingController referenceNumberController = TextEditingController();
  TextEditingController bankNameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController cashAmmountController = TextEditingController();

  get selectedDeliveryType => _selectedDeliveryType;
  get selectedAddress => _selectedAddress;
  get selectedPaymentMethod => _selectedPaymentMethod;
  get showAddressField => _showAddressField;
  get isLoading => _isLoading;
  get paymentMethods => _paymentMethods;
  get addresses => _addresses;

  void loadData() async {
    _isLoading = true;
    notifyListeners();

    _addresses = LoggedUserHelper.getLoggedUser().addresses;

    AppResponse response = await paymentMethodRepository.getAll();
    if (response.isSuccess()) {
      _paymentMethods = response.payload;
    }

    _isLoading = false;
    notifyListeners();
  }

  void changeDeliveryType(int value) {
    _selectedDeliveryType = value;
    if (value == 2) {
      _showAddressField = false;
    } else {
      _showAddressField = true;
    }
    notifyListeners();
  }

  void changeAddress(int value) {
    _selectedAddress = value;
    notifyListeners();
  }

  void selectPaymentMethod(int i) {
    _selectedPaymentMethod = i;
    notifyListeners();
  }

  double getTotal() {
    double total = 0;
    for (CartItem cartItem in CartHelper.getCart()) {
      total = total + (cartItem.product.price * cartItem.quantity);
    }
    return total;
  }

  Future<void> payOrder(BuildContext context) async {
    try {
      Order order = Order();
      order.pickup = _selectedDeliveryType == 2;
      if (!order.pickup) {
        order.address = addresses[_selectedAddress];
      }
      order.observation = descriptionController.text;
      order.products = CartHelper.getCart();
      order.totalPrice = getTotal();
      order.paymentMethod = await getPaymentMethod();
      order.userName = LoggedUserHelper.getLoggedUser().name ?? 'Guest';
      order.statusHistory = [
        OrderStatus(
            id: "status1",
            title: "Orden creada",
            date: DateTime.now(),
            priority: 1,
            status: OrderStatus.STATUS_SUCCESS),
        OrderStatus(
            id: "status2",
            title: "Orden en cocina",
            date: DateTime.now(),
            priority: 2,
            status: OrderStatus.STATUS_PENDING)
      ];
      AppResponse<Order> response = await _orderRepository.create(order);
      if (response.isSuccess()) {
        CartHelper.clearCart();
        Navigator.of(context).pop();
        Navigator.of(context).pop();
        MyNotification.showSuccess(subtitle: 'Orden recibida!');
      } else {
        MyNotification.showError(subtitle: 'Ocurrió un error con su orden');
      }
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
    }
  }

  //Move validation logic to a service
  Future<PaymentMethod> getPaymentMethod() async {
    PaymentMethodSetting paymentMethodSetting =
        _paymentMethods[_selectedPaymentMethod];

    Random random =
        Random(); //TODO for unique identifier on confirmation image, need to change to real unique
    switch (paymentMethodSetting.type) {
      case Payment.TYPE_CASH:
        if (cashAmmountController.text.trim() == null ||
            cashAmmountController.text.trim().length == 0) {
          throw PaymentException.EMPTY_CASH_AMMOUNT;
        }
        return CashPaymentMethod(
            cashAmount: int.parse(cashAmmountController.text));
        break;
      case Payment.TYPE_PAGO_MOVIL:
        if (bankNameController.text.trim() == null ||
            bankNameController.text.trim().length == 0) {
          throw PaymentException.EMPTY_BANK_NAME;
        } else if (referenceNumberController.text.trim() == null ||
            referenceNumberController.text.trim().length == 0) {
          throw PaymentException.EMPTY_REFERENCE_NUMBER;
        } else if (confirmationImage == null) {
          throw PaymentException.EMPTY_CONFIRMATION_IMAGE;
        }
        AppResponse<String> responseImageUrl =
            await _imageResourceRepository.uploadPaymentImage(confirmationImage,
                locator<User>().id + random.nextInt(1000).toString());
        if (responseImageUrl.isSuccess()) {
          return PagoMovilPaymentMethod(
              referenceNumber: referenceNumberController.text,
              confirmationImageUrl: responseImageUrl.payload);
        } else {
          throw PaymentException.ERROR_UPLOADING_IMAGE;
        }
        break;
      case Payment.TYPE_TRANSFER:
        if (bankNameController.text.trim() == null ||
            bankNameController.text.trim().length == 0) {
          throw PaymentException.EMPTY_BANK_NAME;
        } else if (referenceNumberController.text.trim() == null ||
            referenceNumberController.text.trim().length == 0) {
          throw PaymentException.EMPTY_REFERENCE_NUMBER;
        } else if (confirmationImage == null) {
          throw PaymentException.EMPTY_CONFIRMATION_IMAGE;
        }
        AppResponse<String> responseImageUrl =
            await _imageResourceRepository.uploadPaymentImage(confirmationImage,
                locator<User>().id + random.nextInt(1000).toString());
        if (responseImageUrl.isSuccess()) {
          return TransferPaymentMethod(
              referenceNumber: referenceNumberController.text,
              confirmationImageUrl: responseImageUrl.payload);
        } else {
          throw PaymentException.ERROR_UPLOADING_IMAGE;
        }
        break;
      case Payment.TYPE_ZELLE:
        if (confirmationImage == null) {
          throw PaymentException.EMPTY_CONFIRMATION_IMAGE;
        }
        AppResponse<String> responseImageUrl =
            await _imageResourceRepository.uploadPaymentImage(confirmationImage,
                locator<User>().id + random.nextInt(1000).toString());
        if (responseImageUrl.isSuccess()) {
          return ZellePaymentMethod(
              confirmationImageUrl: responseImageUrl.payload);
        } else {
          throw PaymentException.ERROR_UPLOADING_IMAGE;
        }
        break;
      default:
        return null;
    }
  }

  int getTotalProducts() {
    int products = 0;
    for (CartItem cartItem in CartHelper.getCart()) {
      products += cartItem.quantity;
    }
    return products;
  }

  Future<void> selectImage(BuildContext context, ImageSource source) async {
    PickedFile pickedFile = await ImagePicker().getImage(source: source);
    if (pickedFile != null) {
      confirmationImage = File(pickedFile.path);
      notifyListeners();
      Navigator.of(context).pop();
    }
  }

  void showBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(Icons.image),
                  title: new Text(
                    'Galería',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  onTap: () async {
                    selectImage(context, ImageSource.gallery);
                  },
                ),
                new ListTile(
                  leading: new Icon(Icons.camera_alt),
                  title: new Text(
                    'Cámara',
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  onTap: () async {
                    selectImage(context, ImageSource.camera);
                  },
                ),
              ],
            ),
          );
        });
  }
}
