import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/aditional_description.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/delivery_type.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_methods.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/payment_total.dart';
import 'package:delivery_app/ui/screens/payment_detail/components/see_products.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PaymentDetailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Detalles de pago",
      body: ChangeNotifierProvider<PaymentDetailScreenViewModel>(
        create: (context) => _createViewModel()..loadData(),
        child: Consumer<PaymentDetailScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            if (viewModel.isLoading) {
              return MyProgressIndicator();
            } else {
              return Stack(
                children: [
                  ListView(
                    children: <Widget>[
                      SeeProducts(),
                      DeliveryType(),
                      PaymentMethods(),
                      AditionalDescription(),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                      ),
                    ],
                  ),
                  PaymentTotal(
                    action: () => viewModel.payOrder(context),
                    total: viewModel.getTotal().toString(),
                  ),
                ],
              );
            }
          },
        ),
      ),
    );
  }

  PaymentDetailScreenViewModel _createViewModel() {
    return PaymentDetailScreenViewModel();
  }
}
