import 'package:delivery_app/ui/components/buttons/primary_button.dart';
import 'package:delivery_app/ui/screens/cart/components/total_row.dart';
import 'package:flutter/material.dart';

class CartTotal extends StatelessWidget {
  final String subTotal;
  final String total;
  final Function action;

  const CartTotal({this.subTotal, this.total, this.action});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.6),
              offset: Offset(0.0, -1.0), //(x,y)
              blurRadius: 4.0,
            ),
          ],
        ),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.2,
                vertical: MediaQuery.of(context).size.height * 0.015,
              ),
              child: Column(
                children: <Widget>[
                  TotalRow(
                    title: 'Sub Total',
                    total: subTotal + ' \$',
                  ),
                  /* Container(height: 6),
                  TotalRow(
                    title: 'Envío',
                    total: '1.0 \$',
                  ), */
                  Container(height: 6),
                  TotalRow(
                    title: 'Total',
                    total: total + ' \$',
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.15,
                right: MediaQuery.of(context).size.width * 0.15,
                bottom: MediaQuery.of(context).size.height * 0.015,
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: PrimaryButton(
                      action: () => action(),
                      child: Text(
                        'Pagar',
                        style: Theme.of(context).textTheme.button,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
