import 'package:delivery_app/ui/screens/cart/components/icon_item.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class QuantitySelector extends StatelessWidget {
  final String quantity;
  final Function add;
  final Function subtract;

  const QuantitySelector({this.quantity, this.add, this.subtract});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
      height: MediaQuery.of(context).size.height * 0.04,
      width: MediaQuery.of(context).size.width * 0.23,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.6),
            offset: Offset(0.0, 0.0), //(x,y)
            blurRadius: 2.0,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          IconItem(
            icon: Icon(FontAwesomeIcons.minus,
                size: 10, color: Theme.of(context).primaryColor),
            action: () => subtract(),
          ),
          Container(
            child: Text(quantity, style: Theme.of(context).textTheme.bodyText2),
          ),
          IconItem(
            icon: Icon(FontAwesomeIcons.plus,
                size: 10, color: Theme.of(context).primaryColor),
            action: () => add(),
          ),
        ],
      ),
    );
  }
}
