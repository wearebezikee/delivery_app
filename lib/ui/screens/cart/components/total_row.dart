import 'package:flutter/material.dart';

class TotalRow extends StatelessWidget {
  final String title;
  final String total;

  const TotalRow({this.title, this.total});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          title,
          style: Theme.of(context)
              .textTheme
              .headline2
              .copyWith(fontWeight: FontWeight.bold),
        ),
        Text(
          total,
          style: Theme.of(context).textTheme.headline3,
        ),
      ],
    );
  }
}
