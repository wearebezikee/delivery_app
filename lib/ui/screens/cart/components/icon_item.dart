import 'package:flutter/material.dart';

class IconItem extends StatelessWidget {
  final Icon icon;
  final Function action;

  const IconItem({this.icon, this.action});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.04,
        width: MediaQuery.of(context).size.height * 0.13 / 2.3,
        child: icon,
      ),
      onTap: action,
    );
  }
}
