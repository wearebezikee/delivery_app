import 'package:cached_network_image/cached_network_image.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/ui/screens/cart/components/quantity_selector.dart';
import 'package:flutter/material.dart';

class CartItemCard extends StatelessWidget {
  final CartItem cartItem;
  final Function add;
  final Function subtract;

  const CartItemCard({this.cartItem, this.add, this.subtract});

  @override
  Widget build(BuildContext context) {
    double horizontalMargin = MediaQuery.of(context).size.width * 0.1;
    double itemHeight = MediaQuery.of(context).size.height * 0.12;
    return Container(
      height: itemHeight,
      margin: EdgeInsets.only(
        left: horizontalMargin,
        right: horizontalMargin,
        bottom: MediaQuery.of(context).size.height * 0.03,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.6),
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 1.0,
          ),
        ],
      ),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 20),
            width: itemHeight,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: CachedNetworkImageProvider(cartItem.product.imageUrl),
              ),
              borderRadius: BorderRadius.all(Radius.circular(7)),
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  cartItem.product.title,
                  style: Theme.of(context).textTheme.headline2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      (cartItem.product.price * cartItem.quantity).toString() +
                          ' \$',
                      style: Theme.of(context).textTheme.headline3,
                    ),
                    QuantitySelector(
                      quantity: cartItem.quantity.toString(),
                      add: () => add(),
                      subtract: () => subtract(),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
