import 'package:delivery_app/misc/cart_helper.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/screens/cart/cart_screen_view_model.dart';
import 'package:delivery_app/ui/screens/cart/components/cart_item_card.dart';
import 'package:delivery_app/ui/screens/cart/components/cart_total.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatelessWidget {
  final double height = kToolbarHeight * 2.5;

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: 'Tu Pedido',
      body: Consumer<CartScreenViewModel>(
        builder: (context, viewModel, staticChild) {
          if (CartHelper.getCart().length > 0) {
            return _buildCart(context, viewModel);
          } else {
            return _buildEmptyCart(context);
          }
        },
      ),
    );
  }

  Widget _buildEmptyCart(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(top: kToolbarHeight * 2),
      children: <Widget>[
        Center(
          child: Container(
              child: Text('Compranos!',
                  style: Theme.of(context).textTheme.headline3)),
        ),
      ],
    );
  }

  Widget _buildCart(BuildContext context, CartScreenViewModel viewModel) {
    List<Widget> itemWidgets = List();
    double total = 0;
    for (CartItem item in CartHelper.getCart()) {
      total += item.product.price * item.quantity;
      itemWidgets.add(
        CartItemCard(
          cartItem: item,
          add: () => viewModel.add(item),
          subtract: () => viewModel.subtract(item),
        ),
      );
    }
    itemWidgets.add(Container(
      height: MediaQuery.of(context).size.height * 0.22,
    ));
    return Stack(
      children: [
        ListView(
          physics: NeverScrollableScrollPhysics(),
          children: itemWidgets,
        ),
        CartTotal(
          subTotal: total.toString(),
          total: total.toString(),
          action: () => viewModel.paymentAction(),
        ),
      ],
    );
  }
}
