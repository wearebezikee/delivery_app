import 'package:delivery_app/misc/cart_helper.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/menu_navigator/menu_navigator.dart';
import 'package:flutter/foundation.dart';

class CartScreenViewModel extends ChangeNotifier {
  MenuNavigator _navigator = locator<MenuNavigator>();
  int _totalItems = 0;
  final int _maxPerItem = 10;

  get totalItems => _totalItems;

  void add(CartItem item) {
    if (item.quantity < _maxPerItem) {
      item.quantity++;
      _totalItems++;
      notifyListeners();
    }
  }

  void subtract(CartItem item) {
    if (item.quantity > 1) {
      item.quantity--;
      _totalItems--;
    } else {
      _totalItems--;
      CartHelper.removeItem(item);
    }
    notifyListeners();
  }

  addProduct(Product product) {
    for (CartItem item in CartHelper.getCart()) {
      if (item.product.title == product.title) {
        item.quantity++;
        _totalItems++;
        notifyListeners();
        return;
      }
    }
    CartHelper.addItem(CartItem(
      product: product,
      quantity: 1,
    ));
    _totalItems++;
    notifyListeners();
  }

  paymentAction() {
    _navigator.toPaymentDetail();
  }
}
