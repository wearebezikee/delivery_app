import 'package:delivery_app/data_access/repositories/product/product_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/get_current_user_process_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/app_navigator/app_navigator.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void initState() {
    loadAppData();
  }

  loadAppData() async {
    await Future.delayed(Duration(seconds: 1));

    AppNavigator navigator = locator<AppNavigator>();

    GetCurrentUserProcessRepository getCurrentUserProcessRepository =
        locator<GetCurrentUserProcessRepository>();

    AppResponse<User> userResponse = await getCurrentUserProcessRepository.process();

    if (userResponse.isSuccess()) {
      LoggedUserHelper.setLoggedUser(userResponse.payload);
      if (userResponse.payload.isEmptyLoggedUser()) {
        navigator.toCreateUserForm(userResponse.payload.id);
      } else {
        navigator.toDashboard();
      }
    } else {
      navigator.toDashboard();
      MyNotification.showError(subtitle: userResponse.error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          width: MediaQuery.of(context).size.height * 0.2,
          height: MediaQuery.of(context).size.height * 0.2,
          child: MyProgressIndicator(),
        ),
      ),
    );
  }
}
