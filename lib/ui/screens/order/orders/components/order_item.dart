import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:flutter/material.dart';

class OrderItem extends StatelessWidget {
  final Order order;
  final Function action;

  const OrderItem({@required this.order, @required this.action});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => action(),
      child: RoundedContainer(
        margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.1,
          vertical: MediaQuery.of(context).size.height * 0.01,
        ),
        padding: EdgeInsets.all(MediaQuery.of(context).size.width * 0.03),
        topLeft: true,
        topRight: true,
        bottomLeft: true,
        bottomRight: true,
        borderRadius: 5,
        withElevation: true,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "# 34",
              style: Theme.of(context).textTheme.subtitle2,
            ),
            Text(
              "Fecha: 24/11/2020",
              style: Theme.of(context).textTheme.headline3,
            ),
            Column(
              children: <Widget>[
                Text(
                  "Articulos: " + order.products.length.toString(),
                  style: Theme.of(context).textTheme.subtitle2,
                ),
                Container(height: MediaQuery.of(context).size.height * 0.005),
                Text(
                  "Total: " + order.totalPrice.toString() + " \$",
                  style: Theme.of(context).textTheme.subtitle2,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
