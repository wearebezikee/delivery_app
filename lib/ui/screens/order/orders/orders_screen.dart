import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/order/orders/components/order_item.dart';
import 'package:delivery_app/ui/screens/order/orders/orders_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrdersScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Ordenes",
      body: ChangeNotifierProvider<OrdersScreenViewModel>(
        create: (context) => _createViewModel()..loadOrders(),
        child: Consumer<OrdersScreenViewModel>(
          builder: (context, viewModel, staticChild) {
            if (viewModel.isLoading) {
              return MyProgressIndicator();
            } else {
              return ListView(
                children: _buildOrders(context, viewModel),
              );
            }
          },
        ),
      ),
    );
  }

  List<Widget> _buildOrders(
      BuildContext context, OrdersScreenViewModel viewModel) {
    List<Widget> output = List();
    for (Order order in viewModel.orders) {
      output.add(
        OrderItem(
          order: order,
          action: () => viewModel.viewOrderDetails(order),
        ),
      );
    }
    return output;
  }

  OrdersScreenViewModel _createViewModel() {
    return OrdersScreenViewModel();
  }
}
