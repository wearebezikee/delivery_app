import 'package:delivery_app/data_access/repositories/order/order_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/navigators/orders_navigator/orders_navigator.dart';
import 'package:delivery_app/ui/screens/order/order_detail/order_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class OrdersScreenViewModel extends ChangeNotifier {
  final OrderRepository _orderRepository = locator<OrderRepository>();
  final OrdersNavigator _navigator = locator<OrdersNavigator>();
  List<Order> _orders = List();
  bool _isloading = false;

  get orders => _orders;
  get isLoading => _isloading;

  void loadOrders() async {
    _isloading = true;
    notifyListeners();

    AppResponse<List<Order>> response = await _orderRepository.getAll();
    if (response.isSuccess()) {
      _orders = response.payload;
    } else {
      MyNotification.showError(
        subtitle: 'Ocurrió un error cargando las ordenes',
      );
    }

    _isloading = false;
    notifyListeners();
  }

  void viewOrderDetails(Order order) {
    _navigator.toOrderDetail(order);
  }
}
