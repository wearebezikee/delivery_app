import 'package:delivery_app/model/order_status.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/status_item.dart';
import 'package:flutter/material.dart';

class OrderStatusList extends StatelessWidget {
  final List<OrderStatus> orderStatuses;

  const OrderStatusList({this.orderStatuses});

  @override
  Widget build(BuildContext context) {
    return RoundedContainer(
      topLeft: true,
      topRight: true,
      bottomLeft: true,
      bottomRight: true,
      borderRadius: 5,
      withElevation: true,
      color: Colors.white,
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.1,
      ),
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.02,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "Estado de la orden",
              style: Theme.of(context).textTheme.headline2,
            ),
            Container(height: MediaQuery.of(context).size.height * 0.07),
            //for (Widget item in _buildStatusItems()) item,
            StatusItem(title: 'Pedido Creado', hour: '1:05 pm', isDone: true),
            StatusItem(
                title: 'Pedido en proceso', hour: '1:11pm', isDone: true),
            StatusItem(title: 'Pedido en camino'),
            StatusItem(title: 'Orden entregada', isLast: true),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildStatusItems() {
    List<Widget> output = List();
    for (int i = 0; i < orderStatuses.length; i++) {
      output.add(StatusItem(
        title: orderStatuses[i].title,
        hour: orderStatuses[i].date.hour.toString() +
            ':' +
            orderStatuses[i].date.minute.toString(),
        isDone: orderStatuses[i].status == OrderStatus.STATUS_SUCCESS,
        isLast: i == (orderStatuses.length - 1),
      ));
    }
  }
}
