import 'package:delivery_app/model/cart_item.dart';
import 'package:flutter/material.dart';

class OrderResumeProductRow extends StatelessWidget {
  final CartItem cartItem;

  const OrderResumeProductRow({@required this.cartItem});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: MediaQuery.of(context).size.height * 0.01),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                cartItem.product.title,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontWeight: FontWeight.normal),
              ),
              Container(width: MediaQuery.of(context).size.width * 0.01),
              Text(
                '(' + cartItem.quantity.toString() + ')',
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ],
          ),
          Text(
            (cartItem.product.price * cartItem.quantity).toString() + ' \$',
            style: Theme.of(context).textTheme.bodyText2,
          ),
        ],
      ),
    );
  }
}
