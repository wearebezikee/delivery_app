import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_resume_product_row.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_resume_header.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_resume_tile.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_resume_total_row.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class OrderResume extends StatefulWidget {
  final Order order;

  const OrderResume({this.order});
  @override
  _OrderResumeState createState() => _OrderResumeState();
}

class _OrderResumeState extends State<OrderResume> {
  bool showResume;

  @override
  void initState() {
    super.initState();
    showResume = false;
  }

  @override
  Widget build(BuildContext context) {
    return RoundedContainer(
      topLeft: true,
      topRight: true,
      bottomLeft: true,
      bottomRight: true,
      borderRadius: 5,
      withElevation: true,
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.1,
        vertical: MediaQuery.of(context).size.height * 0.02,
      ),
      padding:
          EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.02),
      color: Colors.white,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InkWell(
              onTap: () {
                setState(() {
                  showResume = !showResume;
                });
              },
              child: OrderResumeHeader(showResume: showResume),
            ),
            showResume
                ? Divider(
                    height: 0,
                  )
                : Container(
                    height: MediaQuery.of(context).size.height * 0.01,
                  ),
            showResume
                ? Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * 0.05,
                      vertical: MediaQuery.of(context).size.width * 0.04,
                    ),
                    child: Column(
                      children: <Widget>[
                        for (Widget item in _buildOrderItems()) item,
                        Container(
                          height: MediaQuery.of(context).size.height * 0.03,
                        ),
                        OrderRsumeTotalRow(total: widget.order.totalPrice),
                      ],
                    ),
                  )
                : Container(),
            OrderResumeTile(
              iconData: FontAwesomeIcons.shoppingBag,
              title: "Tipo de entrega",
              rightTitle:
                  widget.order.pickup ? 'Delivery' : 'Entrega a domicilio',
            ),
            OrderResumeTile(
              iconData: FontAwesomeIcons.mapMarkerAlt,
              title: "Dirección de entrega",
              rightTitle: widget.order.address.address1,
            ),
            OrderResumeTile(
              iconData: FontAwesomeIcons.dollarSign,
              title: "Metodo de pago",
              rightTitle: widget.order.paymentMethod.type,
            ),
            OrderResumeTile(
              iconData: FontAwesomeIcons.info,
              title: "Descripcion adicional",
              rightTitle: widget.order.observation,
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _buildOrderItems() {
    List<Widget> output = List();
    for (CartItem item in widget.order.products) {
      output.add(OrderResumeProductRow(cartItem: item));
    }
    return output;
  }
}
