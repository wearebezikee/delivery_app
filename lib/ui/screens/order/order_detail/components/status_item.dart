import 'package:delivery_app/ui/screens/order/order_detail/components/order_status_drawables/status_circle.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_status_drawables/status_line.dart';
import 'package:flutter/material.dart';

class StatusItem extends StatelessWidget {
  final String title;
  //TODO should be date
  final String hour;
  final bool isLast;
  final bool isDone;

  const StatusItem({
    this.isLast = false,
    this.isDone = false,
    @required this.title,
    this.hour,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: !isLast
          ? MediaQuery.of(context).size.height * 0.15
          : MediaQuery.of(context).size.height * 0.05,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: (MediaQuery.of(context).size.width -
                    (MediaQuery.of(context).size.width * 0.15)) /
                4,
            alignment: Alignment.topCenter,
            child: Text(
              title,
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          Container(
            width: (MediaQuery.of(context).size.width -
                    (MediaQuery.of(context).size.width * 0.15)) /
                4,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.015),
                  child: CustomPaint(
                    painter: StatusCircle(context: context, isDone: isDone),
                  ),
                ),
                !isLast
                    ? Container(
                        margin: EdgeInsets.only(top: 15),
                        child: CustomPaint(
                          painter: StatusLine(context: context, isDone: isDone),
                        ),
                      )
                    : Container(),
              ],
            ),
          ),
          Container(
            width: (MediaQuery.of(context).size.width -
                    (MediaQuery.of(context).size.width * 0.15)) /
                4,
            alignment: Alignment.topRight,
            child: Text(
              hour ?? '',
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ],
      ),
    );
  }
}
