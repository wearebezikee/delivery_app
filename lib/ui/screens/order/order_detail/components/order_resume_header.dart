import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class OrderResumeHeader extends StatelessWidget {
  final bool showResume;
  const OrderResumeHeader({this.showResume});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.width * 0.02,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Resumen de la orden",
            style: Theme.of(context).textTheme.headline2,
          ),
          Icon(
            showResume ? FontAwesomeIcons.angleUp : FontAwesomeIcons.angleDown,
            size: MediaQuery.of(context).size.height * 0.05,
          ),
        ],
      ),
    );
  }
}
