import 'package:flutter/material.dart';

class OrderResumeTile extends StatelessWidget {
  final IconData iconData;
  final String title;
  final String rightTitle;

  const OrderResumeTile({
    @required this.iconData,
    @required this.title,
    this.rightTitle,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Column(
        children: <Widget>[
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(
                    vertical: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Icon(
                      iconData,
                      size: MediaQuery.of(context).size.width * 0.04,
                    ),
                    Container(width: MediaQuery.of(context).size.width * 0.02),
                    Text(
                      title,
                      style: Theme.of(context)
                          .textTheme
                          .headline5
                          .copyWith(fontWeight: FontWeight.normal),
                    ),
                  ],
                ),
              ),
              Flexible(
                child: Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.04),
                  child: Text(
                    rightTitle.length == 0 ? 'N/A' : rightTitle,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
