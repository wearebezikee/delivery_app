import 'package:flutter/material.dart';

class StatusCircle extends CustomPainter {
  Paint _paint;
  BuildContext context;
  final bool isDone;

  StatusCircle({@required this.context, this.isDone = false}) {
    _paint = Paint()
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    _paint.color = Colors.grey;
    canvas.drawCircle(Offset(0.0, 0.0), 15, _paint);

    _paint.color = Colors.white;
    canvas.drawCircle(Offset(0.0, 0.0), 14, _paint);

    _paint.color = isDone ? Theme.of(context).primaryColor : Colors.transparent;
    canvas.drawCircle(Offset(0.0, 0.0), 9.5, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
