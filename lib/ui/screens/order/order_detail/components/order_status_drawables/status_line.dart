import 'package:flutter/material.dart';

class StatusLine extends CustomPainter {
  Paint _paint;
  BuildContext context;
  final bool isDone;

  StatusLine({@required this.context, this.isDone = false}) {
    _paint = Paint()
      ..color = isDone ? Theme.of(context).primaryColor : Colors.grey
      ..strokeWidth = 3.0
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(Offset(0.0, 0.0),
        Offset(0.0, MediaQuery.of(context).size.height * 0.12), _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
