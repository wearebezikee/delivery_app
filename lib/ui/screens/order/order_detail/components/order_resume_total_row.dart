import 'package:flutter/material.dart';

class OrderRsumeTotalRow extends StatelessWidget {
  final double total;

  const OrderRsumeTotalRow({this.total});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'Total a pagar',
            style: Theme.of(context).textTheme.headline5,
          ),
          Text(total.toString() + ' \$'),
        ],
      ),
    );
  }
}
