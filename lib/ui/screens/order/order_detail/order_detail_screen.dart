import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_resume.dart';
import 'package:delivery_app/ui/screens/order/order_detail/components/order_status_list.dart';
import 'package:flutter/material.dart';

class OrderDetailScreen extends StatelessWidget {
  final Order order;

  const OrderDetailScreen({this.order});
  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: "Detalle de orden",
      body: ListView(
        children: <Widget>[
          OrderStatusList(orderStatuses: order.statusHistory),
          OrderResume(order: order),
        ],
      ),
    );
  }
}
