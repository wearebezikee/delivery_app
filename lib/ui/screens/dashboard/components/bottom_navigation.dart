import 'package:delivery_app/ui/screens/dashboard/components/nav_bar_dot.dart';
import 'package:delivery_app/ui/screens/dashboard/dashboard.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BottomNavigation extends StatelessWidget {
  BottomNavigation({this.currentTab, this.onSelectTab});

  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: [
        _buildItem(
            tabItem: TabItem.HOME,
            iconData: FontAwesomeIcons.home,
            context: context),
        _buildItem(
            tabItem: TabItem.MENU,
            iconData: FontAwesomeIcons.bookOpen,
            context: context),
        _buildItem(
            tabItem: TabItem.ORDERS,
            iconData: FontAwesomeIcons.archive,
            context: context),
        _buildItem(
            tabItem: TabItem.SETTINGS,
            iconData: FontAwesomeIcons.cog,
            context: context)
      ],
      onTap: (index) => onSelectTab(
        TabItem.values[index],
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {TabItem tabItem, IconData iconData, BuildContext context}) {
    return BottomNavigationBarItem(
      icon: Icon(
        iconData,
        color: _colorTabMatching(item: tabItem, context: context),
      ),
      title: NavBarDot(
        active: currentTab == tabItem,
      ),
    );
  }

  Color _colorTabMatching({TabItem item, BuildContext context}) {
    return currentTab == item ? Theme.of(context).primaryColor : Colors.grey;
  }
}
