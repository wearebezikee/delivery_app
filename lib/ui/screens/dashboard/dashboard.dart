import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/navigators/home_navigator/home_navigator.dart';
import 'package:delivery_app/navigators/home_navigator/home_routes.dart';
import 'package:delivery_app/navigators/menu_navigator/menu_navigator.dart';
import 'package:delivery_app/navigators/menu_navigator/menu_routes.dart';
import 'package:delivery_app/navigators/orders_navigator/orders_navigator.dart';
import 'package:delivery_app/navigators/orders_navigator/orders_routes.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_routes.dart';
import 'package:delivery_app/ui/screens/cart/cart_screen_view_model.dart';
import 'package:delivery_app/ui/screens/dashboard/components/bottom_navigation.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum TabItem { HOME, MENU, ORDERS, SETTINGS }

class Dashboard extends StatefulWidget {
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  TabItem _currentTab = TabItem.HOME;
  Map<TabItem, GlobalKey<NavigatorState>> _navigatorKeys = {
    TabItem.HOME: locator<HomeNavigator>().navigatorKey,
    TabItem.MENU: locator<MenuNavigator>().navigatorKey,
    TabItem.ORDERS: locator<OrdersNavigator>().navigatorKey,
    TabItem.SETTINGS: locator<SettingsNavigator>().navigatorKey
  };

  void _selectTab(TabItem tabItem) {
    if (tabItem == _currentTab) {
      _navigatorKeys[tabItem].currentState.popUntil((route) => route.isFirst);
    } else {
      setState(() => _currentTab = tabItem);
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          final isFirstRouteInCurrentTab =
              !await _navigatorKeys[_currentTab].currentState.maybePop();
          if (isFirstRouteInCurrentTab) {
            if (_currentTab != TabItem.HOME) {
              _selectTab(TabItem.HOME);
              return false;
            }
          }
          return isFirstRouteInCurrentTab;
        },
        child: ChangeNotifierProvider<CartScreenViewModel>(
          create: (context) => _createCartViewModel(),
          child: Scaffold(
            body: Stack(children: <Widget>[
              _buildOffstageNavigator(TabItem.HOME),
              _buildOffstageNavigator(TabItem.MENU),
              _buildOffstageNavigator(TabItem.ORDERS),
              _buildOffstageNavigator(TabItem.SETTINGS)
            ]),
            bottomNavigationBar: BottomNavigation(
              currentTab: _currentTab,
              onSelectTab: _selectTab,
            ),
          ),
        ));
  }

  Widget _buildOffstageNavigator(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.HOME:
        return Offstage(
          offstage: _currentTab != tabItem,
          child: Navigator(
            key: locator<HomeNavigator>().navigatorKey,
            initialRoute: HomeRoutes.home,
            onGenerateRoute: locator<HomeNavigator>().onGenerateRoute,
          ),
        );
        break;
      case TabItem.MENU:
        return Offstage(
          offstage: _currentTab != tabItem,
          child: Navigator(
            key: locator<MenuNavigator>().navigatorKey,
            initialRoute: MenuRoutes.home,
            onGenerateRoute: locator<MenuNavigator>().onGenerateRoute,
          ),
        );
        break;
      case TabItem.ORDERS:
        return Offstage(
          offstage: _currentTab != tabItem,
          child: Navigator(
            key: locator<OrdersNavigator>().navigatorKey,
            initialRoute: OrdersRoutes.home,
            onGenerateRoute: locator<OrdersNavigator>().onGenerateRoute,
          ),
        );
        break;
      case TabItem.SETTINGS:
        return Offstage(
          offstage: _currentTab != tabItem,
          child: Navigator(
            key: locator<SettingsNavigator>().navigatorKey,
            initialRoute: SettingsRoutes.home,
            onGenerateRoute: locator<SettingsNavigator>().onGenerateRoute,
          ),
        );
      default:
        return Offstage(
          offstage: _currentTab != tabItem,
          child: Navigator(
            key: locator<HomeNavigator>().navigatorKey,
            initialRoute: HomeRoutes.home,
            onGenerateRoute: locator<HomeNavigator>().onGenerateRoute,
          ),
        );
    }
  }

  CartScreenViewModel _createCartViewModel() {
    return CartScreenViewModel();
  }
}
