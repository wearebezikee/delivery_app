import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/ui/screens/menu/components/menu_category_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategorySection extends StatelessWidget {
  final List<Category> categories;

  CategorySection({@required this.categories}) : assert(categories != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[_buildSection(context)],
    );
  }

  Widget _buildSection(BuildContext context) {
    List<Widget> categoryItems = List<Widget>();
    categoryItems.add(Container(
      width: MediaQuery.of(context).size.width * 0.1,
    ));
    for (int i = 0; i < categories.length; i++) {
      categoryItems.add(MenuCategoryItem(
        category: categories[i],
      ));
      if (i != categories.length - 1) {
        categoryItems.add(
          Container(
            width: MediaQuery.of(context).size.width * 0.05,
          ),
        );
      }
    }
    categoryItems.add(Container(
      width: MediaQuery.of(context).size.width * 0.1,
    ));

    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.25,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: 2),
        scrollDirection: Axis.horizontal,
        children: categoryItems,
      ),
    );
  }
}
