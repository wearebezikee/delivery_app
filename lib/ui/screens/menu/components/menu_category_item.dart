import 'package:cached_network_image/cached_network_image.dart';
import 'package:delivery_app/model/category.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'menu_category_item_view_model.dart';

class MenuCategoryItem extends StatelessWidget {
  final Category category;

  MenuCategoryItem({this.category});

  MenuCategoryItemViewModel _createViewModel() {
    return MenuCategoryItemViewModel(category: category);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MenuCategoryItemViewModel>(
        create: (context) => _createViewModel(),
        child: Consumer<MenuCategoryItemViewModel>(
            builder: (context, viewModel, staticChild) {
          return GestureDetector(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 0.0), //(x,y)
                    blurRadius: 1.0,
                  ),
                ],
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.13,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(5),
                          topRight: Radius.circular(5)),
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(category.imageUrl),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.114,
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    alignment: Alignment.center,
                    child: Text(
                      category.name,
                      style: Theme.of(context).textTheme.headline5,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            onTap: () => viewModel.onTapped(context),
          );
        }));
  }
}
