import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/ui/components/product_item/product_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductSection extends StatelessWidget {
  final List<Product> products;

  ProductSection({@required this.products}) : assert(products != null);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 20,
        ),
        _buildSection(context),
      ],
    );
  }

  Widget _buildSection(BuildContext context) {
    List<Widget> productItems = List<Widget>();
    for (Product product in products) {
      productItems.add(ProductItem(
        product: product,
      ));
      productItems.add(Container(
        height: MediaQuery.of(context).size.height * 0.03,
      ));
    }
    return Column(
      children: productItems,
    );
  }
}
