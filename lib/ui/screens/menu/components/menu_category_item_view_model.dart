import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/ui/screens/category/category_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MenuCategoryItemViewModel extends ChangeNotifier {
  Category _category;

  MenuCategoryItemViewModel({@required Category category})
      : assert(category != null),
        _category = category;

  bool _loading = false;

  bool get loading => _loading;

  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  Future<void> onTapped(BuildContext context) async {
    _loading = true;
    notifyListeners();

    AppResponse<Category> response =
        await _categoryRepository.getById(_category.id);

    if (response.isSuccess()) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => CategoryScreen(category: response.payload)));
    }
    _loading = false;
    notifyListeners();
  }
}
