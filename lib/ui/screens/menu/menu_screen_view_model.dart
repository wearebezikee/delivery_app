import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:flutter/material.dart';

class MenuScreenViewModel extends ChangeNotifier {
  Category _rootCategory;

  Category get rootCategory => _rootCategory;

  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  bool _loading = false;

  bool get loading => _loading;

  Future<void> onViewStarted() async {
    _loading = true;
    notifyListeners();

    var response = await _categoryRepository.getById(Category.ROOT_ID);

    if (response.isSuccess()) {
      _rootCategory = response.payload;
    } else {
      //TODO handle error
    }

    _loading = false;
    notifyListeners();
  }
}
