import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/menu/menu_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'components/category_section.dart';
import 'components/product_section.dart';

class MenuScreen extends StatelessWidget {
  MenuScreenViewModel createViewModel() {
    return MenuScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return SmallSliverAppBarScreen(
      title: 'Menú',
      withCart: true,
      body: ChangeNotifierProvider<MenuScreenViewModel>(
        create: (context) => createViewModel()..onViewStarted(),
        child: Consumer<MenuScreenViewModel>(
            builder: (context, viewModel, staticChild) {
          if (viewModel.loading) {
            return Container(
              child: MyProgressIndicator(),
            );
          } else {
            return ListView(
              children: <Widget>[
                _buildCategorySection(context, viewModel),
                _buildProductSection(context, viewModel),
              ],
            );
          }
        }),
      ),
    );
  }

  Widget _buildCategorySection(
      BuildContext context, MenuScreenViewModel viewModel) {
    return viewModel.rootCategory.categories.length > 0
        ? CategorySection(categories: viewModel.rootCategory.categories)
        : Container();
  }

  Widget _buildProductSection(
      BuildContext context, MenuScreenViewModel viewModel) {
    return viewModel.rootCategory.categories.length > 0
        ? Container(
            width: MediaQuery.of(context).size.width,
            child: ProductSection(products: viewModel.rootCategory.products))
        : Container();
  }
}
