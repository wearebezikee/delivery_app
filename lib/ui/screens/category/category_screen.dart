import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/ui/custom_screens/big_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/screens/category/components/header_component.dart';
import 'package:delivery_app/ui/screens/menu/components/product_section.dart';
import 'package:flutter/material.dart';

class CategoryScreen extends StatelessWidget {
  final Category category;

  CategoryScreen({this.category});

  @override
  Widget build(BuildContext context) {
    return BigSliverAppBarScreen(
      withCart: true,
      title: category.name,
      header: HeaderComponent(category: category),
      body: ListView(
        children: <Widget>[
          _buildProductsSection(context),
        ],
      ),
    );
  }

  Widget _buildProductsSection(BuildContext context) {
    return category.products.length > 0
        ? Container(
            width: MediaQuery.of(context).size.width,
            child: ProductSection(products: category.products))
        : Container();
  }
}
