import 'package:cached_network_image/cached_network_image.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/ui/screens/category/components/header_component_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HeaderComponent extends StatelessWidget {
  final Category category;

  const HeaderComponent({this.category});
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      bool showTitle = false;
      if (constraints.biggest.height.toInt() ==
          (MediaQuery.of(context).padding.top + kToolbarHeight).toInt()) {
        print(constraints.biggest.height.toString() +
            ' , ' +
            (MediaQuery.of(context).padding.top + kToolbarHeight).toString());
        showTitle = true;
      }

      return FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          showTitle ? category.name : '',
          style: Theme.of(context).textTheme.bodyText1,
        ),
        background: ChangeNotifierProvider<HeaderComponentViewModel>(
          create: (context) => _createViewModel(),
          child: Consumer<HeaderComponentViewModel>(
            builder: (context, viewModel, child) {
              return Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(
                        top: kToolbarHeight * 1.8,
                        bottom: MediaQuery.of(context).size.height * 0.02),
                    child: Text(
                      category.name,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.25,
                    width: MediaQuery.of(context).size.width * 0.4,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: CachedNetworkImageProvider(category.imageUrl),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.1,
                        right: MediaQuery.of(context).size.width * 0.1),
                    height: 40,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: _buildSubCategories(context, viewModel),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      );
    });
  }

  List<Widget> _buildSubCategories(
      BuildContext context, HeaderComponentViewModel viewModel) {
    List<Widget> output = List();
    for (Category category in category.categories) {
      output.add(GestureDetector(
        onTap: () => viewModel.onCategoryTap(context, category.id),
        child: Container(
          child: Text(
            category.name,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
      ));
      output.add(Container(
        width: MediaQuery.of(context).size.width * 0.1,
      ));
    }
    return output;
  }

  HeaderComponentViewModel _createViewModel() {
    return HeaderComponentViewModel();
  }
}
