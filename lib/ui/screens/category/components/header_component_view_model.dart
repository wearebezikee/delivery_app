import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/ui/screens/category/category_screen.dart';
import 'package:flutter/material.dart';

class HeaderComponentViewModel extends ChangeNotifier {
  CategoryRepository _categoryRepository = locator<CategoryRepository>();

  Future<void> onCategoryTap(BuildContext context, String categoryId) async {
    AppResponse<Category> categoryResponse =
        await _categoryRepository.getById(categoryId);
    if (categoryResponse.isSuccess()) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) =>
              CategoryScreen(category: categoryResponse.payload)));
    }
  }
}
