import 'dart:io';

import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/get_current_user_process_repository.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/regex_validator.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/app_navigator/app_navigator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class CreateUserFormScreenViewModel extends ChangeNotifier {
  String _id;

  CreateUserFormScreenViewModel({@required String id})
      : assert(id != null),
        _id = id;

  AppNavigator _appNavigator = locator<AppNavigator>();
  UserRepository _userRepository = locator<UserRepository>();
  GetCurrentUserProcessRepository _currentUserProcessRepository =
      locator<GetCurrentUserProcessRepository>();
  ImageResourceRepository _imageResourceRepository = locator<ImageResourceRepository>();

  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _nameController = TextEditingController();

  TextEditingController get phoneNumberController => _phoneNumberController;

  TextEditingController get nameController => _nameController;

  File _selectedImage;

  File get selectedImage => _selectedImage;

  String _imageUrl = "";
  String _email = "";

  String get imageUrl => _imageUrl;

  bool _imgUrlLoading = false;

  bool get imgUrlLoading => _imgUrlLoading;

  onViewCreated() async {
    _imgUrlLoading = true;
    notifyListeners();

    AppResponse<User> userResponse = await _currentUserProcessRepository.process();
    if (userResponse.isSuccess()) {
      _email = userResponse.payload.email;
      _imageUrl = userResponse.payload.imageUrl ?? "";
      _phoneNumberController.text = userResponse.payload.phoneNumber;
      _nameController.text = userResponse.payload.name;
    } else {
      MyNotification.showError(subtitle: userResponse.error.toString());
    }
    _imgUrlLoading = false;
    notifyListeners();
  }

  Future<void> onSubmitTapped() async {
    if (_validateForm()) {
      if (_selectedImage != null) {
        AppResponse<String> responseImageUrl =
            await _imageResourceRepository.uploadProfileImage(_selectedImage, _id);
        if (responseImageUrl.isSuccess()) {
          _imageUrl = responseImageUrl.payload;
        }
      }
      User toAdd = User(
          id: _id,
          role: User.ROLE_ADMIN,
          phoneNumber: _phoneNumberController.text,
          name: _nameController.text,
          imageUrl: _imageUrl,
          email: _email,
          addresses: []);
      AppResponse response = await _userRepository.create(toAdd);
      if (response.isSuccess()) {
        LoggedUserHelper.setLoggedUser(toAdd);
        _appNavigator.toDashboard();
      } else {
        MyNotification.showError(subtitle: response.error.toString());
      }
    }
  }

  bool _validateForm() {
    try {
      if (!RegexValidator.isPhoneNumber(_phoneNumberController.text)) {
        throw FormFieldValidationException.notAPhoneNumber();
      }
      if (_nameController.text.length < 3) {
        throw FormFieldValidationException.tooShortName();
      }
      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }

  Future<void> pickImage() async {
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    _selectedImage = File(pickedFile.path);
    notifyListeners();
  }
}
