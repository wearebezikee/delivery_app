import 'package:delivery_app/domain_logic/process_repository/authentication/email_sign_in_authentication_process_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/email_sign_up_authentication_process_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/google_authentication_process_repository.dart';
import 'package:delivery_app/domain_logic/services/authentication/authentication_service.dart';
import 'package:delivery_app/exceptions/form_field_validation_exception.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/regex_validator.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:flutter/material.dart';

class AuthenticationScreenViewModel extends ChangeNotifier {
  bool _isSignIn;

  AuthenticationScreenViewModel({@required bool isSignIn})
      : assert(isSignIn != null),
        _isSignIn = isSignIn;

  bool get isSignIn => _isSignIn;

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  TextEditingController get emailController => _emailController;

  TextEditingController get passwordController => _passwordController;

  TextEditingController get confirmPasswordController => _confirmPasswordController;

  AuthenticationService _emailAuthenticationService = locator<AuthenticationService>();
  EmailSignInAuthenticationProcessRepository _emailSignInAuthenticationProcessRepository =
      locator<EmailSignInAuthenticationProcessRepository>();
  EmailSignUpAuthenticationProcessRepository _emailSignUpAuthenticationProcessRepository =
      locator<EmailSignUpAuthenticationProcessRepository>();
  GoogleAuthenticationProcessRepository _googleAuthenticationProcessRepository =
      locator<GoogleAuthenticationProcessRepository>();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  void onToggleIsSignIn() {
    _isSignIn = !_isSignIn;
    notifyListeners();
  }

  Future<void> onEmailSignInTapped() async {
    if (validateSignIn()) {
      await _emailAuthenticationService.process(
          authenticationProcessRepository: _emailSignInAuthenticationProcessRepository,
          email: _emailController.text,
          password: _passwordController.text);
    }
  }

  Future<void> onEmailSignUpTapped() async {
    if (validateSignUp()) {
      await _emailAuthenticationService.process(
          authenticationProcessRepository: _emailSignUpAuthenticationProcessRepository,
          email: _emailController.text,
          password: _passwordController.text);
    }
  }

  Future<void> onGoogleTapped() async {
    await _emailAuthenticationService.process(
        authenticationProcessRepository: _googleAuthenticationProcessRepository);
  }

  bool validateSignIn() {
    try {
      if (!RegexValidator.isEmail(_emailController.text)) {
        throw FormFieldValidationException.notAnEmail();
      }
      if (_passwordController.text.length < 6) {
        throw FormFieldValidationException.tooShortPassword();
      }
      return true;
    } catch (e) {
      MyNotification.showError(subtitle: e.toString());
      return false;
    }
  }

  bool validateSignUp() {
    if (validateSignIn()) {
      try {
        if (_passwordController.text != _confirmPasswordController.text) {
          throw FormFieldValidationException.passwordMismatch();
        }
        return true;
      } catch (e) {
        MyNotification.showError(subtitle: e.toString());
        return false;
      }
    }
  }
}
