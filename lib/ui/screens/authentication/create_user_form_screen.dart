import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/images/user_profile_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/authentication/create_user_form_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class CreateUserFormScreen extends StatelessWidget {
  String id;

  CreateUserFormScreen(this.id);

  CreateUserFormScreenViewModel createViewModel() {
    return CreateUserFormScreenViewModel(id: id);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<CreateUserFormScreenViewModel>(
        create: (context) => createViewModel()..onViewCreated(),
        child: Consumer<CreateUserFormScreenViewModel>(
            builder: (context, viewModel, staticChild) {
          return Scaffold(
            body: ListView(
              shrinkWrap: true,
              children: [
                RoundedContainer(
                  padding: EdgeInsets.only(
                    left: horizontalMargin * 2,
                    right: horizontalMargin * 2,
                    top: MediaQuery.of(context).size.height * 0.15,
                  ),
                  borderRadius: 30,
                  bottomLeft: true,
                  bottomRight: true,
                  withElevation: true,
                  color: Theme.of(context).canvasColor,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _buildProfileImage(context, viewModel),
                      _buildFormFields(context, viewModel),
                      Container(
                        margin: EdgeInsets.symmetric(
                            vertical:
                                MediaQuery.of(context).size.height * 0.12),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: PrimaryButtonText(
                                text: "Crear cuenta",
                                action: viewModel.onSubmitTapped,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        }));
  }

  _buildFormFields(
      BuildContext context, CreateUserFormScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        FormFieldContainer(
          margin: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.03),
          child: FormFieldText(
            textController: viewModel.nameController,
            hint: "Nombre",
            preffix: Icon(FontAwesomeIcons.tag, size: 20),
          ),
          withDivider: true,
        ),
        FormFieldContainer(
          child: FormFieldText(
            textController: viewModel.phoneNumberController,
            hint: "Número de contacto",
            preffix: Icon(FontAwesomeIcons.phoneAlt, size: 20),
          ),
          withDivider: true,
        ),
      ],
    );
  }

  Widget _buildProfileImage(
      BuildContext context, CreateUserFormScreenViewModel viewModel) {
    return InkWell(
      onTap: viewModel.pickImage,
      child: Container(
          width: MediaQuery.of(context).size.height * 0.14,
          height: MediaQuery.of(context).size.height * 0.14,
          margin: const EdgeInsets.only(bottom: 25),
          child: viewModel.imgUrlLoading
              ? MyProgressIndicator()
              : viewModel.selectedImage == null
                  ? UserProfileImage(viewModel.imageUrl)
                  : UserProfileImage.fromFileImage(viewModel.selectedImage)),
    );
  }
}
