import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/ui/components/buttons/primary_button.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/form/form_field_container.dart';
import 'package:delivery_app/ui/components/form/form_field_text.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/authentication/authentication_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class AuthenticationScreen extends StatelessWidget {
  final bool isSignIn;

  AuthenticationScreen({bool isSignIn}) : isSignIn = isSignIn ?? true;

  AuthenticationScreenViewModel createViewModel() {
    return AuthenticationScreenViewModel(isSignIn: isSignIn);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AuthenticationScreenViewModel>(
        create: (context) => createViewModel(),
        child: Consumer<AuthenticationScreenViewModel>(
            builder: (context, viewModel, staticChild) {
          return Scaffold(
            body: ListView(
              shrinkWrap: true,
              children: [
                RoundedContainer(
                  padding: EdgeInsets.only(
                    left: horizontalMargin * 2,
                    right: horizontalMargin * 2,
                    top: MediaQuery.of(context).size.height * 0.15,
                  ),
                  borderRadius: 30,
                  bottomLeft: true,
                  bottomRight: true,
                  withElevation: true,
                  color: Theme.of(context).canvasColor,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Center(child: _buildHeader(context, viewModel)),
                      Center(child: _buildFields(context, viewModel)),
                      Center(child: _buildAuthButtons(context, viewModel)),
                      Container(
                          height: MediaQuery.of(context).size.height * 0.05)
                    ],
                  ),
                ),
                Center(child: _buildViewTypeButton(viewModel, context)),
              ],
            ),
          );
        }));
  }

  Widget _buildHeader(
      BuildContext context, AuthenticationScreenViewModel viewModel) {
    return Container(
      margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.1),
      child: Text(viewModel.isSignIn ? 'Inicia sesión' : 'Crear una cuenta',
          style: Theme.of(context).textTheme.headline6),
    );
  }

  Widget _buildFields(
      BuildContext context, AuthenticationScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        FormFieldContainer(
          margin: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.03),
          child: FormFieldText(
            textController: viewModel.emailController,
            hint: "Correo electrónico",
            preffix: Icon(
              FontAwesomeIcons.envelope,
              size: 20,
            ),
          ),
          withDivider: true,
        ),
        FormFieldContainer(
          margin: EdgeInsets.only(
              bottom: !viewModel.isSignIn
                  ? MediaQuery.of(context).size.height * 0.03
                  : 0),
          child: FormFieldText(
            obscureText: true,
            textController: viewModel.passwordController,
            hint: "Contraseña",
            preffix: Icon(
              FontAwesomeIcons.key,
              size: 20,
            ),
          ),
          withDivider: true,
        ),
        !viewModel.isSignIn
            ? FormFieldContainer(
                child: FormFieldText(
                  obscureText: true,
                  textController: viewModel.confirmPasswordController,
                  hint: "Confirmar contraseña",
                  preffix: Icon(
                    FontAwesomeIcons.key,
                    size: 20,
                  ),
                ),
                withDivider: true,
              )
            : Container(
                height: MediaQuery.of(context).size.height * 0.1,
                child: Center(
                    child: Text(
                  'Olvidaste tu contraseña?',
                  style: Theme.of(context).textTheme.headline3,
                )),
              ),
      ],
    );
  }

  Widget _buildAuthButtons(
      BuildContext context, AuthenticationScreenViewModel viewModel) {
    return Column(
      children: <Widget>[
        viewModel.isSignIn
            ? Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.025),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: PrimaryButtonText(
                        text: "Inicia Sesión",
                        action: viewModel.onEmailSignInTapped,
                      ),
                    ),
                  ],
                ),
              )
            : Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.025),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: PrimaryButtonText(
                        text: "Siguiente",
                        action: viewModel.onEmailSignUpTapped,
                      ),
                    ),
                  ],
                ),
              ),
        Container(
          margin:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.025),
          child: Row(
            children: <Widget>[
              Expanded(
                child: PrimaryButton(
                  color: Colors.blueGrey.withOpacity(0.6),
                  child: Icon(
                    FontAwesomeIcons.google,
                    color: Colors.white,
                  ),
                  action: viewModel.onGoogleTapped,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildViewTypeButton(
      AuthenticationScreenViewModel viewModel, BuildContext context) {
    return InkWell(
      onTap: viewModel.onToggleIsSignIn,
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: Text(
          viewModel.isSignIn ? "Crear una cuenta" : "Ya tienes una cuenta?",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline3,
        ),
      ),
    );
  }
}
