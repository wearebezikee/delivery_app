import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/ui/app_bar/transparent_app_bar.dart';
import 'package:delivery_app/ui/components/buttons/primary_button_text.dart';
import 'package:delivery_app/ui/components/images/rounded_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:delivery_app/ui/screens/cart/cart_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductScreen extends StatelessWidget {
  final Product product;

  const ProductScreen({this.product});

  @override
  Widget build(BuildContext context) {
    return Consumer<CartScreenViewModel>(
        builder: (context, viewModel, staticChild) {
      return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        extendBodyBehindAppBar: true,
        appBar: TransparentAppBar(
          withCart: true,
        ),
        body: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            Stack(
              children: [
                RoundedContainer(
                  padding: EdgeInsets.only(bottom: 15),
                  color: Colors.white,
                  withElevation: true,
                  bottomLeft: true,
                  bottomRight: true,
                  child: _buildpPorductInformation(context),
                  borderRadius: 20,
                ),
                RoundedImage(
                  withGradient: true,
                  withShadow: true,
                  bottomLeft: true,
                  bottomRight: true,
                  borderRadius: 20,
                  height: MediaQuery.of(context).size.height * 0.5,
                  imageUrl: product.imageUrl,
                ),
              ],
            ),
            Container(
              height: MediaQuery.of(context).size.height * 0.038,
            ),
            _buildAddButton(viewModel),
            Container(
              height: MediaQuery.of(context).size.height * 0.038,
            ),
          ],
        ),
      );
    });
  }

  Widget _buildpPorductInformation(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.5,
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Text(
                product.title,
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 5),
              child: Text(
                product.price.toString(),
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: MediaQuery.of(context).size.width,
              child: Text(
                product.description,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline3,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _buildAddButton(CartScreenViewModel viewModel) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 200,
          child: PrimaryButtonText(
            action: () => viewModel.addProduct(product),
            text: 'Agregar',
          ),
        ),
      ],
    );
  }
}
