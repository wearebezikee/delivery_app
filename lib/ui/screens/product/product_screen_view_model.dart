import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/product.dart';
import 'package:flutter/foundation.dart';

class ProductScreenViewModel extends ChangeNotifier {
  List<CartItem> _items = locator<List<CartItem>>();

  addProduct(Product product) {
    for (CartItem item in _items) {
      if (item.product.title == product.title) {
        item.quantity++;
        notifyListeners();
        return;
      }
    }
    _items.add(CartItem(
      product: product,
      quantity: 1,
    ));
    notifyListeners();
  }
}
