import 'package:delivery_app/domain_logic/process_repository/authentication/sign_out_current_user_process_repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/misc/logged_user_helper.dart';
import 'package:delivery_app/misc/my_notification.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/app_navigator/app_navigator.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:flutter/material.dart';

class SettingsScreenViewModel extends ChangeNotifier {
  bool get isUserLogged => LoggedUserHelper.isUserLogged();

  User get loggedUser => LoggedUserHelper.getLoggedUser();

  AppNavigator _appNavigator = locator<AppNavigator>();
  SettingsNavigator _settingsNavigator = locator<SettingsNavigator>();
  SignOutCurrentUserProcessRepository _signOutCurrentUserProcessRepository =
      locator<SignOutCurrentUserProcessRepository>();

  void onSignInTapped() {
    _appNavigator.toAuthentication();
  }

  Future<void> onSignOutTapped() async {
    AppResponse response = await _signOutCurrentUserProcessRepository.process();
    if (response.isSuccess()) {
      locator<AppNavigator>().popScreen();
      locator<AppNavigator>().toDashboard();
    } else {
      MyNotification.showError(subtitle: response.error.toString());
    }
  }

  void onManageInventoryTapped() {
    _settingsNavigator.toManageInventory();
  }

  void onManageTimelineTapped() {
    _settingsNavigator.toManageTimeline();
  }

  void onManageScheduleTapped() {
    _settingsNavigator.toManageSchedule();
  }

  void onManageAddressesTapped() {
    _settingsNavigator.toManageAddresses();
  }

  void onManagePaymentMethodSettingsTapped() {
    _settingsNavigator.toManagePaymentMethodSettings();
  }

  void onManageOrdersTapped() {
    _settingsNavigator.toManageOrders();
  }
}
