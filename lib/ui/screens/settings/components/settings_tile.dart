import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:flutter/material.dart';

class SettingsTile extends StatelessWidget {
  final String title;
  final IconData iconData;
  final Function action;

  const SettingsTile({this.title, this.iconData, this.action});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: RoundedContainer(
        withElevation: true,
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.1, vertical: 8),
        height: MediaQuery.of(context).size.height * 0.08,
        color: Colors.white,
        topLeft: true,
        topRight: true,
        bottomLeft: true,
        bottomRight: true,
        borderRadius: 5,
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05),
              child: Icon(iconData),
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.headline2,
            ),
          ],
        ),
      ),
      onTap: action != null ? () => action() : () {},
    );

    /* Card(
      elevation: 10,
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 8),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(20)),
      ),
      child: ListTile(
        leading: Icon(
          iconData,
        ),
        title: Text(
          title,
          style: Theme.of(context).textTheme.headline2,
        ),
        onTap: action != null ? () => action() : () {},
      ),
    ); */
  }
}
