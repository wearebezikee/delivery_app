import 'package:delivery_app/ui/components/images/user_profile_image.dart';
import 'package:delivery_app/ui/components/rounded_container.dart';
import 'package:flutter/material.dart';

class ProfileTile extends StatelessWidget {
  final String title;
  final String imageUrl;
  final Function action;

  const ProfileTile({this.title, this.imageUrl, this.action});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: RoundedContainer(
        withElevation: true,
        margin: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.05, vertical: 8),
        height: MediaQuery.of(context).size.height * 0.125,
        color: Colors.white,
        topLeft: true,
        topRight: true,
        bottomLeft: true,
        bottomRight: true,
        borderRadius: 5,
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.05),
              child: UserProfileImage(imageUrl),
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.headline2,
            ),
          ],
        ),
      ),
      onTap: action != null ? () => action() : () {},
    );
  }
}
