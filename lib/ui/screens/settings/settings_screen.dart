import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/ui/custom_screens/small_sliver_app_bar_screen.dart';
import 'package:delivery_app/ui/screens/settings/components/settings_tile.dart';
import 'package:delivery_app/ui/screens/settings/settings_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import 'components/profile_tile.dart';

class SettingsScreen extends StatelessWidget {
  SettingsScreenViewModel createViewModel() {
    return SettingsScreenViewModel();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<SettingsScreenViewModel>(
        create: (context) => createViewModel(),
        child: Consumer<SettingsScreenViewModel>(
            builder: (context, viewModel, staticChild) {
          return SmallSliverAppBarScreen(
            body: ListView(
              children: <Widget>[
                _buildProfileTile(viewModel),
                _buildUserTiles(viewModel),
                _buildAdminTiles(viewModel),
                _buildAuthTile(viewModel),
              ],
            ),
            title: "Ajustes",
          );
          /* Scaffold(
            backgroundColor: Theme.of(context).primaryColor.withOpacity(0.5),
            appBar: DeliveryAppBar(
              title: "Settings",
            ),
            body: ListView(children: <Widget>[
              _buildProfileTile(viewModel),
              _buildUserTiles(viewModel),
              _buildAdminTiles(viewModel),
              _buildAuthTile(viewModel),
            ]),
          ); */
        }));
  }

  Widget _buildProfileTile(SettingsScreenViewModel viewModel) {
    return viewModel.isUserLogged
        ? ProfileTile(
            title: viewModel.loggedUser.name,
            imageUrl: viewModel.loggedUser.imageUrl,
          )
        : Container();
  }

  Widget _buildAuthTile(SettingsScreenViewModel viewModel) {
    return viewModel.isUserLogged
        ? Container(
            margin: EdgeInsets.only(top: 20),
            child: SettingsTile(
              title: 'Cerrar sesión',
              iconData: FontAwesomeIcons.signOutAlt,
              action: viewModel.onSignOutTapped,
            ),
          )
        : SettingsTile(
            title: 'Iniciar sesión',
            iconData: FontAwesomeIcons.signInAlt,
            action: viewModel.onSignInTapped,
          );
  }

  Widget _buildUserTiles(SettingsScreenViewModel viewModel) {
    return viewModel.isUserLogged
        ? Column(
            children: <Widget>[
              SettingsTile(
                title: 'Administrar direcciones',
                iconData: FontAwesomeIcons.map,
                action: viewModel.onManageAddressesTapped,
              ),
            ],
          )
        : Container();
  }

  Widget _buildAdminTiles(SettingsScreenViewModel viewModel) {
    return viewModel.isUserLogged &&
            viewModel.loggedUser.role == User.ROLE_ADMIN
        ? Column(
            children: <Widget>[
              SettingsTile(
                title: 'Administrar inventario',
                iconData: FontAwesomeIcons.box,
                action: viewModel.onManageInventoryTapped,
              ),
              SettingsTile(
                title: 'Administrar métodos de pago',
                iconData: FontAwesomeIcons.fileInvoice,
                action: viewModel.onManagePaymentMethodSettingsTapped,
              ),
              SettingsTile(
                title: 'Administrar órdenes',
                iconData: FontAwesomeIcons.fileInvoice,
                action: viewModel.onManageOrdersTapped,
              ),
              SettingsTile(
                title: 'Administrar horario',
                iconData: FontAwesomeIcons.clock,
                action: viewModel.onManageScheduleTapped,
              ),
              SettingsTile(
                  title: 'Añadir publicación',
                  iconData: FontAwesomeIcons.newspaper,
                  action: viewModel.onManageTimelineTapped),
            ],
          )
        : Container();
  }
}
