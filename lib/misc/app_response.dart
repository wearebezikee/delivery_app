import 'package:delivery_app/constants/app_constants.dart';

class AppResponse<T> {
  String status;
  T payload;
  dynamic error;

  AppResponse.success(T payload) {
    this.status = AppConstants.SUCCESS;
    this.payload = payload;
    this.error = null;
  }

  AppResponse.failure(dynamic error) {
    print(error.toString());
    this.status = AppConstants.FAILURE;
    this.payload = null;
    this.error = error;
  }

  bool isSuccess() {
    return status == AppConstants.SUCCESS;
  }
}
