import 'dart:math';

class RandomGenerator {
  static Future<String> randomString(int strlen) async {
    const chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
    await Future.delayed(Duration(milliseconds: 1));
    String result = "";
    for (var i = 0; i < strlen; i++) {
      result += chars[rnd.nextInt(chars.length)];
    }
    return result;
  }

  static Future<int> randomInt(int max) async {
    Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
    await Future.delayed(Duration(milliseconds: 1));
    return rnd.nextInt(max);
  }
}
