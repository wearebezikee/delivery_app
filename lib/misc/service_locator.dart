import 'package:delivery_app/data_access/repositories/category/category_repository.dart';
import 'package:delivery_app/data_access/repositories/order/order_repository.dart';
import 'package:delivery_app/data_access/repositories/payment_method_setting/payment_method_setting_repository.dart';
import 'package:delivery_app/data_access/repositories/post/post_repository.dart';
import 'package:delivery_app/data_access/repositories/product/product_repository.dart';
import 'package:delivery_app/data_access/repositories/schedule/schedule_repository.dart';
import 'package:delivery_app/data_access/repositories/user/user_repository.dart';
import 'package:delivery_app/data_access/resource_storage_repository/image/image_resource_storage_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/email_sign_in_authentication_process_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/email_sign_up_authentication_process_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/get_current_user_process_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/google_authentication_process_repository.dart';
import 'package:delivery_app/domain_logic/process_repository/authentication/sign_out_current_user_process_repository.dart';
import 'package:delivery_app/domain_logic/services/authentication/authentication_service.dart';
import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/user.dart';
import 'package:delivery_app/navigators/app_navigator/app_navigator.dart';
import 'package:delivery_app/navigators/home_navigator/home_navigator.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_navigator.dart';
import 'package:delivery_app/navigators/menu_navigator/menu_navigator.dart';
import 'package:delivery_app/navigators/orders_navigator/orders_navigator.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_navigator.dart';
import 'package:get_it/get_it.dart';

final ServiceLocator locator = ServiceLocator._();

/// Convenience wrapper around GetIt.
class ServiceLocator<T> {
  ServiceLocator._();

  final GetIt _getIt = GetIt.instance;

  T call<T>() => _getIt<T>();

  Future<void> registerDependencies() async {
    //Navigators Start
    _getIt.registerSingleton(AppNavigator());
    _getIt.registerSingleton(OrdersNavigator());
    _getIt.registerSingleton(HomeNavigator());
    _getIt.registerSingleton(MenuNavigator());
    _getIt.registerSingleton(SettingsNavigator());
    _getIt.registerSingleton(InventoryCmsNavigator());
    //Navigators End

    //Repositories Begin
    _getIt.registerSingleton(CategoryRepository());
    _getIt.registerSingleton(ProductRepository());
    _getIt.registerSingleton(UserRepository());
    _getIt.registerSingleton(PostRepository());
    _getIt.registerSingleton(ScheduleRepository());
    _getIt.registerSingleton(PaymentMethodSettingRepository());
    _getIt.registerSingleton(OrderRepository());
    //Repositories End

    //Shopping Cart Start
    _getIt.registerSingleton(List<CartItem>());
    //Shopping Cart End
    //Auth Begin
    _getIt.registerSingleton(User.empty());
    //Auth End

    //ProcessRepositories Begin
    _getIt.registerSingleton(EmailSignInAuthenticationProcessRepository());
    _getIt.registerSingleton(EmailSignUpAuthenticationProcessRepository());
    _getIt.registerSingleton(GoogleAuthenticationProcessRepository());
    _getIt.registerSingleton(GetCurrentUserProcessRepository());
    _getIt.registerSingleton(SignOutCurrentUserProcessRepository());
    //ProcessRepositories End

    //StorageRepositories Begin
    _getIt.registerSingleton(ImageResourceRepository());
    //StorageRepositories End

    //Services Begin
    _getIt.registerSingleton(AuthenticationService());
    //Services End

    //Other Begin
    //Other End
  }
}
