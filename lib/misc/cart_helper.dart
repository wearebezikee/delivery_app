import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/model/cart_item.dart';

class CartHelper {
  static void addItem(CartItem item) {
    locator<List<CartItem>>().add(item);
  }

  static void removeItem(CartItem item) {
    locator<List<CartItem>>().remove(item);
  }

  static void clearCart() {
    locator<List<CartItem>>().clear();
  }

  static List<CartItem> getCart() {
    return locator<List<CartItem>>();
  }
}
