import 'package:bot_toast/bot_toast.dart';
import 'package:delivery_app/misc/service_locator.dart';
import 'package:delivery_app/misc/theme.dart';
import 'package:delivery_app/navigators/app_navigator/app_navigator.dart';
import 'package:delivery_app/ui/screens/splashscreen/splashscreen.dart';
import 'package:flutter/material.dart';

class App extends StatefulWidget {
  _AppState createState() => _AppState();
}

final GlobalKey<NavigatorState> keyGlobal = GlobalKey();

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return BotToastInit(
        child: MaterialApp(
      navigatorObservers: [BotToastNavigatorObserver()],
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      theme: AppTheme.theme,
      navigatorKey: locator<AppNavigator>().navigatorKey,
      onGenerateRoute: locator<AppNavigator>().onGenerateRoute,
      home: SplashScreen(),
    ));
  }
}
