import 'dart:io';

import 'package:delivery_app/data_access/resource_storage/image/image_firebase_resource__storage.dart';
import 'package:delivery_app/misc/app_response.dart';

class ImageResourceRepository {
  ImageFirebaseResourceStorage _resourceStorage =
      ImageFirebaseResourceStorage();

  Future<AppResponse<String>> uploadProfileImage(
      File image, String imageName) async {
    try {
      return AppResponse.success(
          await _resourceStorage.uploadProfileImage(image, imageName));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<String>> uploadCategoryImage(
      File image, String imageName) async {
    try {
      return AppResponse.success(
          await _resourceStorage.uploadCategoryImage(image, imageName));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<String>> uploadProductImage(
      File image, String imageName) async {
    try {
      return AppResponse.success(
          await _resourceStorage.uploadProductImage(image, imageName));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<String>> uploadPostImage(
      File image, String imageName) async {
    try {
      return AppResponse.success(
          await _resourceStorage.uploadPostImage(image, imageName));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<String>> uploadPaymentImage(
      File image, String imageName) async {
    try {
      return AppResponse.success(
          await _resourceStorage.uploadPaymentImage(image, imageName));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
