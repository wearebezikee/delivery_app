import 'dart:io';

import 'package:delivery_app/data_access/resource_storage/image/i_image_resource_storage.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart' as Path;

class ImageFirebaseResourceStorage extends IImageResourceStorage {
  static const String PROFILE_PATH = 'images/profile/';
  static const String CATEGORY_PATH = 'images/categories/';
  static const String PRODUCT_PATH = 'images/products/';
  static const String POST_PATH = 'images/posts/';
  static const String PAYMENT_CONFIRMATION_PATH =
      'images/paymentConfirmations/';

  Future<String> uploadProfileImage(File image, String imageName) async {
    return await uploadImage(image, imageName, PROFILE_PATH);
  }

  Future<String> uploadCategoryImage(File image, String imageName) async {
    return await uploadImage(image, imageName, CATEGORY_PATH);
  }

  Future<String> uploadProductImage(File image, String imageName) async {
    return await uploadImage(image, imageName, PRODUCT_PATH);
  }

  Future<String> uploadPostImage(File image, String imageName) async {
    return await uploadImage(image, imageName, POST_PATH);
  }

  Future<String> uploadPaymentImage(File image, String imageName) async {
    return await uploadImage(image, imageName, PAYMENT_CONFIRMATION_PATH);
  }

  Future<String> uploadImage(File image, String imageName, String path) async {
    var result = await compressImage(image);

    StorageReference ref =
        instance.ref().child(path + Path.basename(imageName));
    StorageUploadTask uploadTask = ref.putFile(result);
    await uploadTask.onComplete;
    var downloadUrl = await ref.getDownloadURL();
    return downloadUrl;
  }
}
