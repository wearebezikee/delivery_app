import 'dart:io';

import 'package:delivery_app/data_access/resource_storage/firebase_resource_storage.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

abstract class IImageResourceStorage extends FirebaseResourceStorage {
  Future<String> uploadProfileImage(File image, String imageName);

  Future<File> compressImage(File image) async {
    int minWidth = 2300;
    int minHeight = 1500;
    int quality = 70;

    String resultPath =
        image.absolute.path.substring(0, image.absolute.path.length - 4) + "-compressed.jpg";

    return await FlutterImageCompress.compressAndGetFile(
      image.absolute.path,
      resultPath,
      minWidth: minWidth,
      minHeight: minHeight,
      quality: quality,
    );
  }
}
