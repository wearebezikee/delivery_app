import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/category.dart';

class CategoryRepository extends Repository<Category> {
  CategoryFirebaseDao dao = CategoryFirebaseDao(Firestore.instance);

  Future<AppResponse<void>> create(Category entity) async {
    try {
      return AppResponse.success(await dao.create(entity));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> delete(String uid) async {
    try {
      return AppResponse.success(await dao.delete(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<Category>> getById(String uid) async {
    try {
      return AppResponse.success(await dao.getById(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<Category>> getByProductId(String uid) async {
    try {
      return AppResponse.success(await dao.getByProductId(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> update(Category entity) async {
    try {
      return AppResponse.success(await dao.update(entity));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<Category>> getFullTreeFrom(String uid) async {
    try {
      return AppResponse.success(await dao.getFullTreeFrom(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
