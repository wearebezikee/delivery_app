import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/schedule/schedule_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/schedule.dart';

class ScheduleRepository extends Repository<Schedule> {
  ScheduleFirebaseDao dao = ScheduleFirebaseDao(Firestore.instance);

  Future<AppResponse<Schedule>> create(Schedule schedule) async {
    try {
      return AppResponse.success(await dao.create(schedule));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> update(Schedule schedule) async {
    try {
      return AppResponse.success(await dao.update(schedule));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> delete(String id) async {
    try {
      return AppResponse.success(await dao.delete(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<Schedule>> getById(String id) async {
    try {
      return AppResponse.success(await dao.getById(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<List<Schedule>>> getAll() async {
    try {
      return AppResponse.success(await dao.getAll());
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
