import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/order/order_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/order.dart';

class OrderRepository extends Repository<Order> {
  OrderFirebaseDao dao = OrderFirebaseDao(Firestore.instance);

  Future<AppResponse<Order>> create(Order post) async {
    try {
      return AppResponse.success(await dao.create(post));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> update(Order post) async {
    try {
      return AppResponse.success(await dao.update(post));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> delete(String id) async {
    try {
      return AppResponse.success(await dao.delete(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<Order>> getById(String id) async {
    try {
      return AppResponse.success(await dao.getById(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<List<Order>>> getAll() async {
    try {
      return AppResponse.success(await dao.getAll());
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
