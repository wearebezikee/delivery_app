import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/user/user_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/user.dart';

class UserRepository extends Repository<User> {
  UserFirebaseDao dao = UserFirebaseDao(Firestore.instance);

  Future<AppResponse<void>> create(User entity) async {
    try {
      return AppResponse.success(await dao.create(entity));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> delete(String uid) async {
    try {
      return AppResponse.success(await dao.delete(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<User>> getById(String uid) async {
    try {
      return AppResponse.success(await dao.getById(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> update(User entity) async {
    try {
      return AppResponse.success(await dao.update(entity));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<User>> getFullUser(String uid) async {
    try {
      return AppResponse.success(await dao.getFullUserById(uid));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
