import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/payment_method_setting/payment_method_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';

class PaymentMethodSettingRepository extends Repository<PaymentMethodSetting> {
  PaymentMethodSettingFirebaseDao dao =
      PaymentMethodSettingFirebaseDao(Firestore.instance);

  Future<AppResponse<PaymentMethodSetting>> create(
      PaymentMethodSetting post) async {
    try {
      return AppResponse.success(await dao.create(post));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> update(PaymentMethodSetting post) async {
    try {
      return AppResponse.success(await dao.update(post));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> delete(String id) async {
    try {
      return AppResponse.success(await dao.delete(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<PaymentMethodSetting>> getById(String id) async {
    try {
      return AppResponse.success(await dao.getById(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<List<PaymentMethodSetting>>> getAll() async {
    try {
      return AppResponse.success(await dao.getAll());
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
