import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/product/product_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/product.dart';

class ProductRepository extends Repository<Product> {
  ProductFirebaseDao dao = ProductFirebaseDao(Firestore.instance);

  @override
  Future<AppResponse<void>> create(Product entity) async {
    try {
      return AppResponse.success(await dao.create(entity));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  @override
  Future<AppResponse<void>> delete(String id) async {
    try {
      return AppResponse.success(await dao.delete(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  @override
  Future<AppResponse<Product>> getById(String id) async {
    try {
      return AppResponse.success(await dao.getById(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  @override
  Future<AppResponse<void>> update(Product entity) async {
    try {
      return AppResponse.success(await dao.update(entity));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
