import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/post/post_firebase_dao.dart';
import 'package:delivery_app/data_access/repositories/repository.dart';
import 'package:delivery_app/misc/app_response.dart';
import 'package:delivery_app/model/posts/post.dart';

class PostRepository extends Repository<Post> {
  PostFirebaseDao dao = PostFirebaseDao(Firestore.instance);

  Future<AppResponse<Post>> create(Post post) async {
    try {
      return AppResponse.success(await dao.create(post));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> update(Post post) async {
    try {
      return AppResponse.success(await dao.update(post));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<void>> delete(String id) async {
    try {
      return AppResponse.success(await dao.delete(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<Post>> getById(String id) async {
    try {
      return AppResponse.success(await dao.getById(id));
    } catch (error) {
      return AppResponse.failure(error);
    }
  }

  Future<AppResponse<List<Post>>> getAll() async {
    try {
      return AppResponse.success(await dao.getAll());
    } catch (error) {
      return AppResponse.failure(error);
    }
  }
}
