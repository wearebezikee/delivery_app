import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/posts/post.dart';

abstract class IPostDao extends Dao<Post> {
  Future<Post> create(Post post);

  Future<void> update(Post post);

  Future<Post> getById(String id);

  Future<void> delete(String id);

  Future<List<Post>> getAll();
}
