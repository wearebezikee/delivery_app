import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/data_access/daos/post/i_post_dao.dart';
import 'package:delivery_app/domain_logic/factories/post_factory.dart';
import 'package:delivery_app/model/posts/post.dart';

class PostFirebaseDao extends FirebaseDao<Post> implements IPostDao {
  PostFirebaseDao(Firestore firestore) : super(firestore);

  @override
  Future<Post> create(Post post) async {
    post.id = await createRandomId();
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_POST)
        .document(post.id)
        .setData(post.toJson());
    return post;
  }

  @override
  Future<void> update(Post post) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_POST)
        .document(post.id)
        .updateData(post.toJson());
  }

  @override
  Future<List<Post>> getAll() async {
    var posts = await firestore
        .collection(FirebaseDao.COLLECTION_ID_POST)
        .orderBy(Post.ATTRIBUTE_DATE, descending: true)
        .getDocuments();
    List<Post> output = List();
    for (int i = 0; i < posts.documents.length; i++) {
      output.add(PostFactory.instance(posts.documents[i].data));
    }
    return output;
  }

  @override
  Future<void> delete(String id) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_POST)
        .document(id)
        .delete();
  }

  @override
  Future<Post> getById(String id) async {
    DocumentSnapshot response = await firestore
        .collection(FirebaseDao.COLLECTION_ID_POST)
        .document(id)
        .get();

    return PostFactory.instance(response.data);
  }
}
