import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/model/user.dart';

import 'i_user_dao.dart';

class UserFirebaseDao extends FirebaseDao<User> implements IUserDao {
  UserFirebaseDao(Firestore firestore) : super(firestore);

  Future<void> create(User toAdd) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_USER)
        .document(toAdd.id)
        .setData(toAdd.toJson());
  }

  Future<void> delete(String idToDelete) async {
    await firestore.collection(FirebaseDao.COLLECTION_ID_USER).document(idToDelete).delete();
  }

  Future<User> getById(String idToGet) async {
    DocumentSnapshot response =
        await firestore.collection(FirebaseDao.COLLECTION_ID_USER).document(idToGet).get();
    return User.fromJson(response.data);
  }

  Future<void> update(User toUpdate) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_USER)
        .document(toUpdate.id)
        .updateData(toUpdate.toJson());
  }

  Future<User> getFullUserById(String idToGet) async {
    DocumentSnapshot response =
        await firestore.collection(FirebaseDao.COLLECTION_ID_USER).document(idToGet).get();
    return User.fromJson(response.data);
  }
}
