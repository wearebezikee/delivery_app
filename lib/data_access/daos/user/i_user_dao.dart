import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/user.dart';

abstract class IUserDao extends Dao<User> {
  Future<void> create(User toAdd);

  Future<void> delete(String idToDelete);

  Future<User> getById(String idToGet);

  Future<void> update(User toUpdate);

  Future<User> getFullUserById(String idToGet);
}
