import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/category/i_category_dao.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/data_access/daos/product/product_firebase_dao.dart';
import 'package:delivery_app/model/category.dart';

class CategoryFirebaseDao extends FirebaseDao<Category>
    implements ICategoryDao {
  static const String ID_SEPARATOR = "->";

  CategoryFirebaseDao(Firestore firestore) : super(firestore);

  Future<void> create(Category toAdd) async {
    await firestore.runTransaction((transaction) async {
      if (!toAdd.isRoot()) {
        DocumentSnapshot parentSnapshot =
            await transaction.get(getParentCategoryReference(toAdd.id));
        Category parent = Category.fromJson(parentSnapshot.data);
        parent.categories.add(toAdd);
        await transaction.update(getParentCategoryReference(toAdd.id),
            {Category.ATTRIBUTE_CATEGORIES: parent.categoriesPreviewToJson()});
      }
      await transaction.set(getCategoryReference(toAdd.id), toAdd.toJson());
    });
  }

  Future<void> delete(String id) async {
    await firestore.runTransaction((transaction) async {
      if (id != Category.ROOT_ID) {
        DocumentSnapshot parentSnapshot =
            await transaction.get(getParentCategoryReference(id));
        Category parent = Category.fromJson(parentSnapshot.data);

        for (int i = 0; i < parent.categories.length; i++) {
          if (parent.categories[i].id == id) {
            parent.categories.removeAt(i);
          }
        }
        await transaction.update(getParentCategoryReference(id),
            {Category.ATTRIBUTE_CATEGORIES: parent.categoriesPreviewToJson()});
      }
      await getCategoryReference(id).delete();
    });
  }

  Future<Category> getById(String id) async {
    var response = await getCategoryReference(id).get();
    return Category.fromJson(response.data);
  }

  Future<Category> getByProductId(String id) async {
    return getById(getParentId(id));
  }

  Future<void> update(Category toUpdate) async {
    await firestore.runTransaction((transaction) async {
      if (!toUpdate.isRoot()) {
        DocumentSnapshot parentSnapshot =
            await transaction.get(getParentCategoryReference(toUpdate.id));
        Category parent = Category.fromJson(parentSnapshot.data);
        for (int i = 0; i < parent.categories.length; i++) {
          if (parent.categories[i].id == toUpdate.id) {
            parent.categories[i] = toUpdate;
          }
        }
        await transaction.update(getParentCategoryReference(toUpdate.id),
            {Category.ATTRIBUTE_CATEGORIES: parent.categoriesPreviewToJson()});
      }
      await transaction.update(
          getCategoryReference(toUpdate.id), toUpdate.toJson());
    });

    await getCategoryReference(toUpdate.id).updateData(toUpdate.toJson());
  }

  Future<Category> getFullTreeFrom(String categoryId) async {
    ProductFirebaseDao productFirebaseDao = ProductFirebaseDao(firestore);

    Category parent = await getById(categoryId);
    for (int i = 0; i < parent.categories.length; i++) {
      parent.categories[i] = await getFullTreeFrom(parent.categories[i].id);
    }
    for (int i = 0; i < parent.products.length; i++) {
      parent.products[i] =
          await productFirebaseDao.getById(parent.products[i].id);
    }
    return parent;
  }

  List<String> splitId(String id) {
    List<String> output = id.split(ID_SEPARATOR);
    return output;
  }

  String getParentId(String id) {
    List<String> aux = splitId(id);

    aux.removeLast();
    return buildId(aux);
  }

  String buildId(List<String> idList) {
    if (idList.length > 0) {
      String output = idList[0];
      for (int i = 1; i < idList.length; i++) {
        output = output + ID_SEPARATOR + idList[i];
      }
      return output;
    } else {
      return null;
    }
  }

  DocumentReference getCategoryReference(String id) {
    var reference = firestore
        .collection(FirebaseDao.COLLECTION_ID_CATEGORY)
        .document(Category.ROOT_ID);
    List<String> idList = splitId(id);
    idList.removeAt(0);
    for (int i = 0; i < idList.length; i++) {
      var actual = idList[i];
      reference = reference
          .collection(FirebaseDao.COLLECTION_ID_CATEGORY)
          .document(actual);
    }
    return reference;
  }

  DocumentReference getParentCategoryReference(String id) {
    var reference = firestore
        .collection(FirebaseDao.COLLECTION_ID_CATEGORY)
        .document(Category.ROOT_ID);
    List<String> idList = splitId(id);
    idList.removeAt(0);
    idList.removeLast();
    for (int i = 0; i < idList.length; i++) {
      var actual = idList[i];
      reference = reference
          .collection(FirebaseDao.COLLECTION_ID_CATEGORY)
          .document(actual);
    }
    return reference;
  }
}
