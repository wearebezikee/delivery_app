import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/category.dart';

abstract class ICategoryDao extends Dao<Category> {
  Future<void> create(Category toAdd);

  Future<void> delete(String id);

  Future<Category> getById(String id);

  Future<void> update(Category toUpdate);
}
