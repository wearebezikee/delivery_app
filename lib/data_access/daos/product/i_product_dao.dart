import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/product.dart';

abstract class IProductDao extends Dao<Product> {
  Future<void> create(Product toAdd);

  Future<void> delete(String id);

  Future<Product> getById(String id);

  Future<void> update(Product toUpdate);
}
