import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/category/category_firebase_dao.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';

import 'i_product_dao.dart';

class ProductFirebaseDao extends FirebaseDao<Product> implements IProductDao {
  CategoryFirebaseDao _categoryFirebaseDao;

  ProductFirebaseDao(Firestore firestore) : super(firestore) {
    _categoryFirebaseDao = CategoryFirebaseDao(firestore);
  }

  Future<void> create(Product toAdd) async {
    await firestore.runTransaction((transaction) async {
      DocumentSnapshot categorySnapshot = await transaction.get(
          _categoryFirebaseDao.getCategoryReference(
              _categoryFirebaseDao.getParentId(toAdd.id)));
      Category category = Category.fromJson(categorySnapshot.data);
      category.products.add(toAdd);
      await transaction.update(
          _categoryFirebaseDao
              .getCategoryReference(_categoryFirebaseDao.getParentId(toAdd.id)),
          {Category.ATTRIBUTE_PRODUCTS: category.productsPreviewToJson()});

      await transaction.set(
          _categoryFirebaseDao
              .getCategoryReference(_categoryFirebaseDao.getParentId(toAdd.id))
              .collection(FirebaseDao.COLLECTION_ID_PRODUCT)
              .document(toAdd.id),
          toAdd.toJson());
    });
  }

  Future<void> delete(String id) async {
    await firestore.runTransaction((transaction) async {
      DocumentSnapshot categorySnapshot = await transaction.get(
          _categoryFirebaseDao
              .getCategoryReference(_categoryFirebaseDao.getParentId(id)));
      Category category = Category.fromJson(categorySnapshot.data);

      for (int i = 0; i < category.products.length; i++) {
        if (category.products[i].id == id) {
          category.products.removeAt(i);
        }
      }
      await transaction.update(
          _categoryFirebaseDao
              .getCategoryReference(_categoryFirebaseDao.getParentId(id)),
          {Category.ATTRIBUTE_PRODUCTS: category.productsPreviewToJson()});

      await _categoryFirebaseDao
          .getCategoryReference(_categoryFirebaseDao.getParentId(id))
          .collection(FirebaseDao.COLLECTION_ID_PRODUCT)
          .document(id)
          .delete();
    });
  }

  Future<Product> getById(String id) async {
    var response = await _categoryFirebaseDao
        .getCategoryReference(_categoryFirebaseDao.getParentId(id))
        .collection(FirebaseDao.COLLECTION_ID_PRODUCT)
        .document(id)
        .get();
    return Product.fromJson(response.data);
  }

  Future<void> update(Product toUpdate) async {
    await firestore.runTransaction((transaction) async {
      DocumentSnapshot categorySnapshot = await transaction.get(
          _categoryFirebaseDao.getCategoryReference(
              _categoryFirebaseDao.getParentId(toUpdate.id)));
      Category category = Category.fromJson(categorySnapshot.data);

      for (int i = 0; i < category.products.length; i++) {
        if (category.products[i].id == toUpdate.id) {
          category.products[i] = toUpdate;
        }
      }

      await transaction.update(
          _categoryFirebaseDao.getCategoryReference(
              _categoryFirebaseDao.getParentId(toUpdate.id)),
          {Category.ATTRIBUTE_PRODUCTS: category.productsPreviewToJson()});

      await transaction.update(
          _categoryFirebaseDao
              .getCategoryReference(
                  _categoryFirebaseDao.getParentId(toUpdate.id))
              .collection(FirebaseDao.COLLECTION_ID_PRODUCT)
              .document(toUpdate.id),
          toUpdate.toJson());
    });
  }
}
