import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/model/order.dart';

import 'i_order_dao.dart';

class OrderFirebaseDao extends FirebaseDao<Order> implements IOrderDao {
  OrderFirebaseDao(Firestore firestore) : super(firestore);

  @override
  Future<Order> create(Order toCreate) async {
    toCreate.id = await createRandomId();
    await firestore
        .collection(FirebaseDao.COLLECTION_ORDER)
        .document(toCreate.id)
        .setData(toCreate.toJson());
    return toCreate;
  }

  @override
  Future<void> update(Order post) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ORDER)
        .document(post.id)
        .updateData(post.toJson());
  }

  @override
  Future<List<Order>> getAll() async {
    var posts =
        await firestore.collection(FirebaseDao.COLLECTION_ORDER).getDocuments();
    List<Order> output = List();
    for (int i = 0; i < posts.documents.length; i++) {
      output.add(Order.fromJson(posts.documents[i].data));
    }
    return output;
  }

  @override
  Future<List<Order>> getNext(
      String attributeName, int limit, Order last) async {
    QuerySnapshot results;
    if (last == null) {
      results = await firestore
          .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
          .orderBy(attributeName)
          .limit(limit)
          .getDocuments();
    } else {
      results = await firestore
          .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
          .orderBy(attributeName)
          .limit(limit)
          .startAt([last.toJson()[attributeName]]).getDocuments();
    }

    List<Order> output = List();
    for (int i = 0; i < results.documents.length; i++) {
      output.add(Order.fromJson(results.documents[i].data));
    }
    return output;
  }

  @override
  Future<void> delete(String id) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ORDER)
        .document(id)
        .delete();
  }

  @override
  Future<Order> getById(String id) async {
    DocumentSnapshot response = await firestore
        .collection(FirebaseDao.COLLECTION_ORDER)
        .document(id)
        .get();

    return Order.fromJson(response.data);
  }
}
