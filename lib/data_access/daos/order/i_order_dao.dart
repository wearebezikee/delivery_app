import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/order.dart';

abstract class IOrderDao extends Dao<Order> {
  Future<void> create(Order toAdd);

  Future<void> delete(String id);

  Future<Order> getById(String id);

  Future<void> update(Order toUpdate);
}
