import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/data_access/daos/payment_method_setting/i_payment_method_setting_dao.dart';
import 'package:delivery_app/domain_logic/factories/payment_method_setting_factory.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';

class PaymentMethodSettingFirebaseDao extends FirebaseDao<PaymentMethodSetting>
    implements IPaymentMethodSettingDao {
  PaymentMethodSettingFirebaseDao(Firestore firestore) : super(firestore);

  @override
  Future<PaymentMethodSetting> create(PaymentMethodSetting toCreate) async {
    toCreate.id = await createRandomId();
    await firestore
        .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
        .document(toCreate.id)
        .setData(toCreate.toJson());
    return toCreate;
  }

  @override
  Future<void> update(PaymentMethodSetting post) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
        .document(post.id)
        .updateData(post.toJson());
  }

  @override
  Future<List<PaymentMethodSetting>> getAll() async {
    var posts = await firestore
        .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
        .getDocuments();
    List<PaymentMethodSetting> output = List();
    for (int i = 0; i < posts.documents.length; i++) {
      output.add(PaymentMethodSettingFactory.instance(posts.documents[i].data));
    }
    return output;
  }

  @override
  Future<void> delete(String id) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
        .document(id)
        .delete();
  }

  @override
  Future<PaymentMethodSetting> getById(String id) async {
    DocumentSnapshot response = await firestore
        .collection(FirebaseDao.COLLECTION_PAYMENT_METHODS)
        .document(id)
        .get();

    return PaymentMethodSettingFactory.instance(response.data);
  }
}
