import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/payment/payment_method_setting/payment_method_setting.dart';

abstract class IPaymentMethodSettingDao extends Dao<PaymentMethodSetting> {
  Future<void> create(PaymentMethodSetting toAdd);

  Future<void> delete(String id);

  Future<PaymentMethodSetting> getById(String id);

  Future<void> update(PaymentMethodSetting toUpdate);
}
