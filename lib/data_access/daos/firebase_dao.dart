import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/misc/random_generator.dart';

import 'dao.dart';

abstract class FirebaseDao<T> extends Dao<T> {
  Firestore firestore;

  FirebaseDao(this.firestore);

  static const String COLLECTION_ID_CATEGORY = "Category";
  static const String COLLECTION_ID_PRODUCT = "Product";
  static const String COLLECTION_ID_USER = "User";
  static const String COLLECTION_ID_POST = "Post";
  static const String COLLECTION_ID_SCHEDULE = "Schedule";
  static const String COLLECTION_ID_SETTINGS = "Settings";
  static const String COLLECTION_PAYMENT_METHODS = "Payment Methods";
  static const String COLLECTION_ORDER = "Order";

  Future<String> createRandomId() async {
    return await RandomGenerator.randomString(25);
  }
}
