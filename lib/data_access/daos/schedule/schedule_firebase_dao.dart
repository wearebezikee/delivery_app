import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:delivery_app/data_access/daos/firebase_dao.dart';
import 'package:delivery_app/model/schedule.dart';

import 'i_schedule_dao.dart';

class ScheduleFirebaseDao extends FirebaseDao<Schedule>
    implements IScheduleDao {
  ScheduleFirebaseDao(Firestore firestore) : super(firestore);

  Future<Schedule> create(Schedule toAdd) async {
    toAdd.id = await createRandomId();
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_SCHEDULE)
        .document(toAdd.id)
        .setData(toAdd.toJson());
    return toAdd;
  }

  Future<void> delete(String idToDelete) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_SCHEDULE)
        .document(idToDelete)
        .delete();
  }

  Future<Schedule> getById(String idToGet) async {
    DocumentSnapshot response = await firestore
        .collection(FirebaseDao.COLLECTION_ID_SCHEDULE)
        .document(idToGet)
        .get();
    return Schedule.fromJson(response.data);
  }

  Future<void> update(Schedule toUpdate) async {
    await firestore
        .collection(FirebaseDao.COLLECTION_ID_SCHEDULE)
        .document(toUpdate.id)
        .updateData(toUpdate.toJson());
  }

  Future<List<Schedule>> getAll() async {
    var posts = await firestore
        .collection(FirebaseDao.COLLECTION_ID_SCHEDULE)
        .getDocuments();
    List<Schedule> output = List();
    for (int i = 0; i < posts.documents.length; i++) {
      output.add(Schedule.fromJson(posts.documents[i].data));
    }
    //TODO sort cronologically using output.sort and defining comparison function

    return output;
  }
}
