import 'package:delivery_app/data_access/daos/dao.dart';
import 'package:delivery_app/model/schedule.dart';

abstract class IScheduleDao extends Dao<Schedule> {
  Future<Schedule> create(Schedule toAdd);

  Future<void> delete(String idToDelete);

  Future<Schedule> getById(String idToGet);

  Future<void> update(Schedule toUpdate);

  Future<List<Schedule>> getAll();
}
