import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/navigators/orders_navigator/orders_routes.dart';
import 'package:delivery_app/ui/screens/order/order_detail/order_detail_screen.dart';
import 'package:delivery_app/ui/screens/order/orders/orders_screen.dart';
import 'package:flutter/material.dart';

class OrdersNavigator {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> toHome() {
    return _pushRoute(OrdersRoutes.home);
  }

  Future<void> toOrderDetail(Order order) {
    return _pushRoute(OrdersRoutes.orderDetail, arguments: order);
  }

  popScreen() {
    navigatorKey.currentState.pop();
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case OrdersRoutes.home:
        return MaterialPageRoute(builder: (context) => OrdersScreen());

      case OrdersRoutes.orderDetail:
        return MaterialPageRoute(
            builder: (context) => OrderDetailScreen(order: settings.arguments));
    }
    return null;
  }

  Future<T> _pushRoute<T>(String routeName,
      {Object arguments, bool clearStack = false}) {
    if (clearStack) {
      return navigatorKey.currentState.pushNamedAndRemoveUntil<T>(
          routeName, (route) => false,
          arguments: arguments);
    } else {
      return navigatorKey.currentState
          .pushNamed<T>(routeName, arguments: arguments);
    }
  }
}
