class MenuRoutes {
  MenuRoutes._();

  static const String home = '/home';
  static const String category = '/category';
  static const String product = '/product';
  static const String paymentDetail = '/paymentDetail';
}
