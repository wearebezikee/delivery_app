import 'package:delivery_app/model/cart_item.dart';
import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/menu_navigator/menu_routes.dart';
import 'package:delivery_app/ui/screens/category/category_screen.dart';
import 'package:delivery_app/ui/screens/menu/menu_screen.dart';
import 'package:delivery_app/ui/screens/payment_detail/payment_detail_screen.dart';
import 'package:delivery_app/ui/screens/product/product_screen.dart';
import 'package:flutter/material.dart';

class MenuNavigator {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> toHome() {
    return _pushRoute(MenuRoutes.home);
  }

  Future<void> toCategory(Category category) {
    return _pushRoute(MenuRoutes.category, arguments: category);
  }

  Future<void> toProduct(Product product) {
    return _pushRoute(MenuRoutes.product, arguments: product);
  }

  Future<void> toPaymentDetail() {
    return _pushRoute(MenuRoutes.paymentDetail);
  }

  popScreen() {
    navigatorKey.currentState.pop();
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case MenuRoutes.home:
        return MaterialPageRoute(builder: (context) => MenuScreen());
      case MenuRoutes.category:
        return MaterialPageRoute(
            builder: (context) => CategoryScreen(
                  category: settings.arguments,
                ));
      case MenuRoutes.product:
        return MaterialPageRoute(
            builder: (context) => ProductScreen(product: settings.arguments));
      case MenuRoutes.paymentDetail:
        return MaterialPageRoute(builder: (context) => PaymentDetailScreen());
    }
    return null;
  }

  Future<T> _pushRoute<T>(String routeName,
      {Object arguments, bool clearStack = false}) {
    if (clearStack) {
      return navigatorKey.currentState.pushNamedAndRemoveUntil<T>(
          routeName, (route) => false,
          arguments: arguments);
    } else {
      return navigatorKey.currentState
          .pushNamed<T>(routeName, arguments: arguments);
    }
  }
}
