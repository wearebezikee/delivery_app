class InventoryCmsRoutes {
  InventoryCmsRoutes._();

  static const String home = '/home';

  //Category Begin
  static const String categoryView = '/categoryView';
  static const String categoryCreate = '/categoryCreate';
  static const String categoryUpdate = '/categoryUpdate';
//Category End

  //Product Begin
  static const String productView = '/productView';
  static const String productCreate = '/productCreate';
  static const String productUpdate = '/productUpdate';
//Product End

}
