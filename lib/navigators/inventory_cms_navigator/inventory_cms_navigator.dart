import 'package:delivery_app/model/category.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/inventory_cms_navigator/inventory_cms_routes.dart';
import 'package:delivery_app/ui/loaders/my_progress_indicator.dart';
import 'package:delivery_app/ui/screens/cms/inventory/category/create_category_cms_component.dart';
import 'package:delivery_app/ui/screens/cms/inventory/category/update_category_cms_component.dart';
import 'package:delivery_app/ui/screens/cms/inventory/category/view_category_cms_component.dart';
import 'package:delivery_app/ui/screens/cms/inventory/product/create_product_cms_component.dart';
import 'package:delivery_app/ui/screens/cms/inventory/product/update_product_cms_component.dart';
import 'package:delivery_app/ui/screens/cms/inventory/product/view_product_cms_component.dart';
import 'package:flutter/material.dart';

class InventoryCmsNavigator {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> toHome() {
    return _pushRoute(InventoryCmsRoutes.home);
  }

  //Category begin
  Future<void> toCategoryView(Category category) {
    return _pushRoute(InventoryCmsRoutes.categoryView, arguments: category);
  }

  Future<void> toCategoryCreate(String parentCategoryId) {
    return _pushRoute(InventoryCmsRoutes.categoryCreate,
        arguments: parentCategoryId);
  }

  Future<void> toCategoryUpdate(Category category) {
    return _pushRoute(InventoryCmsRoutes.categoryUpdate, arguments: category);
  }
  //Category end

  //Product begin
  Future<void> toProductView(Product product) {
    return _pushRoute(InventoryCmsRoutes.productView, arguments: product);
  }

  Future<void> toProductCreate(String parentCategoryId) {
    return _pushRoute(InventoryCmsRoutes.productCreate,
        arguments: parentCategoryId);
  }

  Future<void> toProductUpdate(Product product) {
    return _pushRoute(InventoryCmsRoutes.productUpdate, arguments: product);
  }
  //Product end

  popScreen() {
    navigatorKey.currentState.pop();
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case InventoryCmsRoutes.home:
        return MaterialPageRoute(
            builder: (context) => Center(
                    child: Container(
                  height: 80,
                  width: 80,
                  child: MyProgressIndicator(),
                )));

      //Category Begin
      case InventoryCmsRoutes.categoryView:
        return MaterialPageRoute(
            builder: (context) =>
                ViewCategoryCmsComponent(category: settings.arguments));
      case InventoryCmsRoutes.categoryCreate:
        return MaterialPageRoute(
            builder: (context) => CreateCategoryCmsComponent(
                parentCategoryId: settings.arguments));
      case InventoryCmsRoutes.categoryUpdate:
        return MaterialPageRoute(
            builder: (context) =>
                UpdateCategoryCmsComponent(category: settings.arguments));
      //Category end
      //Product Begin
      case InventoryCmsRoutes.productView:
        return MaterialPageRoute(
            builder: (context) =>
                ViewProductCmsComponent(product: settings.arguments));
      case InventoryCmsRoutes.productCreate:
        return MaterialPageRoute(
            builder: (context) => CreateProductCmsComponent(
                parentCategoryId: settings.arguments));
      case InventoryCmsRoutes.productUpdate:
        return MaterialPageRoute(
            builder: (context) =>
                UpdateProductCmsComponent(product: settings.arguments));
      //Product end
    }
    return null;
  }

  Future<T> _pushRoute<T>(String routeName,
      {Object arguments, bool clearStack = true}) {
    if (clearStack) {
      return navigatorKey.currentState.pushNamedAndRemoveUntil<T>(
          routeName, (route) => false,
          arguments: arguments);
    } else {
      return navigatorKey.currentState
          .pushNamed<T>(routeName, arguments: arguments);
    }
  }
}
