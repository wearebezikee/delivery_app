class SettingsRoutes {
  SettingsRoutes._();

  static const String home = '/home';
  static const String manageInventory = '/manageInventory';
  static const String manageTimeline = '/manageTimeline';
  static const String editPost = '/editPost';
  static const String manageSchedule = '/manageSchedule';
  static const String manageAddresses = '/manageAddresses';
  static const String createAddress = '/createAddress';
  static const String managePaymentMethodsSettings =
      '/managePaymentMethodsSettings';
  static const String createPaymentMethodSetting =
      '/createPaymentMethodSetting';
  static const String manageOrders = '/manageOrders';
  static const String orderDetail = '/orderDetail';
}
