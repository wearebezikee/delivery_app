import 'package:delivery_app/model/order.dart';
import 'package:delivery_app/navigators/settings_navigator/settings_routes.dart';
import 'package:delivery_app/ui/screens/cms/addresses/create_address_screen.dart';
import 'package:delivery_app/ui/screens/cms/addresses/mange_addresses_screen.dart';
import 'package:delivery_app/ui/screens/cms/inventory/manage_inventory_screen.dart';
import 'package:delivery_app/ui/screens/cms/orders/mange_orders_screen.dart';
import 'package:delivery_app/ui/screens/cms/payment_method_setting/create_payment_method_setting_screen.dart';
import 'package:delivery_app/ui/screens/cms/payment_method_setting/mange_payment_method_settings_screen.dart';
import 'package:delivery_app/ui/screens/cms/schedule/manage_schedule_screen.dart';
import 'package:delivery_app/ui/screens/cms/timeline/manage_timeline_screen.dart';
import 'package:delivery_app/ui/screens/order/order_detail/order_detail_screen.dart';
import 'package:delivery_app/ui/screens/settings/settings_screen.dart';
import 'package:flutter/material.dart';

class SettingsNavigator {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> toSettings() {
    return _pushRoute(SettingsRoutes.home);
  }

  Future<void> toManageInventory() {
    return _pushRoute(SettingsRoutes.manageInventory);
  }

  Future<void> toManageTimeline() {
    return _pushRoute(SettingsRoutes.manageTimeline);
  }

  Future<void> toManageSchedule() {
    return _pushRoute(SettingsRoutes.manageSchedule);
  }

  Future<void> toManageAddresses() {
    return _pushRoute(SettingsRoutes.manageAddresses);
  }

  Future<void> toCreateAddress() {
    return _pushRoute(SettingsRoutes.createAddress);
  }

  Future<void> toManagePaymentMethodSettings() {
    return _pushRoute(SettingsRoutes.managePaymentMethodsSettings);
  }

  Future<void> toCreatePaymentMethodSetting() {
    return _pushRoute(SettingsRoutes.createPaymentMethodSetting);
  }

  Future<void> toManageOrders() {
    return _pushRoute(SettingsRoutes.manageOrders);
  }

  Future<void> toOrderDetail(Order order) {
    return _pushRoute(SettingsRoutes.orderDetail, arguments: order);
  }

  popScreen() {
    navigatorKey.currentState.pop();
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SettingsRoutes.home:
        return MaterialPageRoute(builder: (context) => SettingsScreen());
      case SettingsRoutes.manageInventory:
        return MaterialPageRoute(builder: (context) => ManageInventoryScreen());
      case SettingsRoutes.manageTimeline:
        return MaterialPageRoute(builder: (context) => ManageTimelineScreen());
      case SettingsRoutes.manageSchedule:
        return MaterialPageRoute(builder: (context) => ManageScheduleScreen());
      case SettingsRoutes.manageAddresses:
        return MaterialPageRoute(builder: (context) => ManageAddressesScreen());
      case SettingsRoutes.createAddress:
        return MaterialPageRoute(builder: (context) => CreateAddressScreen());
      case SettingsRoutes.managePaymentMethodsSettings:
        return MaterialPageRoute(
            builder: (context) => ManagePaymentMethodSettingsScreen());
      case SettingsRoutes.createPaymentMethodSetting:
        return MaterialPageRoute(
            builder: (context) => CreatePaymentMethodSettingScreen());
      case SettingsRoutes.manageOrders:
        return MaterialPageRoute(builder: (context) => ManageOrdersScreen());
      case SettingsRoutes.orderDetail:
        return MaterialPageRoute(builder: (context) => OrderDetailScreen());
    }
    return null;
  }

  Future<T> _pushRoute<T>(String routeName,
      {Object arguments, bool clearStack = false}) {
    if (clearStack) {
      return navigatorKey.currentState.pushNamedAndRemoveUntil<T>(
          routeName, (route) => false,
          arguments: arguments);
    } else {
      return navigatorKey.currentState
          .pushNamed<T>(routeName, arguments: arguments);
    }
  }
}
