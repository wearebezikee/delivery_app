class HomeRoutes {
  HomeRoutes._();

  static const String home = '/home';
  static const String product = '/product';
  static const String editPost = '/editPost';
}
