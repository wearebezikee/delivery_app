import 'package:delivery_app/model/posts/post.dart';
import 'package:delivery_app/model/product.dart';
import 'package:delivery_app/navigators/home_navigator/home_routes.dart';
import 'package:delivery_app/ui/screens/home/edit_post_screen.dart';
import 'package:delivery_app/ui/screens/home/home_screen.dart';
import 'package:delivery_app/ui/screens/product/product_screen.dart';
import 'package:flutter/material.dart';

class HomeNavigator {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> toHome() {
    return _pushRoute(HomeRoutes.home);
  }

  Future<void> toProduct(Product product) {
    return _pushRoute(HomeRoutes.product, arguments: product);
  }

  Future<void> toEditPost(Post post) {
    return _pushRoute(HomeRoutes.editPost, arguments: post);
  }

  popScreen() {
    navigatorKey.currentState.pop();
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomeRoutes.home:
        return MaterialPageRoute(builder: (context) => HomeScreen());
      case HomeRoutes.product:
        return MaterialPageRoute(
            builder: (context) => ProductScreen(product: settings.arguments));
      case HomeRoutes.editPost:
        return MaterialPageRoute(
            builder: (context) => EditPostScreen(post: settings.arguments));
    }
    return null;
  }

  Future<T> _pushRoute<T>(String routeName,
      {Object arguments, bool clearStack = false}) {
    if (clearStack) {
      return navigatorKey.currentState.pushNamedAndRemoveUntil<T>(
          routeName, (route) => false,
          arguments: arguments);
    } else {
      return navigatorKey.currentState
          .pushNamed<T>(routeName, arguments: arguments);
    }
  }
}
