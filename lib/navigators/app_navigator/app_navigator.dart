import 'package:delivery_app/navigators/app_navigator/app_routes.dart';
import 'package:delivery_app/ui/screens/authentication/authentication_screen.dart';
import 'package:delivery_app/ui/screens/authentication/create_user_form_screen.dart';
import 'package:delivery_app/ui/screens/dashboard/dashboard.dart';
import 'package:flutter/material.dart';

class AppNavigator {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<void> toDashboard() {
    return _pushRoute(AppRoutes.dashboard, clearStack: true);
  }

  Future<void> toAuthentication({bool isSignIn}) {
    return _pushRoute(AppRoutes.authentication, clearStack: true, arguments: isSignIn);
  }

  Future<void> toCreateUserForm(String id) {
    return _pushRoute(AppRoutes.createUserForm, arguments: id);
  }

  popScreen() {
    navigatorKey.currentState.pop();
  }

  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.dashboard:
        return MaterialPageRoute(builder: (context) => Dashboard());
      case AppRoutes.authentication:
        return MaterialPageRoute(
            builder: (context) => AuthenticationScreen(isSignIn: settings.arguments));
      case AppRoutes.createUserForm:
        return MaterialPageRoute(builder: (context) => CreateUserFormScreen(settings.arguments));
    }
    return null;
  }

  Future<T> _pushRoute<T>(String routeName, {Object arguments, bool clearStack = false}) {
    if (clearStack) {
      return navigatorKey.currentState
          .pushNamedAndRemoveUntil<T>(routeName, (route) => false, arguments: arguments);
    } else {
      return navigatorKey.currentState.pushNamed<T>(routeName, arguments: arguments);
    }
  }
}
