class PaymentException implements Exception {
  static const String EMPTY_CASH_AMMOUNT =
      'Por favor indique la cantidad de efectivo a usar para el pago';
  static const String EMPTY_REFERENCE_NUMBER =
      'Por favor indique el número de referencia';
  static const String EMPTY_BANK_NAME =
      'Por favor indique el nombre del banco de origen';
  static const String EMPTY_CONFIRMATION_IMAGE =
      'por favor suba una imagen con la confirmacion del pago';
  static const String ERROR_UPLOADING_IMAGE =
      'Ha ocurrido un error cargando la imagen';
}
