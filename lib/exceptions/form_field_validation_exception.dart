class FormFieldValidationException implements Exception {
  //User form Begin
  static const String _NOT_AN_EMAIL = "Email is not properly formatted.";
  static const String _NOT_A_PHONE_NUMBER =
      "Phone number is not properly formatted.";
  static const String _TOO_SHORT_NAME =
      "The name should be at least 3 characters long.";
  static const String _TOO_SHORT_PASSWORD =
      "The password should be at least 6 characters long.";
  static const String _PASSWORD_MISMATCH =
      "The passwords entered should be the same.";

  //User form End

  //Category form Begin
  static const String _TOO_SHORT_CATEGORY_NAME =
      "The category name should be at least 3 characters long.";

  //Category form End

  //Post form Begin
  static const String _TOO_SHORT_POST_NAME =
      "The post title should be at least 3 characters long.";
  static const String _TOO_SHORT_POST_DESCRIPTION =
      "The post description should be at least 3 characters long.";

  //Post form End

  //Product form Begin
  static const String _TOO_SHORT_PRODUCT_NAME =
      "The product name should be at least 3 characters long.";
  static const String _NO_PRICE = "Please, select a price.";
  static const String _TOO_SHORT_PRODUCT_DESCRIPTION =
      "The product description should be at least 3 characters long.";

  //Product form End

  //Payment Method form Begin
  static const String _PAYMENT_METHOD_TITLE_TOO_SHORT =
      "The payment method title should be at least 3 characters long.";

  static const String _PAYMENT_METHOD_BANK_NAME_TOO_SHORT =
      "The payment method bank name should be at least 3 characters long.";

  static const String _PAYMENT_METHOD_ACCOUNT_NUMBER_TOO_SHORT =
      "The payment method account number should be at least 10 characters long.";

  static const String _PAYMENT_METHOD_ID_NUMBER_TOO_SHORT =
      "The payment method id number should be at least 10 characters long.";

  static const String _PAYMENT_METHOD_DESCRIPTION_TOO_SHORT =
      "The payment method description should be at least 3 characters long.";

  //Payment Method form End
  //Schedule form Begin
  static const String _NO_START_HOUR = "Please, select a start hour.";
  static const String _NO_END_HOUR = "Please, select a end hour.";
  static const String _NO_DAY_OF_WEEK = "Please, select a day of week.";

  //Schedule form End

  //Address Begin
  static const String _TOO_SHORT_ADDRESS =
      "The address should be at least 10 characters long.";
  static const String _NO_STREET = "Please, enter a street.";
  static const String _NO_TOWN = "Please, enter a town.";
  static const String _NO_CITY = "Please, enter a city.";

  //Address End

  //Other Begin

  static const String _NO_IMAGE_SELECTED = "Select an image.";

  //Other End

  String _message;

  //User form Begin
  FormFieldValidationException.notAnEmail() {
    this._message = _NOT_AN_EMAIL;
  }

  FormFieldValidationException.notAPhoneNumber() {
    this._message = _NOT_A_PHONE_NUMBER;
  }

  FormFieldValidationException.tooShortName() {
    this._message = _TOO_SHORT_NAME;
  }

  FormFieldValidationException.tooShortPassword() {
    this._message = _TOO_SHORT_PASSWORD;
  }

  FormFieldValidationException.passwordMismatch() {
    this._message = _PASSWORD_MISMATCH;
  }

  //User form End

  //Address form begin
  FormFieldValidationException.tooShortAddress() {
    this._message = _TOO_SHORT_ADDRESS;
  }

  FormFieldValidationException.noStreet() {
    this._message = _NO_STREET;
  }

  FormFieldValidationException.noTown() {
    this._message = _NO_TOWN;
  }

  FormFieldValidationException.noCity() {
    this._message = _NO_CITY;
  }

  //Address form end

  //Category form begin
  FormFieldValidationException.tooShortCategoryName() {
    this._message = _TOO_SHORT_CATEGORY_NAME;
  }

  //Category form end

  //Post form begin
  FormFieldValidationException.tooShortPostTitle() {
    this._message = _TOO_SHORT_POST_NAME;
  }

  FormFieldValidationException.tooShortPostDescription() {
    this._message = _TOO_SHORT_POST_DESCRIPTION;
  }

  //Post form end

  //Product form begin
  FormFieldValidationException.tooShortProductName() {
    this._message = _TOO_SHORT_PRODUCT_NAME;
  }

  FormFieldValidationException.noPrice() {
    this._message = _NO_PRICE;
  }

  FormFieldValidationException.tooShortProductDescription() {
    this._message = _TOO_SHORT_PRODUCT_DESCRIPTION;
  }

  //Product form end

  //Schedule form begin

  FormFieldValidationException.noStartHour() {
    this._message = _NO_START_HOUR;
  }

  FormFieldValidationException.noEndHour() {
    this._message = _NO_END_HOUR;
  }

  FormFieldValidationException.noDayOfWeek() {
    this._message = _NO_DAY_OF_WEEK;
  }

  //Schedule form end

  //Payment Method Setting form begin

  FormFieldValidationException.paymentMethodSettingTitleTooShort() {
    this._message = _PAYMENT_METHOD_TITLE_TOO_SHORT;
  }

  FormFieldValidationException.paymentMethodSettingBankNameTooShort() {
    this._message = _PAYMENT_METHOD_BANK_NAME_TOO_SHORT;
  }

  FormFieldValidationException.paymentMethodSettingAccountNumberTooShort() {
    this._message = _PAYMENT_METHOD_ACCOUNT_NUMBER_TOO_SHORT;
  }

  FormFieldValidationException.paymentMethodSettingIdNumberTooShort() {
    this._message = _PAYMENT_METHOD_ID_NUMBER_TOO_SHORT;
  }

  FormFieldValidationException.paymentMethodSettingDescriptionTooShort() {
    this._message = _PAYMENT_METHOD_DESCRIPTION_TOO_SHORT;
  }

//Product form end

//Other begin
  FormFieldValidationException.noImageSelected() {
    this._message = _NO_IMAGE_SELECTED;
//Other end
  }

  @override
  String toString() {
    return "Invalid field: " + _message;
  }
}
